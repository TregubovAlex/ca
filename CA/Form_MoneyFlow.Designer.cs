﻿namespace CA
{
    partial class Form_MoneyFlow : Form_BaseStyle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_MoneyFlow));
            this.table_ProjectsDataGridView = new System.Windows.Forms.DataGridView();
            this.ProjectID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.companyIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.commentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.marginDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.projectStatusIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableProjectsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cA_DB_DataSet = new CA.CA_DB_DataSet();
            this.table_ProjectsTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_ProjectsTableAdapter();
            this.table_MoneyFlowBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.table_MoneyFlowTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_MoneyFlowTableAdapter();
            this.tableAdapterManager = new CA.CA_DB_DataSetTableAdapters.TableAdapterManager();
            this.table_MoneyFlowDataGridView = new System.Windows.Forms.DataGridView();
            this.MoneyFlowID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MoneyFlowState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MoneyFlowProjectID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tableProjectsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.MoneyFlowDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MoneyFlowSum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MoneyFlowIsCash = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.MoneyFlowEmployeeID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tableEmployeesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.MoneyFlowMaterialID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tableMaterialsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.MoneyFlowCommentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.table_EmployeesTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_EmployeesTableAdapter();
            this.table_MaterialsTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_MaterialsTableAdapter();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.bindingNavigator2 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem1 = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveFirstItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem1 = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_ToExcel1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_ToExcel2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.cA_DB_DataSet1 = new CA.CA_DB_DataSet();
            ((System.ComponentModel.ISupportInitialize)(this.table_ProjectsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableProjectsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cA_DB_DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_MoneyFlowBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_MoneyFlowDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableProjectsBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableEmployeesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableMaterialsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator2)).BeginInit();
            this.bindingNavigator2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cA_DB_DataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // table_ProjectsDataGridView
            // 
            this.table_ProjectsDataGridView.AllowUserToAddRows = false;
            this.table_ProjectsDataGridView.AllowUserToDeleteRows = false;
            this.table_ProjectsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.table_ProjectsDataGridView.AutoGenerateColumns = false;
            this.table_ProjectsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_ProjectsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProjectID,
            this.ProjectState,
            this.companyIDDataGridViewTextBoxColumn,
            this.commentDataGridViewTextBoxColumn,
            this.numDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.dateDataGridViewTextBoxColumn,
            this.marginDataGridViewTextBoxColumn,
            this.sumDataGridViewTextBoxColumn,
            this.projectStatusIDDataGridViewTextBoxColumn});
            this.table_ProjectsDataGridView.DataSource = this.tableProjectsBindingSource;
            this.table_ProjectsDataGridView.Location = new System.Drawing.Point(0, 28);
            this.table_ProjectsDataGridView.MultiSelect = false;
            this.table_ProjectsDataGridView.Name = "table_ProjectsDataGridView";
            this.table_ProjectsDataGridView.ReadOnly = true;
            this.table_ProjectsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.table_ProjectsDataGridView.Size = new System.Drawing.Size(922, 103);
            this.table_ProjectsDataGridView.TabIndex = 0;
            this.table_ProjectsDataGridView.CurrentCellChanged += new System.EventHandler(this.table_ProjectsDataGridView_CurrentCellChanged);
            // 
            // ProjectID
            // 
            this.ProjectID.DataPropertyName = "id";
            this.ProjectID.HeaderText = "id";
            this.ProjectID.Name = "ProjectID";
            this.ProjectID.ReadOnly = true;
            this.ProjectID.Visible = false;
            this.ProjectID.Width = 40;
            // 
            // ProjectState
            // 
            this.ProjectState.DataPropertyName = "State";
            this.ProjectState.HeaderText = "State";
            this.ProjectState.Name = "ProjectState";
            this.ProjectState.ReadOnly = true;
            this.ProjectState.Visible = false;
            // 
            // companyIDDataGridViewTextBoxColumn
            // 
            this.companyIDDataGridViewTextBoxColumn.DataPropertyName = "CompanyID";
            this.companyIDDataGridViewTextBoxColumn.HeaderText = "CompanyID";
            this.companyIDDataGridViewTextBoxColumn.Name = "companyIDDataGridViewTextBoxColumn";
            this.companyIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.companyIDDataGridViewTextBoxColumn.Visible = false;
            this.companyIDDataGridViewTextBoxColumn.Width = 87;
            // 
            // commentDataGridViewTextBoxColumn
            // 
            this.commentDataGridViewTextBoxColumn.DataPropertyName = "Comment";
            this.commentDataGridViewTextBoxColumn.HeaderText = "Comment";
            this.commentDataGridViewTextBoxColumn.Name = "commentDataGridViewTextBoxColumn";
            this.commentDataGridViewTextBoxColumn.ReadOnly = true;
            this.commentDataGridViewTextBoxColumn.Visible = false;
            this.commentDataGridViewTextBoxColumn.Width = 76;
            // 
            // numDataGridViewTextBoxColumn
            // 
            this.numDataGridViewTextBoxColumn.DataPropertyName = "Num";
            this.numDataGridViewTextBoxColumn.HeaderText = "Номер";
            this.numDataGridViewTextBoxColumn.Name = "numDataGridViewTextBoxColumn";
            this.numDataGridViewTextBoxColumn.ReadOnly = true;
            this.numDataGridViewTextBoxColumn.Width = 66;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Название";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            this.nameDataGridViewTextBoxColumn.Width = 82;
            // 
            // dateDataGridViewTextBoxColumn
            // 
            this.dateDataGridViewTextBoxColumn.DataPropertyName = "Date";
            this.dateDataGridViewTextBoxColumn.HeaderText = "Date";
            this.dateDataGridViewTextBoxColumn.Name = "dateDataGridViewTextBoxColumn";
            this.dateDataGridViewTextBoxColumn.ReadOnly = true;
            this.dateDataGridViewTextBoxColumn.Visible = false;
            this.dateDataGridViewTextBoxColumn.Width = 55;
            // 
            // marginDataGridViewTextBoxColumn
            // 
            this.marginDataGridViewTextBoxColumn.DataPropertyName = "Margin";
            this.marginDataGridViewTextBoxColumn.HeaderText = "Margin";
            this.marginDataGridViewTextBoxColumn.Name = "marginDataGridViewTextBoxColumn";
            this.marginDataGridViewTextBoxColumn.ReadOnly = true;
            this.marginDataGridViewTextBoxColumn.Visible = false;
            this.marginDataGridViewTextBoxColumn.Width = 64;
            // 
            // sumDataGridViewTextBoxColumn
            // 
            this.sumDataGridViewTextBoxColumn.DataPropertyName = "Sum";
            this.sumDataGridViewTextBoxColumn.HeaderText = "Sum";
            this.sumDataGridViewTextBoxColumn.Name = "sumDataGridViewTextBoxColumn";
            this.sumDataGridViewTextBoxColumn.ReadOnly = true;
            this.sumDataGridViewTextBoxColumn.Visible = false;
            this.sumDataGridViewTextBoxColumn.Width = 53;
            // 
            // projectStatusIDDataGridViewTextBoxColumn
            // 
            this.projectStatusIDDataGridViewTextBoxColumn.DataPropertyName = "ProjectStatusID";
            this.projectStatusIDDataGridViewTextBoxColumn.HeaderText = "ProjectStatusID";
            this.projectStatusIDDataGridViewTextBoxColumn.Name = "projectStatusIDDataGridViewTextBoxColumn";
            this.projectStatusIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.projectStatusIDDataGridViewTextBoxColumn.Visible = false;
            this.projectStatusIDDataGridViewTextBoxColumn.Width = 106;
            // 
            // tableProjectsBindingSource
            // 
            this.tableProjectsBindingSource.DataMember = "Table_Projects";
            this.tableProjectsBindingSource.DataSource = this.cA_DB_DataSet;
            // 
            // cA_DB_DataSet
            // 
            this.cA_DB_DataSet.DataSetName = "CA_DB_DataSet";
            this.cA_DB_DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // table_ProjectsTableAdapter
            // 
            this.table_ProjectsTableAdapter.ClearBeforeFill = true;
            // 
            // table_MoneyFlowBindingSource
            // 
            this.table_MoneyFlowBindingSource.DataMember = "FK_Table_MoneyMoves_Table_Projects";
            this.table_MoneyFlowBindingSource.DataSource = this.tableProjectsBindingSource;
            // 
            // table_MoneyFlowTableAdapter
            // 
            this.table_MoneyFlowTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Table_CompaniesTableAdapter = null;
            this.tableAdapterManager.Table_CompanyTypesTableAdapter = null;
            this.tableAdapterManager.Table_EmployeesTableAdapter = null;
            this.tableAdapterManager.Table_HistoryTableAdapter = null;
            this.tableAdapterManager.Table_MaterialGroupsTableAdapter = null;
            this.tableAdapterManager.Table_MaterialsTableAdapter = null;
            this.tableAdapterManager.Table_MeasureUnitsTableAdapter = null;
            this.tableAdapterManager.Table_MoneyFlowTableAdapter = this.table_MoneyFlowTableAdapter;
            this.tableAdapterManager.Table_ProductionStagesTableAdapter = null;
            this.tableAdapterManager.Table_ProjectFilesTableAdapter = null;
            this.tableAdapterManager.Table_ProjectsTableAdapter = null;
            this.tableAdapterManager.Table_ProjectStatusesTableAdapter = null;
            this.tableAdapterManager.Table_SectionsTableAdapter = null;
            this.tableAdapterManager.Table_SubProjectsTableAdapter = null;
            this.tableAdapterManager.Table_TEOMaterialsTableAdapter = null;
            this.tableAdapterManager.Table_UsersTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = CA.CA_DB_DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // table_MoneyFlowDataGridView
            // 
            this.table_MoneyFlowDataGridView.AllowUserToAddRows = false;
            this.table_MoneyFlowDataGridView.AllowUserToDeleteRows = false;
            this.table_MoneyFlowDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.table_MoneyFlowDataGridView.AutoGenerateColumns = false;
            this.table_MoneyFlowDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_MoneyFlowDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MoneyFlowID,
            this.MoneyFlowState,
            this.MoneyFlowProjectID,
            this.MoneyFlowDate,
            this.MoneyFlowSum,
            this.MoneyFlowIsCash,
            this.MoneyFlowEmployeeID,
            this.MoneyFlowMaterialID,
            this.MoneyFlowCommentID});
            this.table_MoneyFlowDataGridView.DataSource = this.table_MoneyFlowBindingSource;
            this.table_MoneyFlowDataGridView.Location = new System.Drawing.Point(0, 28);
            this.table_MoneyFlowDataGridView.MultiSelect = false;
            this.table_MoneyFlowDataGridView.Name = "table_MoneyFlowDataGridView";
            this.table_MoneyFlowDataGridView.Size = new System.Drawing.Size(923, 195);
            this.table_MoneyFlowDataGridView.TabIndex = 2;
            this.table_MoneyFlowDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.table_MoneyFlowDataGridView_CellBeginEdit);
            this.table_MoneyFlowDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.table_MoneyFlowDataGridView_CellClick);
            this.table_MoneyFlowDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.table_MoneyFlowDataGridView_CellValueChanged);
            // 
            // MoneyFlowID
            // 
            this.MoneyFlowID.DataPropertyName = "id";
            this.MoneyFlowID.HeaderText = "id";
            this.MoneyFlowID.Name = "MoneyFlowID";
            this.MoneyFlowID.Visible = false;
            this.MoneyFlowID.Width = 40;
            // 
            // MoneyFlowState
            // 
            this.MoneyFlowState.DataPropertyName = "State";
            this.MoneyFlowState.HeaderText = "State";
            this.MoneyFlowState.Name = "MoneyFlowState";
            this.MoneyFlowState.Visible = false;
            // 
            // MoneyFlowProjectID
            // 
            this.MoneyFlowProjectID.DataPropertyName = "ProjectID";
            this.MoneyFlowProjectID.DataSource = this.tableProjectsBindingSource1;
            this.MoneyFlowProjectID.DisplayMember = "Name";
            this.MoneyFlowProjectID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.MoneyFlowProjectID.HeaderText = "Проект";
            this.MoneyFlowProjectID.Name = "MoneyFlowProjectID";
            this.MoneyFlowProjectID.ReadOnly = true;
            this.MoneyFlowProjectID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.MoneyFlowProjectID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.MoneyFlowProjectID.ValueMember = "id";
            this.MoneyFlowProjectID.Width = 69;
            // 
            // tableProjectsBindingSource1
            // 
            this.tableProjectsBindingSource1.DataMember = "Table_Projects";
            this.tableProjectsBindingSource1.DataSource = this.cA_DB_DataSet;
            // 
            // MoneyFlowDate
            // 
            this.MoneyFlowDate.DataPropertyName = "Date";
            this.MoneyFlowDate.HeaderText = "Дата";
            this.MoneyFlowDate.Name = "MoneyFlowDate";
            this.MoneyFlowDate.Width = 58;
            // 
            // MoneyFlowSum
            // 
            this.MoneyFlowSum.DataPropertyName = "Sum";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "C2";
            dataGridViewCellStyle1.NullValue = null;
            this.MoneyFlowSum.DefaultCellStyle = dataGridViewCellStyle1;
            this.MoneyFlowSum.HeaderText = "Сумма";
            this.MoneyFlowSum.Name = "MoneyFlowSum";
            this.MoneyFlowSum.Width = 66;
            // 
            // MoneyFlowIsCash
            // 
            this.MoneyFlowIsCash.DataPropertyName = "IsCash";
            this.MoneyFlowIsCash.HeaderText = "Наличные";
            this.MoneyFlowIsCash.Name = "MoneyFlowIsCash";
            this.MoneyFlowIsCash.Width = 64;
            // 
            // MoneyFlowEmployeeID
            // 
            this.MoneyFlowEmployeeID.DataPropertyName = "EmployeeID";
            this.MoneyFlowEmployeeID.DataSource = this.tableEmployeesBindingSource;
            this.MoneyFlowEmployeeID.DisplayMember = "LastName";
            this.MoneyFlowEmployeeID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.MoneyFlowEmployeeID.HeaderText = "Сотрудник";
            this.MoneyFlowEmployeeID.Name = "MoneyFlowEmployeeID";
            this.MoneyFlowEmployeeID.ReadOnly = true;
            this.MoneyFlowEmployeeID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.MoneyFlowEmployeeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.MoneyFlowEmployeeID.ValueMember = "id";
            this.MoneyFlowEmployeeID.Width = 85;
            // 
            // tableEmployeesBindingSource
            // 
            this.tableEmployeesBindingSource.DataMember = "Table_Employees";
            this.tableEmployeesBindingSource.DataSource = this.cA_DB_DataSet;
            // 
            // MoneyFlowMaterialID
            // 
            this.MoneyFlowMaterialID.DataPropertyName = "MaterialID";
            this.MoneyFlowMaterialID.DataSource = this.tableMaterialsBindingSource;
            this.MoneyFlowMaterialID.DisplayMember = "Name";
            this.MoneyFlowMaterialID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.MoneyFlowMaterialID.HeaderText = "Материал";
            this.MoneyFlowMaterialID.Name = "MoneyFlowMaterialID";
            this.MoneyFlowMaterialID.ReadOnly = true;
            this.MoneyFlowMaterialID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.MoneyFlowMaterialID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.MoneyFlowMaterialID.ValueMember = "id";
            this.MoneyFlowMaterialID.Width = 82;
            // 
            // tableMaterialsBindingSource
            // 
            this.tableMaterialsBindingSource.DataMember = "Table_Materials";
            this.tableMaterialsBindingSource.DataSource = this.cA_DB_DataSet;
            // 
            // MoneyFlowCommentID
            // 
            this.MoneyFlowCommentID.DataPropertyName = "Comment";
            this.MoneyFlowCommentID.HeaderText = "Комментарий";
            this.MoneyFlowCommentID.Name = "MoneyFlowCommentID";
            this.MoneyFlowCommentID.Width = 102;
            // 
            // table_EmployeesTableAdapter
            // 
            this.table_EmployeesTableAdapter.ClearBeforeFill = true;
            // 
            // table_MaterialsTableAdapter
            // 
            this.table_MaterialsTableAdapter.ClearBeforeFill = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.bindingNavigator2);
            this.splitContainer1.Panel1.Controls.Add(this.table_ProjectsDataGridView);
            this.splitContainer1.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.bindingNavigator1);
            this.splitContainer1.Panel2.Controls.Add(this.table_MoneyFlowDataGridView);
            this.splitContainer1.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.splitContainer1.Size = new System.Drawing.Size(927, 359);
            this.splitContainer1.SplitterDistance = 130;
            this.splitContainer1.TabIndex = 2;
            // 
            // bindingNavigator2
            // 
            this.bindingNavigator2.AddNewItem = this.bindingNavigatorAddNewItem1;
            this.bindingNavigator2.BindingSource = this.tableProjectsBindingSource;
            this.bindingNavigator2.CountItem = this.bindingNavigatorCountItem1;
            this.bindingNavigator2.CountItemFormat = "из {0}";
            this.bindingNavigator2.DeleteItem = this.bindingNavigatorDeleteItem1;
            this.bindingNavigator2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel2,
            this.toolStripSeparator2,
            this.bindingNavigatorMoveFirstItem1,
            this.bindingNavigatorMovePreviousItem1,
            this.bindingNavigatorSeparator3,
            this.bindingNavigatorPositionItem1,
            this.bindingNavigatorCountItem1,
            this.bindingNavigatorSeparator4,
            this.bindingNavigatorMoveNextItem1,
            this.bindingNavigatorMoveLastItem1,
            this.bindingNavigatorSeparator5,
            this.bindingNavigatorAddNewItem1,
            this.bindingNavigatorDeleteItem1,
            this.toolStripSeparator5,
            this.toolStripButton_ToExcel1,
            this.toolStripSeparator6});
            this.bindingNavigator2.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator2.MoveFirstItem = this.bindingNavigatorMoveFirstItem1;
            this.bindingNavigator2.MoveLastItem = this.bindingNavigatorMoveLastItem1;
            this.bindingNavigator2.MoveNextItem = this.bindingNavigatorMoveNextItem1;
            this.bindingNavigator2.MovePreviousItem = this.bindingNavigatorMovePreviousItem1;
            this.bindingNavigator2.Name = "bindingNavigator2";
            this.bindingNavigator2.PositionItem = this.bindingNavigatorPositionItem1;
            this.bindingNavigator2.Size = new System.Drawing.Size(923, 28);
            this.bindingNavigator2.TabIndex = 1;
            this.bindingNavigator2.Text = "bindingNavigator2";
            // 
            // bindingNavigatorAddNewItem1
            // 
            this.bindingNavigatorAddNewItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem1.Image")));
            this.bindingNavigatorAddNewItem1.Name = "bindingNavigatorAddNewItem1";
            this.bindingNavigatorAddNewItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorAddNewItem1.Text = "Добавить";
            this.bindingNavigatorAddNewItem1.Visible = false;
            // 
            // bindingNavigatorCountItem1
            // 
            this.bindingNavigatorCountItem1.Name = "bindingNavigatorCountItem1";
            this.bindingNavigatorCountItem1.Size = new System.Drawing.Size(36, 25);
            this.bindingNavigatorCountItem1.Text = "из {0}";
            this.bindingNavigatorCountItem1.ToolTipText = "Общее число элементов";
            // 
            // bindingNavigatorDeleteItem1
            // 
            this.bindingNavigatorDeleteItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem1.Image")));
            this.bindingNavigatorDeleteItem1.Name = "bindingNavigatorDeleteItem1";
            this.bindingNavigatorDeleteItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorDeleteItem1.Text = "Удалить";
            this.bindingNavigatorDeleteItem1.Visible = false;
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(221, 25);
            this.toolStripLabel2.Text = "Заказы:                             ";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveFirstItem1
            // 
            this.bindingNavigatorMoveFirstItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem1.Image")));
            this.bindingNavigatorMoveFirstItem1.Name = "bindingNavigatorMoveFirstItem1";
            this.bindingNavigatorMoveFirstItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveFirstItem1.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem1
            // 
            this.bindingNavigatorMovePreviousItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem1.Image")));
            this.bindingNavigatorMovePreviousItem1.Name = "bindingNavigatorMovePreviousItem1";
            this.bindingNavigatorMovePreviousItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMovePreviousItem1.Text = "Переместить назад";
            // 
            // bindingNavigatorSeparator3
            // 
            this.bindingNavigatorSeparator3.Name = "bindingNavigatorSeparator3";
            this.bindingNavigatorSeparator3.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorPositionItem1
            // 
            this.bindingNavigatorPositionItem1.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem1.AutoSize = false;
            this.bindingNavigatorPositionItem1.Name = "bindingNavigatorPositionItem1";
            this.bindingNavigatorPositionItem1.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem1.Text = "0";
            this.bindingNavigatorPositionItem1.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorSeparator4
            // 
            this.bindingNavigatorSeparator4.Name = "bindingNavigatorSeparator4";
            this.bindingNavigatorSeparator4.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveNextItem1
            // 
            this.bindingNavigatorMoveNextItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem1.Image")));
            this.bindingNavigatorMoveNextItem1.Name = "bindingNavigatorMoveNextItem1";
            this.bindingNavigatorMoveNextItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveNextItem1.Text = "Переместить вперед";
            // 
            // bindingNavigatorMoveLastItem1
            // 
            this.bindingNavigatorMoveLastItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem1.Image")));
            this.bindingNavigatorMoveLastItem1.Name = "bindingNavigatorMoveLastItem1";
            this.bindingNavigatorMoveLastItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveLastItem1.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator5
            // 
            this.bindingNavigatorSeparator5.Name = "bindingNavigatorSeparator5";
            this.bindingNavigatorSeparator5.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButton_ToExcel1
            // 
            this.toolStripButton_ToExcel1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton_ToExcel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_ToExcel1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_ToExcel1.Image")));
            this.toolStripButton_ToExcel1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_ToExcel1.Name = "toolStripButton_ToExcel1";
            this.toolStripButton_ToExcel1.Size = new System.Drawing.Size(46, 25);
            this.toolStripButton_ToExcel1.Text = "в Excel";
            this.toolStripButton_ToExcel1.Click += new System.EventHandler(this.toolStripButton_ToExcel1_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = this.bindingNavigatorAddNewItem;
            this.bindingNavigator1.BindingSource = this.table_MoneyFlowBindingSource;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator1.CountItemFormat = "из {0}";
            this.bindingNavigator1.DeleteItem = this.bindingNavigatorDeleteItem;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.toolStripSeparator1,
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.toolStripButtonAdd,
            this.toolStripSeparator3,
            this.toolStripButtonDelete,
            this.toolStripSeparator4,
            this.toolStripSeparator7,
            this.toolStripButton_ToExcel2,
            this.toolStripSeparator8});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator1.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bindingNavigator1.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bindingNavigator1.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigator1.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator1.Size = new System.Drawing.Size(923, 28);
            this.bindingNavigator1.TabIndex = 1;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorAddNewItem.Text = "Добавить";
            this.bindingNavigatorAddNewItem.Visible = false;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(36, 25);
            this.bindingNavigatorCountItem.Text = "из {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Общее число элементов";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorDeleteItem.Text = "Удалить";
            this.bindingNavigatorDeleteItem.Visible = false;
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(221, 25);
            this.toolStripLabel1.Text = "Расходы и поступления:";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveFirstItem.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMovePreviousItem.Text = "Переместить назад";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveNextItem.Text = "Переместить вперед";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveLastItem.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonAdd
            // 
            this.toolStripButtonAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAdd.Image")));
            this.toolStripButtonAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAdd.Name = "toolStripButtonAdd";
            this.toolStripButtonAdd.Size = new System.Drawing.Size(63, 25);
            this.toolStripButtonAdd.Text = "Добавить";
            this.toolStripButtonAdd.Click += new System.EventHandler(this.toolStripButtonAdd_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonDelete
            // 
            this.toolStripButtonDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonDelete.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDelete.Image")));
            this.toolStripButtonDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDelete.Name = "toolStripButtonDelete";
            this.toolStripButtonDelete.Size = new System.Drawing.Size(55, 25);
            this.toolStripButtonDelete.Text = "Удалить";
            this.toolStripButtonDelete.Click += new System.EventHandler(this.toolStripButtonDelete_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButton_ToExcel2
            // 
            this.toolStripButton_ToExcel2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton_ToExcel2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_ToExcel2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_ToExcel2.Image")));
            this.toolStripButton_ToExcel2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_ToExcel2.Name = "toolStripButton_ToExcel2";
            this.toolStripButton_ToExcel2.Size = new System.Drawing.Size(46, 25);
            this.toolStripButton_ToExcel2.Text = "в Excel";
            this.toolStripButton_ToExcel2.Click += new System.EventHandler(this.toolStripButton_ToExcel2_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 28);
            // 
            // cA_DB_DataSet1
            // 
            this.cA_DB_DataSet1.DataSetName = "CA_DB_DataSet";
            this.cA_DB_DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // Form_MoneyFlow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(927, 381);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form_MoneyFlow";
            this.Text = "Расходы и поступления";
            this.Load += new System.EventHandler(this.Form_MoneyFlow_Load);
            this.Controls.SetChildIndex(this.splitContainer1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.table_ProjectsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableProjectsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cA_DB_DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_MoneyFlowBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_MoneyFlowDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableProjectsBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableEmployeesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableMaterialsBindingSource)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator2)).EndInit();
            this.bindingNavigator2.ResumeLayout(false);
            this.bindingNavigator2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cA_DB_DataSet1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView table_ProjectsDataGridView;
        private CA_DB_DataSet cA_DB_DataSet;
        private System.Windows.Forms.BindingSource tableProjectsBindingSource;
        private CA_DB_DataSetTableAdapters.Table_ProjectsTableAdapter table_ProjectsTableAdapter;
        private System.Windows.Forms.BindingSource table_MoneyFlowBindingSource;
        private CA_DB_DataSetTableAdapters.Table_MoneyFlowTableAdapter table_MoneyFlowTableAdapter;
        private CA_DB_DataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView table_MoneyFlowDataGridView;
        private System.Windows.Forms.BindingSource tableEmployeesBindingSource;
        private CA_DB_DataSetTableAdapters.Table_EmployeesTableAdapter table_EmployeesTableAdapter;
        private System.Windows.Forms.BindingSource tableMaterialsBindingSource;
        private CA_DB_DataSetTableAdapters.Table_MaterialsTableAdapter table_MaterialsTableAdapter;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.BindingSource tableProjectsBindingSource1;
        private CA_DB_DataSet cA_DB_DataSet1;
        private System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.BindingNavigator bindingNavigator2;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem1;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator3;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator4;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator5;
        private System.Windows.Forms.ToolStripButton toolStripButtonDelete;
        private System.Windows.Forms.ToolStripButton toolStripButtonAdd;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectState;
        private System.Windows.Forms.DataGridViewTextBoxColumn companyIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn commentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn marginDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectStatusIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton toolStripButton_ToExcel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton toolStripButton_ToExcel2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.DataGridViewTextBoxColumn MoneyFlowID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MoneyFlowState;
        private System.Windows.Forms.DataGridViewComboBoxColumn MoneyFlowProjectID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MoneyFlowDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn MoneyFlowSum;
        private System.Windows.Forms.DataGridViewCheckBoxColumn MoneyFlowIsCash;
        private System.Windows.Forms.DataGridViewComboBoxColumn MoneyFlowEmployeeID;
        private System.Windows.Forms.DataGridViewComboBoxColumn MoneyFlowMaterialID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MoneyFlowCommentID;
    }
}