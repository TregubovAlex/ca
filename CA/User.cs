﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA
{
    static class User
    {
        static Guid _ID;
        static string _FullName;
        public static Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public static string FullName
        {
            get { return _FullName; }
            set { _FullName = value; }
        }
    }
}