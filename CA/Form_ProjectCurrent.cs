﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace CA
{
    public partial class Form_ProjectCurrent : Form_BaseStyle
    {
        private Guid currID;
        ButtonSelect buttonSelect = new ButtonSelect();
        public override void BS_Fill()
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_ProjectFiles". При необходимости она может быть перемещена или удалена.
            this.table_ProjectFilesTableAdapter.Fill(this.cA_DB_DataSet.Table_ProjectFiles);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_Users". При необходимости она может быть перемещена или удалена.
            this.table_UsersTableAdapter.Fill(this.cA_DB_DataSet.Table_Users);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_ProductionStages". При необходимости она может быть перемещена или удалена.
            this.table_ProductionStagesTableAdapter.Fill(this.cA_DB_DataSet.Table_ProductionStages);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_Employees". При необходимости она может быть перемещена или удалена.
            this.table_EmployeesTableAdapter.Fill(this.cA_DB_DataSet.Table_Employees);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_MoneyFlow". При необходимости она может быть перемещена или удалена.
            this.table_MoneyFlowTableAdapter.Fill(this.cA_DB_DataSet.Table_MoneyFlow);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_Companies". При необходимости она может быть перемещена или удалена.
            this.table_CompaniesTableAdapter.Fill(this.cA_DB_DataSet.Table_Companies);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_ProjectStatuses". При необходимости она может быть перемещена или удалена.
            this.table_ProjectStatusesTableAdapter.Fill(this.cA_DB_DataSet.Table_ProjectStatuses);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_Materials". При необходимости она может быть перемещена или удалена.
            this.table_MaterialsTableAdapter.Fill(this.cA_DB_DataSet.Table_Materials);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_TEOMaterials". При необходимости она может быть перемещена или удалена.
            this.table_TEOMaterialsTableAdapter.Fill(this.cA_DB_DataSet.Table_TEOMaterials);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_SubProjects". При необходимости она может быть перемещена или удалена.
            this.table_SubProjectsTableAdapter.Fill(this.cA_DB_DataSet.Table_SubProjects);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_Projects". При необходимости она может быть перемещена или удалена.
            this.table_ProjectsTableAdapter.Fill(this.cA_DB_DataSet.Table_Projects);
        }
        public override void BS_Update()
        {
            if (!IsLoaded) return;
            this.table_ProjectsBindingSource.EndEdit();
            this.table_ProjectsTableAdapter.Adapter.Update(cA_DB_DataSet);
            this.table_SubProjectsBindingSource.EndEdit();
            this.table_SubProjectsTableAdapter.Adapter.Update(cA_DB_DataSet);
            this.table_TEOMaterialsBindingSource.EndEdit();
            this.table_TEOMaterialsTableAdapter.Adapter.Update(cA_DB_DataSet);
            this.table_MoneyFlowBindingSource.EndEdit();
            this.table_MoneyFlowTableAdapter.Adapter.Update(cA_DB_DataSet);
            this.table_ProductionStagesBindingSource.EndEdit();
            this.table_ProductionStagesTableAdapter.Adapter.Update(cA_DB_DataSet);
            this.table_ProjectFilesBindingSource.EndEdit();
            this.table_ProjectFilesTableAdapter.Adapter.Update(cA_DB_DataSet);
        }
        private void toolStripButtonAddSubProject_Click(object sender, EventArgs e)
        {
            IsLoaded = false;
            table_SubProjectsBindingSource.AddNew();
            DataGridViewRow newrow = table_SubProjectsDataGridView.Rows[table_SubProjectsDataGridView.Rows.Count - 1];
            newrow.Cells["SubProjectID"].Value = Guid.NewGuid();
            newrow.Cells["SubProjectState"].Value = "normal";
            IsLoaded = true;
            BS_Update();
            newrow.Cells["SubProjectName"].Selected = true;
        }
        private void toolStripButtonAddTEOMaterial_Click(object sender, EventArgs e)
        {
            IsLoaded = false;
            table_TEOMaterialsBindingSource.AddNew();
            DataGridViewRow newrow = table_TEOMaterialsDataGridView.Rows[table_TEOMaterialsDataGridView.Rows.Count - 1];
            newrow.Cells["TEOMaterialID"].Value = Guid.NewGuid();
            newrow.Cells["TEOMaterialState"].Value = "normal";
            newrow.Cells["TEOMaterialMaterialID"].Value = "448b05fc-b591-428e-8994-3518047d8339";//[ пусто ]
            newrow.Cells["TEOMaterialCompanyID"].Value = "f7422c37-6eda-43ea-874e-8cbfb37a161a";//[ пусто ]
            newrow.Cells["TEOMaterialPrice"].Value = 0;
            newrow.Cells["TEOMaterialCount"].Value = 0;
            IsLoaded = true;
            BS_Update();
            newrow.Cells["TEOMaterialMaterialID"].Selected = true;
        }
        private void toolStripButtonAddMoneyFlow_Click(object sender, EventArgs e)
        {
            IsLoaded = false;
            table_MoneyFlowBindingSource.AddNew();
            DataGridViewRow newrow = table_MoneyFlowDataGridView.Rows[table_MoneyFlowDataGridView.Rows.Count - 1];
            newrow.Cells["MoneyFlowID"].Value = Guid.NewGuid();
            newrow.Cells["MoneyFlowState"].Value = "normal";
            newrow.Cells["MoneyFlowDate"].Value = DateTime.Today;
            newrow.Cells["MoneyFlowIsCash"].Value = true;
            IsLoaded = true;
            BS_Update();
            newrow.Cells["MoneyFlowSum"].Selected = true;
        }
        private void toolStripButtonAddProductionStage_Click(object sender, EventArgs e)
        {
            IsLoaded = false;
            table_ProductionStagesBindingSource.AddNew();
            DataGridViewRow newrow = table_ProductionStagesDataGridView.Rows[table_ProductionStagesDataGridView.Rows.Count - 1];
            newrow.Cells["ProductionStageID"].Value = Guid.NewGuid();
            newrow.Cells["ProductionStageState"].Value = "normal";
            newrow.Cells["ProductionStageName"].Value = "пусто";
            DataTable dt = table_ProductionStagesTableAdapter.GetData();
            OrderedEnumerableRowCollection<DataRow> collection = dt.AsEnumerable()
                .Where(project => (project["ProjectID"].Equals(currID)))
                .OrderBy(r => r.Field<DateTime>("Date"));
            DateTime laststartdate = DateTime.Today;
            int lastduration = 0;
            if (collection.ToList().Count > 0)
            {
                laststartdate = collection
                    .Select(r => r.Field<DateTime>("Date"))
                    .Last();
                lastduration = collection
                    .Select(r => r.Field<int>("Duration"))
                    .Last();
            }
            newrow.Cells["ProductionStageStartDate"].Value = laststartdate.AddDays(lastduration);
            newrow.Cells["ProductionStageDuration"].Value = 1;
            IsLoaded = true;
            BS_Update();
            newrow.Cells["ProductionStageName"].Selected = true;
        }
        private void toolStripButtonAddFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                IsLoaded = false;
                //Связываем файловый поток с открываемым файлом
                FileStream fileStream = new FileStream(openFileDialog.FileName, FileMode.Open);
                //Создаем объект BinaryReader для чтения двоичных данных
                BinaryReader binaryReader = new BinaryReader(fileStream);
                //Объявляем переменную byte[], так как varbinary требует именно этот тип данных
                byte[] mas;
                //указали длинну массива
                mas = new byte[fileStream.Length];
                //в цикле считали побойтово файл
                for (int i = 0; i < fileStream.Length; i++)
                    mas[i] = binaryReader.ReadByte();
                //закрыли поток
                binaryReader.Close();
                fileStream.Close();
                //добавили запись
                //bindingSource.AddNew();
                //создали объект DataRow для работы со строкой таблицы
                //DataRow row = ((DataRowView)bindingSource.Current).Row;
                //в таблице есть поле file типа varbinary, пишем в него открытый нами файл
                //row["file"] = mas;
                //заканчиваем операцию редактирования
                //bindingSource.EndEdit();
                //обновляем БД
                //sqlDataAdapter1.Update(dataSet, "table1");
                table_ProjectFilesBindingSource.AddNew();
                DataGridViewRow newrow = table_ProjectFilesDataGridView.Rows[table_ProjectFilesDataGridView.Rows.Count - 1];
                newrow.Cells["FileID"].Value = Guid.NewGuid();
                newrow.Cells["FileState"].Value = "normal";
                newrow.Cells["FileName"].Value = System.IO.Path.GetFileNameWithoutExtension(openFileDialog.FileName);
                newrow.Cells["FileExtension"].Value = System.IO.Path.GetExtension(openFileDialog.FileName);
                newrow.Cells["FileData"].Value = mas;
                IsLoaded = true;
                BS_Update();
                newrow.Cells["FileComment"].Selected = true;
            }
        }
        private void toolStripButtonOpenFile_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = table_ProjectFilesDataGridView.CurrentRow;

            //создали файл
            string tempPath = System.IO.Path.GetTempPath();
            string tempFile = tempPath + row.Cells["FileName"].Value.ToString() + row.Cells["FileExtension"].Value.ToString();

            FileStream fs = new FileStream(tempFile, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);
            byte[] mas = (byte[])row.Cells["FileData"].Value;
            bw.Write(mas, 0, mas.Length);
            bw.Close();
            fs.Close();
            //System.IO.File.Delete(tempFile);
            Process p = new Process();
            p.StartInfo.WindowStyle = ProcessWindowStyle.Maximized;
            p.StartInfo.FileName = tempFile;
            p.StartInfo.WorkingDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            Process.Start(p.StartInfo);
        }
        private void table_SubProjectsDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            BS_dgvSpecialEdit(sender as DataGridView, e);
        }
        private void table_TEOMaterialsDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            BS_dgvSpecialEdit(sender as DataGridView, e);
        }
        private void table_MoneyFlowDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            BS_dgvSpecialEdit(sender as DataGridView, e);
        }
        private void table_ProductionStagesDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            BS_dgvSpecialEdit(sender as DataGridView, e);
        }
        private void table_ProjectFilesDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            BS_dgvSpecialEdit(sender as DataGridView, e);
        }
        private void table_SubProjectsDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            BS_Save();
        }
        private void table_TEOMaterialsDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            BS_Save();
        }
        private void table_MoneyFlowDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            BS_Save();
        }
        private void table_ProductionStagesDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            BS_Save();
        }
        private void table_ProjectFilesDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            BS_Save();
        }
        private void toolStripButtonDeleteSubProject_Click(object sender, EventArgs e)
        {
            /*Guid guidSubProjectID = (Guid)table_SubProjectsDataGridView.CurrentRow.Cells["SubProjectID"].Value;
            DataTable dt = table_TEOMaterialsTableAdapter.GetData();
            List<DataRow> list = dt.AsEnumerable()
                .Where(subproject => (subproject["SubProjectID"].Equals(guidSubProjectID)))
                .ToList<DataRow>();
            foreach (DataRow datarow in list)
            {
                DataRowView datarowview = dt.DefaultView[dt.Rows.IndexOf(datarow)]; ;
                table_TEOMaterialsBindingSource.Remove(datarowview);
            }*/
            foreach (object item in table_TEOMaterialsBindingSource.List)
                table_TEOMaterialsBindingSource.Remove(item);
            BS_Update();
            BS_DeleteSingleRow(table_SubProjectsDataGridView);
        }
        private void toolStripButtonDeleteTEOMaterial_Click(object sender, EventArgs e)
        {
            BS_DeleteSingleRow(table_TEOMaterialsDataGridView);
        }
        private void toolStripButtonDeleteMoneyFlow_Click(object sender, EventArgs e)
        {
            BS_DeleteSingleRow(table_MoneyFlowDataGridView);
        }
        private void toolStripButtonDeleteProductionStage_Click(object sender, EventArgs e)
        {
            BS_DeleteSingleRow(table_ProductionStagesDataGridView);
        }
        private void toolStripButtonDeleteFile_Click(object sender, EventArgs e)
        {
            BS_DeleteSingleRow(table_ProjectFilesDataGridView);
        }
        public Form_ProjectCurrent()
        { InitializeComponent(); }
        public Form_ProjectCurrent(Guid _currID)
        {
            InitializeComponent();
            table_ProjectsBindingSource.Filter = "id = '" + _currID.ToString() + "'";
            currID = _currID;
        }
        private void Form_ProjectCurrent_Load(object sender, EventArgs e)
        {
            BS_Refresh(table_SubProjectsDataGridView, table_TEOMaterialsDataGridView);
            tableSubProjectsBindingSource.Filter = "ProjectID = '" + currID.ToString() + "'";
            IsLoaded = true;
        }
        private void button_Refresh_Click(object sender, EventArgs e)
        {
            IsLoaded = false;
            BS_Refresh(table_SubProjectsDataGridView, table_TEOMaterialsDataGridView);
            IsLoaded = true;
        }
        private void sumNumericUpDown_DoubleClick(object sender, EventArgs e)
        {
            FormEdit_Calculator form = new FormEdit_Calculator(sumNumericUpDown.Text);
            form.ShowDialog();
            sumNumericUpDown.Text = form.Value.ToString("N2", CultureInfo.CreateSpecificCulture("ru").NumberFormat);
        }
        private void marginNumericUpDown_DoubleClick(object sender, EventArgs e)
        {
            FormEdit_Calculator form = new FormEdit_Calculator(marginNumericUpDown.Text);
            form.ShowDialog();
            marginNumericUpDown.Text = form.Value.ToString("N1", CultureInfo.CreateSpecificCulture("ru").NumberFormat);
        }
        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            MoneyFlowID.Visible = false;
            table_MoneyFlowDataGridView.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
            foreach (DataGridViewColumn col in table_MoneyFlowDataGridView.Columns)
            {
                int size = col.Width;
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                col.Width = size;
            }
            ProductionStageID.Visible = false;
            table_ProductionStagesDataGridView.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
            foreach (DataGridViewColumn col in table_ProductionStagesDataGridView.Columns)
            {
                int size = col.Width;
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                col.Width = size;
            }
            FileID.Visible = false;
            table_ProjectFilesDataGridView.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
            foreach (DataGridViewColumn col in table_ProjectFilesDataGridView.Columns)
            {
                int size = col.Width;
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                col.Width = size;
            }
        }
        private void Form_ProjectCurrent_FormClosing(object sender, FormClosingEventArgs e)
        {
            BS_Update();
        }
        private void button_Save_Click(object sender, EventArgs e)
        {
            BS_Update();
        }
        private void button_TEO_to_Excel_Click(object sender, EventArgs e)
        {
            Excel.Application exApp = new Excel.Application();
            Excel.Workbook exWorkBook = exApp.Workbooks.Open("F:\\Мой рабочий стол\\Template_TEO.xltm",
                Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            //exApp.Workbooks.Add();
            Excel.Worksheet exSheet = exWorkBook.Sheets[1];
            exSheet.Name = "ТЭО";
            MessageBox.Show(exSheet.Name);


            exApp.Visible = true;
            //exApp = null;
        }
        private void table_SubProjectsDataGridView_CurrentCellChanged(object sender, EventArgs e)
        {
            BS_TuneUpDataGridViews(sender);
        }
        private void toolStripButton_ToExcel1_Click(object sender, EventArgs e)
        {
            BS_ToExcel(table_SubProjectsDataGridView);
        }
        private void toolStripButton_ToExcel2_Click(object sender, EventArgs e)
        {
            BS_ToExcel(table_TEOMaterialsDataGridView);
        }
        private void toolStripButton_ToExcel3_Click(object sender, EventArgs e)
        {
            BS_ToExcel(table_MoneyFlowDataGridView);
        }
        private void toolStripButton_ToExcel4_Click(object sender, EventArgs e)
        {
            BS_ToExcel(table_ProductionStagesDataGridView);
        }
        private void toolStripButton_ToExcel5_Click(object sender, EventArgs e)
        {
            BS_ToExcel(table_ProjectFilesDataGridView);
        }
        private void table_TEOMaterialsDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (table_TEOMaterialsDataGridView.Columns["TEOMaterialMaterialID"].Index == e.ColumnIndex)
                buttonSelect.Configure(new Form_Materials(true, table_TEOMaterialsDataGridView[e.ColumnIndex, e.RowIndex].Value.ToString()), sender, e.ColumnIndex, e.RowIndex);
            else if (table_TEOMaterialsDataGridView.Columns["TEOMaterialCompanyID"].Index == e.ColumnIndex)
                buttonSelect.Configure(new Form_Companies(true, false, table_TEOMaterialsDataGridView[e.ColumnIndex, e.RowIndex].Value.ToString()), sender, e.ColumnIndex, e.RowIndex);
        }
        private void companyComboBox_Click(object sender, EventArgs e)
        {
            buttonSelect.Configure(new Form_Companies(true, false, (sender as ComboBox).SelectedValue.ToString()), sender);
        }
        private void table_MoneyFlowDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (table_MoneyFlowDataGridView.Columns["MoneyFlowMaterialID"].Index == e.ColumnIndex)
                buttonSelect.Configure(new Form_Materials(true, table_MoneyFlowDataGridView[e.ColumnIndex, e.RowIndex].Value.ToString()), sender, e.ColumnIndex, e.RowIndex);
            else if (table_MoneyFlowDataGridView.Columns["MoneyFlowEmployeeID"].Index == e.ColumnIndex)
                buttonSelect.Configure(new Form_Companies(true, true, table_MoneyFlowDataGridView[e.ColumnIndex, e.RowIndex].Value.ToString()), sender, e.ColumnIndex, e.RowIndex);
            else if (table_MoneyFlowDataGridView.Columns["MoneyFlowProjectID"].Index == e.ColumnIndex)
                buttonSelect.Configure(new Form_Projects(true, table_MoneyFlowDataGridView[e.ColumnIndex, e.RowIndex].Value.ToString()), sender, e.ColumnIndex, e.RowIndex);
        }
        private void table_ProjectFilesDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (table_ProjectFilesDataGridView.Columns["FileProjectID"].Index == e.ColumnIndex)
                buttonSelect.Configure(new Form_Projects(true, table_ProjectFilesDataGridView[e.ColumnIndex, e.RowIndex].Value.ToString()), sender, e.ColumnIndex, e.RowIndex);
        }


    }
}