﻿namespace CA
{
    partial class Form_Companies: Form_BaseStyle 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Companies));
            this.table_CompanyTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cA_DB_DataSet = new CA.CA_DB_DataSet();
            this.table_CompanyTypesTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_CompanyTypesTableAdapter();
            this.tableAdapterManager = new CA.CA_DB_DataSetTableAdapters.TableAdapterManager();
            this.table_CompaniesTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_CompaniesTableAdapter();
            this.tableCompanyTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.table_CompaniesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.table_CompanyTypesDataGridView = new System.Windows.Forms.DataGridView();
            this.CompanyTypeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyTypeState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyTypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.table_CompanyTypesBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.table_CompanyTypesBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonAddCompanyType = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonDeleteType = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_ToExcel = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.table_CompaniesDataGridView = new System.Windows.Forms.DataGridView();
            this.CompanyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyCompanyTypeID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.CompanyNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem1 = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveFirstItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem1 = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAddCompany = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigator2 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem2 = new System.Windows.Forms.ToolStripButton();
            this.table_EmployeesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorCountItem2 = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveFirstItem2 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem2 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem2 = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem2 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem2 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAddEmployee = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonDeleteEmployee = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.table_EmployeesDataGridView = new System.Windows.Forms.DataGridView();
            this.EmployeeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployeeState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployeeCompanyID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tableCompaniesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.EmployeeLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployeeFirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployeeMiddleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployeePhones = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployeeEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployeeAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployeeComment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.table_EmployeesTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_EmployeesTableAdapter();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_ToExcel2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_ToExcel3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.table_CompanyTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cA_DB_DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableCompanyTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_CompaniesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_CompanyTypesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_CompanyTypesBindingNavigator)).BeginInit();
            this.table_CompanyTypesBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_CompaniesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator2)).BeginInit();
            this.bindingNavigator2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_EmployeesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_EmployeesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableCompaniesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // table_CompanyTypesBindingSource
            // 
            this.table_CompanyTypesBindingSource.DataMember = "Table_CompanyTypes";
            this.table_CompanyTypesBindingSource.DataSource = this.cA_DB_DataSet;
            this.table_CompanyTypesBindingSource.Filter = "Name <> \'[ пусто ]\'";
            this.table_CompanyTypesBindingSource.Sort = "Name";
            // 
            // cA_DB_DataSet
            // 
            this.cA_DB_DataSet.DataSetName = "CA_DB_DataSet";
            this.cA_DB_DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // table_CompanyTypesTableAdapter
            // 
            this.table_CompanyTypesTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Table_CompaniesTableAdapter = this.table_CompaniesTableAdapter;
            this.tableAdapterManager.Table_CompanyTypesTableAdapter = this.table_CompanyTypesTableAdapter;
            this.tableAdapterManager.Table_EmployeesTableAdapter = null;
            this.tableAdapterManager.Table_HistoryTableAdapter = null;
            this.tableAdapterManager.Table_MaterialGroupsTableAdapter = null;
            this.tableAdapterManager.Table_MaterialsTableAdapter = null;
            this.tableAdapterManager.Table_MeasureUnitsTableAdapter = null;
            this.tableAdapterManager.Table_MoneyFlowTableAdapter = null;
            this.tableAdapterManager.Table_ProductionStagesTableAdapter = null;
            this.tableAdapterManager.Table_ProjectFilesTableAdapter = null;
            this.tableAdapterManager.Table_ProjectsTableAdapter = null;
            this.tableAdapterManager.Table_ProjectStatusesTableAdapter = null;
            this.tableAdapterManager.Table_SectionsTableAdapter = null;
            this.tableAdapterManager.Table_SubProjectsTableAdapter = null;
            this.tableAdapterManager.Table_TEOMaterialsTableAdapter = null;
            this.tableAdapterManager.Table_UsersTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = CA.CA_DB_DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // table_CompaniesTableAdapter
            // 
            this.table_CompaniesTableAdapter.ClearBeforeFill = true;
            // 
            // tableCompanyTypesBindingSource
            // 
            this.tableCompanyTypesBindingSource.DataMember = "Table_CompanyTypes";
            this.tableCompanyTypesBindingSource.DataSource = this.cA_DB_DataSet;
            // 
            // table_CompaniesBindingSource
            // 
            this.table_CompaniesBindingSource.DataMember = "FK_Table_Companies_Table_CompanyTypes";
            this.table_CompaniesBindingSource.DataSource = this.table_CompanyTypesBindingSource;
            this.table_CompaniesBindingSource.Sort = "Num";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.table_CompanyTypesDataGridView);
            this.splitContainer1.Panel1.Controls.Add(this.table_CompanyTypesBindingNavigator);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(965, 665);
            this.splitContainer1.SplitterDistance = 198;
            this.splitContainer1.TabIndex = 0;
            // 
            // table_CompanyTypesDataGridView
            // 
            this.table_CompanyTypesDataGridView.AllowUserToAddRows = false;
            this.table_CompanyTypesDataGridView.AllowUserToDeleteRows = false;
            this.table_CompanyTypesDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.table_CompanyTypesDataGridView.AutoGenerateColumns = false;
            this.table_CompanyTypesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_CompanyTypesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CompanyTypeID,
            this.CompanyTypeState,
            this.CompanyTypeName});
            this.table_CompanyTypesDataGridView.DataSource = this.table_CompanyTypesBindingSource;
            this.table_CompanyTypesDataGridView.Location = new System.Drawing.Point(0, 28);
            this.table_CompanyTypesDataGridView.MultiSelect = false;
            this.table_CompanyTypesDataGridView.Name = "table_CompanyTypesDataGridView";
            this.table_CompanyTypesDataGridView.Size = new System.Drawing.Size(961, 167);
            this.table_CompanyTypesDataGridView.TabIndex = 0;
            this.table_CompanyTypesDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.table_CompanyTypesDataGridView_CellBeginEdit);
            this.table_CompanyTypesDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.table_CompanyTypesDataGridView_CellValueChanged);
            this.table_CompanyTypesDataGridView.CurrentCellChanged += new System.EventHandler(this.table_CompanyTypesDataGridView_CurrentCellChanged);
            // 
            // CompanyTypeID
            // 
            this.CompanyTypeID.DataPropertyName = "id";
            this.CompanyTypeID.HeaderText = "id";
            this.CompanyTypeID.Name = "CompanyTypeID";
            this.CompanyTypeID.Visible = false;
            this.CompanyTypeID.Width = 40;
            // 
            // CompanyTypeState
            // 
            this.CompanyTypeState.DataPropertyName = "State";
            this.CompanyTypeState.HeaderText = "State";
            this.CompanyTypeState.Name = "CompanyTypeState";
            this.CompanyTypeState.Visible = false;
            // 
            // CompanyTypeName
            // 
            this.CompanyTypeName.DataPropertyName = "Name";
            this.CompanyTypeName.HeaderText = "Тип организации";
            this.CompanyTypeName.Name = "CompanyTypeName";
            this.CompanyTypeName.Width = 340;
            // 
            // table_CompanyTypesBindingNavigator
            // 
            this.table_CompanyTypesBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.table_CompanyTypesBindingNavigator.BindingSource = this.table_CompanyTypesBindingSource;
            this.table_CompanyTypesBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.table_CompanyTypesBindingNavigator.CountItemFormat = "из {0}";
            this.table_CompanyTypesBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.table_CompanyTypesBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.toolStripSeparator1,
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.table_CompanyTypesBindingNavigatorSaveItem,
            this.toolStripButtonAddCompanyType,
            this.toolStripSeparator8,
            this.toolStripButtonDeleteType,
            this.toolStripSeparator9,
            this.toolStripSeparator10,
            this.toolStripButton_ToExcel,
            this.toolStripSeparator11});
            this.table_CompanyTypesBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.table_CompanyTypesBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.table_CompanyTypesBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.table_CompanyTypesBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.table_CompanyTypesBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.table_CompanyTypesBindingNavigator.Name = "table_CompanyTypesBindingNavigator";
            this.table_CompanyTypesBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.table_CompanyTypesBindingNavigator.Size = new System.Drawing.Size(961, 28);
            this.table_CompanyTypesBindingNavigator.TabIndex = 1;
            this.table_CompanyTypesBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorAddNewItem.Text = "Добавить";
            this.bindingNavigatorAddNewItem.Visible = false;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(36, 25);
            this.bindingNavigatorCountItem.Text = "из {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Общее число элементов";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorDeleteItem.Text = "Удалить";
            this.bindingNavigatorDeleteItem.Visible = false;
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(197, 25);
            this.toolStripLabel1.Text = "Группы организаций:";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveFirstItem.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMovePreviousItem.Text = "Переместить назад";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveNextItem.Text = "Переместить вперед";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveLastItem.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 28);
            // 
            // table_CompanyTypesBindingNavigatorSaveItem
            // 
            this.table_CompanyTypesBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.table_CompanyTypesBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("table_CompanyTypesBindingNavigatorSaveItem.Image")));
            this.table_CompanyTypesBindingNavigatorSaveItem.Name = "table_CompanyTypesBindingNavigatorSaveItem";
            this.table_CompanyTypesBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 25);
            this.table_CompanyTypesBindingNavigatorSaveItem.Text = "Сохранить данные";
            this.table_CompanyTypesBindingNavigatorSaveItem.Visible = false;
            // 
            // toolStripButtonAddCompanyType
            // 
            this.toolStripButtonAddCompanyType.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonAddCompanyType.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAddCompanyType.Image")));
            this.toolStripButtonAddCompanyType.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAddCompanyType.Name = "toolStripButtonAddCompanyType";
            this.toolStripButtonAddCompanyType.Size = new System.Drawing.Size(63, 25);
            this.toolStripButtonAddCompanyType.Text = "Добавить";
            this.toolStripButtonAddCompanyType.Click += new System.EventHandler(this.toolStripButtonAddCompanyType_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonDeleteType
            // 
            this.toolStripButtonDeleteType.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripButtonDeleteType.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonDeleteType.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDeleteType.Image")));
            this.toolStripButtonDeleteType.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDeleteType.Name = "toolStripButtonDeleteType";
            this.toolStripButtonDeleteType.Size = new System.Drawing.Size(55, 25);
            this.toolStripButtonDeleteType.Text = "Удалить";
            this.toolStripButtonDeleteType.Click += new System.EventHandler(this.toolStripButtonDeleteType_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButton_ToExcel
            // 
            this.toolStripButton_ToExcel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton_ToExcel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_ToExcel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_ToExcel.Image")));
            this.toolStripButton_ToExcel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_ToExcel.Name = "toolStripButton_ToExcel";
            this.toolStripButton_ToExcel.Size = new System.Drawing.Size(46, 25);
            this.toolStripButton_ToExcel.Text = "в Excel";
            this.toolStripButton_ToExcel.Click += new System.EventHandler(this.toolStripButton_ToExcel_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 28);
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.AutoScroll = true;
            this.splitContainer2.Panel1.Controls.Add(this.table_CompaniesDataGridView);
            this.splitContainer2.Panel1.Controls.Add(this.bindingNavigator1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.AutoScroll = true;
            this.splitContainer2.Panel2.Controls.Add(this.bindingNavigator2);
            this.splitContainer2.Panel2.Controls.Add(this.table_EmployeesDataGridView);
            this.splitContainer2.Size = new System.Drawing.Size(965, 463);
            this.splitContainer2.SplitterDistance = 292;
            this.splitContainer2.TabIndex = 0;
            // 
            // table_CompaniesDataGridView
            // 
            this.table_CompaniesDataGridView.AllowUserToAddRows = false;
            this.table_CompaniesDataGridView.AllowUserToDeleteRows = false;
            this.table_CompaniesDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.table_CompaniesDataGridView.AutoGenerateColumns = false;
            this.table_CompaniesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_CompaniesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CompanyID,
            this.CompanyState,
            this.CompanyCompanyTypeID,
            this.CompanyNum,
            this.CompanyName,
            this.CompanyDate});
            this.table_CompaniesDataGridView.DataSource = this.table_CompaniesBindingSource;
            this.table_CompaniesDataGridView.Location = new System.Drawing.Point(0, 28);
            this.table_CompaniesDataGridView.MultiSelect = false;
            this.table_CompaniesDataGridView.Name = "table_CompaniesDataGridView";
            this.table_CompaniesDataGridView.Size = new System.Drawing.Size(961, 260);
            this.table_CompaniesDataGridView.TabIndex = 0;
            this.table_CompaniesDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.table_CompaniesDataGridView_CellBeginEdit);
            this.table_CompaniesDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.table_CompaniesDataGridView_CellValueChanged);
            this.table_CompaniesDataGridView.CurrentCellChanged += new System.EventHandler(this.table_CompaniesDataGridView_CurrentCellChanged);
            // 
            // CompanyID
            // 
            this.CompanyID.DataPropertyName = "id";
            this.CompanyID.HeaderText = "id";
            this.CompanyID.Name = "CompanyID";
            this.CompanyID.Visible = false;
            this.CompanyID.Width = 40;
            // 
            // CompanyState
            // 
            this.CompanyState.DataPropertyName = "State";
            this.CompanyState.HeaderText = "State";
            this.CompanyState.Name = "CompanyState";
            this.CompanyState.Visible = false;
            // 
            // CompanyCompanyTypeID
            // 
            this.CompanyCompanyTypeID.DataPropertyName = "CompanyTypeID";
            this.CompanyCompanyTypeID.DataSource = this.tableCompanyTypesBindingSource;
            this.CompanyCompanyTypeID.DisplayMember = "Name";
            this.CompanyCompanyTypeID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.CompanyCompanyTypeID.HeaderText = "Тип организации";
            this.CompanyCompanyTypeID.Name = "CompanyCompanyTypeID";
            this.CompanyCompanyTypeID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CompanyCompanyTypeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.CompanyCompanyTypeID.ValueMember = "id";
            this.CompanyCompanyTypeID.Width = 109;
            // 
            // CompanyNum
            // 
            this.CompanyNum.DataPropertyName = "Num";
            this.CompanyNum.HeaderText = "№";
            this.CompanyNum.Name = "CompanyNum";
            this.CompanyNum.Width = 43;
            // 
            // CompanyName
            // 
            this.CompanyName.DataPropertyName = "Name";
            this.CompanyName.HeaderText = "Название";
            this.CompanyName.Name = "CompanyName";
            this.CompanyName.Width = 82;
            // 
            // CompanyDate
            // 
            this.CompanyDate.DataPropertyName = "Date";
            this.CompanyDate.HeaderText = "Дата вноса";
            this.CompanyDate.Name = "CompanyDate";
            this.CompanyDate.Width = 84;
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = this.bindingNavigatorAddNewItem1;
            this.bindingNavigator1.BindingSource = this.table_CompaniesBindingSource;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem1;
            this.bindingNavigator1.CountItemFormat = "из {0}";
            this.bindingNavigator1.DeleteItem = this.bindingNavigatorDeleteItem1;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel2,
            this.toolStripSeparator2,
            this.bindingNavigatorMoveFirstItem1,
            this.bindingNavigatorMovePreviousItem1,
            this.bindingNavigatorSeparator3,
            this.bindingNavigatorPositionItem1,
            this.bindingNavigatorCountItem1,
            this.bindingNavigatorSeparator4,
            this.bindingNavigatorMoveNextItem1,
            this.bindingNavigatorMoveLastItem1,
            this.bindingNavigatorSeparator5,
            this.bindingNavigatorAddNewItem1,
            this.bindingNavigatorDeleteItem1,
            this.toolStripButtonAddCompany,
            this.toolStripSeparator6,
            this.toolStripButtonDelete,
            this.toolStripSeparator7,
            this.toolStripSeparator12,
            this.toolStripButton_ToExcel2,
            this.toolStripSeparator13});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator1.MoveFirstItem = this.bindingNavigatorMoveFirstItem1;
            this.bindingNavigator1.MoveLastItem = this.bindingNavigatorMoveLastItem1;
            this.bindingNavigator1.MoveNextItem = this.bindingNavigatorMoveNextItem1;
            this.bindingNavigator1.MovePreviousItem = this.bindingNavigatorMovePreviousItem1;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem1;
            this.bindingNavigator1.Size = new System.Drawing.Size(961, 28);
            this.bindingNavigator1.TabIndex = 1;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem1
            // 
            this.bindingNavigatorAddNewItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem1.Image")));
            this.bindingNavigatorAddNewItem1.Name = "bindingNavigatorAddNewItem1";
            this.bindingNavigatorAddNewItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorAddNewItem1.Text = "Добавить";
            this.bindingNavigatorAddNewItem1.Visible = false;
            // 
            // bindingNavigatorCountItem1
            // 
            this.bindingNavigatorCountItem1.Name = "bindingNavigatorCountItem1";
            this.bindingNavigatorCountItem1.Size = new System.Drawing.Size(36, 25);
            this.bindingNavigatorCountItem1.Text = "из {0}";
            this.bindingNavigatorCountItem1.ToolTipText = "Общее число элементов";
            // 
            // bindingNavigatorDeleteItem1
            // 
            this.bindingNavigatorDeleteItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem1.Image")));
            this.bindingNavigatorDeleteItem1.Name = "bindingNavigatorDeleteItem1";
            this.bindingNavigatorDeleteItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorDeleteItem1.Text = "Удалить";
            this.bindingNavigatorDeleteItem1.Visible = false;
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(196, 25);
            this.toolStripLabel2.Text = "Организации:             ";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveFirstItem1
            // 
            this.bindingNavigatorMoveFirstItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem1.Image")));
            this.bindingNavigatorMoveFirstItem1.Name = "bindingNavigatorMoveFirstItem1";
            this.bindingNavigatorMoveFirstItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveFirstItem1.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem1
            // 
            this.bindingNavigatorMovePreviousItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem1.Image")));
            this.bindingNavigatorMovePreviousItem1.Name = "bindingNavigatorMovePreviousItem1";
            this.bindingNavigatorMovePreviousItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMovePreviousItem1.Text = "Переместить назад";
            // 
            // bindingNavigatorSeparator3
            // 
            this.bindingNavigatorSeparator3.Name = "bindingNavigatorSeparator3";
            this.bindingNavigatorSeparator3.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorPositionItem1
            // 
            this.bindingNavigatorPositionItem1.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem1.AutoSize = false;
            this.bindingNavigatorPositionItem1.Name = "bindingNavigatorPositionItem1";
            this.bindingNavigatorPositionItem1.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem1.Text = "0";
            this.bindingNavigatorPositionItem1.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorSeparator4
            // 
            this.bindingNavigatorSeparator4.Name = "bindingNavigatorSeparator4";
            this.bindingNavigatorSeparator4.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveNextItem1
            // 
            this.bindingNavigatorMoveNextItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem1.Image")));
            this.bindingNavigatorMoveNextItem1.Name = "bindingNavigatorMoveNextItem1";
            this.bindingNavigatorMoveNextItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveNextItem1.Text = "Переместить вперед";
            // 
            // bindingNavigatorMoveLastItem1
            // 
            this.bindingNavigatorMoveLastItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem1.Image")));
            this.bindingNavigatorMoveLastItem1.Name = "bindingNavigatorMoveLastItem1";
            this.bindingNavigatorMoveLastItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveLastItem1.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator5
            // 
            this.bindingNavigatorSeparator5.Name = "bindingNavigatorSeparator5";
            this.bindingNavigatorSeparator5.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonAddCompany
            // 
            this.toolStripButtonAddCompany.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonAddCompany.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAddCompany.Image")));
            this.toolStripButtonAddCompany.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAddCompany.Name = "toolStripButtonAddCompany";
            this.toolStripButtonAddCompany.Size = new System.Drawing.Size(63, 25);
            this.toolStripButtonAddCompany.Text = "Добавить";
            this.toolStripButtonAddCompany.Click += new System.EventHandler(this.toolStripButtonAddCompany_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonDelete
            // 
            this.toolStripButtonDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonDelete.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDelete.Image")));
            this.toolStripButtonDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDelete.Name = "toolStripButtonDelete";
            this.toolStripButtonDelete.Size = new System.Drawing.Size(55, 25);
            this.toolStripButtonDelete.Text = "Удалить";
            this.toolStripButtonDelete.Click += new System.EventHandler(this.toolStripButtonDelete_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigator2
            // 
            this.bindingNavigator2.AddNewItem = this.bindingNavigatorAddNewItem2;
            this.bindingNavigator2.BindingSource = this.table_EmployeesBindingSource;
            this.bindingNavigator2.CountItem = this.bindingNavigatorCountItem2;
            this.bindingNavigator2.DeleteItem = this.bindingNavigatorDeleteItem2;
            this.bindingNavigator2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel3,
            this.toolStripSeparator3,
            this.bindingNavigatorMoveFirstItem2,
            this.bindingNavigatorMovePreviousItem2,
            this.bindingNavigatorSeparator6,
            this.bindingNavigatorPositionItem2,
            this.bindingNavigatorCountItem2,
            this.bindingNavigatorSeparator7,
            this.bindingNavigatorMoveNextItem2,
            this.bindingNavigatorMoveLastItem2,
            this.bindingNavigatorSeparator8,
            this.bindingNavigatorAddNewItem2,
            this.bindingNavigatorDeleteItem2,
            this.toolStripButtonAddEmployee,
            this.toolStripSeparator4,
            this.toolStripButtonDeleteEmployee,
            this.toolStripSeparator5,
            this.toolStripSeparator14,
            this.toolStripButton_ToExcel3,
            this.toolStripSeparator15});
            this.bindingNavigator2.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator2.MoveFirstItem = this.bindingNavigatorMoveFirstItem2;
            this.bindingNavigator2.MoveLastItem = this.bindingNavigatorMoveLastItem2;
            this.bindingNavigator2.MoveNextItem = this.bindingNavigatorMoveNextItem2;
            this.bindingNavigator2.MovePreviousItem = this.bindingNavigatorMovePreviousItem2;
            this.bindingNavigator2.Name = "bindingNavigator2";
            this.bindingNavigator2.PositionItem = this.bindingNavigatorPositionItem2;
            this.bindingNavigator2.Size = new System.Drawing.Size(961, 28);
            this.bindingNavigator2.TabIndex = 1;
            this.bindingNavigator2.Text = "bindingNavigator2";
            // 
            // bindingNavigatorAddNewItem2
            // 
            this.bindingNavigatorAddNewItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem2.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem2.Image")));
            this.bindingNavigatorAddNewItem2.Name = "bindingNavigatorAddNewItem2";
            this.bindingNavigatorAddNewItem2.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem2.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorAddNewItem2.Text = "Добавить";
            this.bindingNavigatorAddNewItem2.Visible = false;
            // 
            // table_EmployeesBindingSource
            // 
            this.table_EmployeesBindingSource.DataMember = "FK_Table_Employees_Table_Companies";
            this.table_EmployeesBindingSource.DataSource = this.table_CompaniesBindingSource;
            // 
            // bindingNavigatorCountItem2
            // 
            this.bindingNavigatorCountItem2.Name = "bindingNavigatorCountItem2";
            this.bindingNavigatorCountItem2.Size = new System.Drawing.Size(43, 25);
            this.bindingNavigatorCountItem2.Text = "для {0}";
            this.bindingNavigatorCountItem2.ToolTipText = "Общее число элементов";
            // 
            // bindingNavigatorDeleteItem2
            // 
            this.bindingNavigatorDeleteItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem2.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem2.Image")));
            this.bindingNavigatorDeleteItem2.Name = "bindingNavigatorDeleteItem2";
            this.bindingNavigatorDeleteItem2.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem2.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorDeleteItem2.Text = "Удалить";
            this.bindingNavigatorDeleteItem2.Visible = false;
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(199, 25);
            this.toolStripLabel3.Text = "Сотрудники:                ";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveFirstItem2
            // 
            this.bindingNavigatorMoveFirstItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem2.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem2.Image")));
            this.bindingNavigatorMoveFirstItem2.Name = "bindingNavigatorMoveFirstItem2";
            this.bindingNavigatorMoveFirstItem2.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem2.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveFirstItem2.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem2
            // 
            this.bindingNavigatorMovePreviousItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem2.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem2.Image")));
            this.bindingNavigatorMovePreviousItem2.Name = "bindingNavigatorMovePreviousItem2";
            this.bindingNavigatorMovePreviousItem2.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem2.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMovePreviousItem2.Text = "Переместить назад";
            // 
            // bindingNavigatorSeparator6
            // 
            this.bindingNavigatorSeparator6.Name = "bindingNavigatorSeparator6";
            this.bindingNavigatorSeparator6.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorPositionItem2
            // 
            this.bindingNavigatorPositionItem2.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem2.AutoSize = false;
            this.bindingNavigatorPositionItem2.Name = "bindingNavigatorPositionItem2";
            this.bindingNavigatorPositionItem2.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem2.Text = "0";
            this.bindingNavigatorPositionItem2.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorSeparator7
            // 
            this.bindingNavigatorSeparator7.Name = "bindingNavigatorSeparator7";
            this.bindingNavigatorSeparator7.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveNextItem2
            // 
            this.bindingNavigatorMoveNextItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem2.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem2.Image")));
            this.bindingNavigatorMoveNextItem2.Name = "bindingNavigatorMoveNextItem2";
            this.bindingNavigatorMoveNextItem2.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem2.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveNextItem2.Text = "Переместить вперед";
            // 
            // bindingNavigatorMoveLastItem2
            // 
            this.bindingNavigatorMoveLastItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem2.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem2.Image")));
            this.bindingNavigatorMoveLastItem2.Name = "bindingNavigatorMoveLastItem2";
            this.bindingNavigatorMoveLastItem2.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem2.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveLastItem2.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator8
            // 
            this.bindingNavigatorSeparator8.Name = "bindingNavigatorSeparator8";
            this.bindingNavigatorSeparator8.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonAddEmployee
            // 
            this.toolStripButtonAddEmployee.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonAddEmployee.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAddEmployee.Image")));
            this.toolStripButtonAddEmployee.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAddEmployee.Name = "toolStripButtonAddEmployee";
            this.toolStripButtonAddEmployee.Size = new System.Drawing.Size(63, 25);
            this.toolStripButtonAddEmployee.Text = "Добавить";
            this.toolStripButtonAddEmployee.Click += new System.EventHandler(this.toolStripButtonAddEmployee_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonDeleteEmployee
            // 
            this.toolStripButtonDeleteEmployee.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonDeleteEmployee.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDeleteEmployee.Image")));
            this.toolStripButtonDeleteEmployee.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDeleteEmployee.Name = "toolStripButtonDeleteEmployee";
            this.toolStripButtonDeleteEmployee.Size = new System.Drawing.Size(55, 25);
            this.toolStripButtonDeleteEmployee.Text = "Удалить";
            this.toolStripButtonDeleteEmployee.Click += new System.EventHandler(this.toolStripButtonDeleteEmployee_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 28);
            // 
            // table_EmployeesDataGridView
            // 
            this.table_EmployeesDataGridView.AllowUserToAddRows = false;
            this.table_EmployeesDataGridView.AllowUserToDeleteRows = false;
            this.table_EmployeesDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.table_EmployeesDataGridView.AutoGenerateColumns = false;
            this.table_EmployeesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_EmployeesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EmployeeID,
            this.EmployeeState,
            this.EmployeeCompanyID,
            this.EmployeeLastName,
            this.EmployeeFirstName,
            this.EmployeeMiddleName,
            this.EmployeePhones,
            this.EmployeeEmail,
            this.EmployeeAddress,
            this.EmployeeComment});
            this.table_EmployeesDataGridView.DataSource = this.table_EmployeesBindingSource;
            this.table_EmployeesDataGridView.Location = new System.Drawing.Point(0, 28);
            this.table_EmployeesDataGridView.MultiSelect = false;
            this.table_EmployeesDataGridView.Name = "table_EmployeesDataGridView";
            this.table_EmployeesDataGridView.Size = new System.Drawing.Size(961, 135);
            this.table_EmployeesDataGridView.TabIndex = 0;
            this.table_EmployeesDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.table_EmployeesDataGridView_CellBeginEdit);
            this.table_EmployeesDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.table_EmployeesDataGridView_CellValueChanged);
            // 
            // EmployeeID
            // 
            this.EmployeeID.DataPropertyName = "id";
            this.EmployeeID.HeaderText = "id";
            this.EmployeeID.Name = "EmployeeID";
            this.EmployeeID.Visible = false;
            // 
            // EmployeeState
            // 
            this.EmployeeState.DataPropertyName = "State";
            this.EmployeeState.HeaderText = "State";
            this.EmployeeState.Name = "EmployeeState";
            this.EmployeeState.Visible = false;
            // 
            // EmployeeCompanyID
            // 
            this.EmployeeCompanyID.DataPropertyName = "CompanyID";
            this.EmployeeCompanyID.DataSource = this.tableCompaniesBindingSource;
            this.EmployeeCompanyID.DisplayMember = "Name";
            this.EmployeeCompanyID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.EmployeeCompanyID.HeaderText = "Организация";
            this.EmployeeCompanyID.Name = "EmployeeCompanyID";
            this.EmployeeCompanyID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.EmployeeCompanyID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.EmployeeCompanyID.ValueMember = "id";
            // 
            // tableCompaniesBindingSource
            // 
            this.tableCompaniesBindingSource.DataMember = "Table_Companies";
            this.tableCompaniesBindingSource.DataSource = this.cA_DB_DataSet;
            // 
            // EmployeeLastName
            // 
            this.EmployeeLastName.DataPropertyName = "LastName";
            this.EmployeeLastName.HeaderText = "Фамилия";
            this.EmployeeLastName.Name = "EmployeeLastName";
            // 
            // EmployeeFirstName
            // 
            this.EmployeeFirstName.DataPropertyName = "FirstName";
            this.EmployeeFirstName.HeaderText = "Имя";
            this.EmployeeFirstName.Name = "EmployeeFirstName";
            // 
            // EmployeeMiddleName
            // 
            this.EmployeeMiddleName.DataPropertyName = "MiddleName";
            this.EmployeeMiddleName.HeaderText = "Отчество";
            this.EmployeeMiddleName.Name = "EmployeeMiddleName";
            // 
            // EmployeePhones
            // 
            this.EmployeePhones.DataPropertyName = "Phones";
            this.EmployeePhones.HeaderText = "Телефоны";
            this.EmployeePhones.Name = "EmployeePhones";
            // 
            // EmployeeEmail
            // 
            this.EmployeeEmail.DataPropertyName = "Email";
            this.EmployeeEmail.HeaderText = "Email";
            this.EmployeeEmail.Name = "EmployeeEmail";
            // 
            // EmployeeAddress
            // 
            this.EmployeeAddress.DataPropertyName = "Address";
            this.EmployeeAddress.HeaderText = "Адрес";
            this.EmployeeAddress.Name = "EmployeeAddress";
            // 
            // EmployeeComment
            // 
            this.EmployeeComment.DataPropertyName = "Comment";
            this.EmployeeComment.HeaderText = "Комментарий";
            this.EmployeeComment.Name = "EmployeeComment";
            // 
            // table_EmployeesTableAdapter
            // 
            this.table_EmployeesTableAdapter.ClearBeforeFill = true;
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButton_ToExcel2
            // 
            this.toolStripButton_ToExcel2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton_ToExcel2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_ToExcel2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_ToExcel2.Image")));
            this.toolStripButton_ToExcel2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_ToExcel2.Name = "toolStripButton_ToExcel2";
            this.toolStripButton_ToExcel2.Size = new System.Drawing.Size(46, 25);
            this.toolStripButton_ToExcel2.Text = "в Excel";
            this.toolStripButton_ToExcel2.Click += new System.EventHandler(this.toolStripButton_ToExcel2_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButton_ToExcel3
            // 
            this.toolStripButton_ToExcel3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton_ToExcel3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_ToExcel3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_ToExcel3.Image")));
            this.toolStripButton_ToExcel3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_ToExcel3.Name = "toolStripButton_ToExcel3";
            this.toolStripButton_ToExcel3.Size = new System.Drawing.Size(46, 25);
            this.toolStripButton_ToExcel3.Text = "в Excel";
            this.toolStripButton_ToExcel3.Click += new System.EventHandler(this.toolStripButton_ToExcel3_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(6, 28);
            // 
            // Form_Companies
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(965, 687);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form_Companies";
            this.Text = "Организации и сотрудники";
            this.Load += new System.EventHandler(this.Form_Companies_Load);
            this.Controls.SetChildIndex(this.splitContainer1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.table_CompanyTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cA_DB_DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableCompanyTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_CompaniesBindingSource)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.table_CompanyTypesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_CompanyTypesBindingNavigator)).EndInit();
            this.table_CompanyTypesBindingNavigator.ResumeLayout(false);
            this.table_CompanyTypesBindingNavigator.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.table_CompaniesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator2)).EndInit();
            this.bindingNavigator2.ResumeLayout(false);
            this.bindingNavigator2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_EmployeesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_EmployeesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableCompaniesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private CA_DB_DataSet cA_DB_DataSet;
        private System.Windows.Forms.BindingSource table_CompanyTypesBindingSource;
        private CA_DB_DataSetTableAdapters.Table_CompanyTypesTableAdapter table_CompanyTypesTableAdapter;
        private CA_DB_DataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator table_CompanyTypesBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton table_CompanyTypesBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView table_CompanyTypesDataGridView;
        private CA_DB_DataSetTableAdapters.Table_CompaniesTableAdapter table_CompaniesTableAdapter;
        private System.Windows.Forms.BindingSource table_CompaniesBindingSource;
        private System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem1;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator3;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator4;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator5;
        private System.Windows.Forms.DataGridView table_CompaniesDataGridView;
        private System.Windows.Forms.BindingSource tableCompanyTypesBindingSource;
        private System.Windows.Forms.ToolStripButton toolStripButtonDelete;
        private System.Windows.Forms.ToolStripButton toolStripButtonDeleteType;
        private System.Windows.Forms.ToolStripButton toolStripButtonAddCompanyType;
        private System.Windows.Forms.ToolStripButton toolStripButtonAddCompany;
        private System.Windows.Forms.BindingSource table_EmployeesBindingSource;
        private CA_DB_DataSetTableAdapters.Table_EmployeesTableAdapter table_EmployeesTableAdapter;
        private System.Windows.Forms.BindingNavigator bindingNavigator2;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem2;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem2;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem2;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem2;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem2;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator6;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem2;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator7;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem2;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem2;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator8;
        private System.Windows.Forms.ToolStripButton toolStripButtonAddEmployee;
        private System.Windows.Forms.ToolStripButton toolStripButtonDeleteEmployee;
        private System.Windows.Forms.DataGridView table_EmployeesDataGridView;
        private System.Windows.Forms.BindingSource tableCompaniesBindingSource;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyTypeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyTypeState;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyTypeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyState;
        private System.Windows.Forms.DataGridViewComboBoxColumn CompanyCompanyTypeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeState;
        private System.Windows.Forms.DataGridViewComboBoxColumn EmployeeCompanyID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeLastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeFirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeMiddleName;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeePhones;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeComment;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripButton toolStripButton_ToExcel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripButton toolStripButton_ToExcel2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripButton toolStripButton_ToExcel3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
    }
}