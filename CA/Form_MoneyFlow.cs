﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CA
{
    public partial class Form_MoneyFlow : Form_BaseStyle
    {
        ButtonSelect buttonSelect = new ButtonSelect();
        public override void BS_Fill()
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_Materials". При необходимости она может быть перемещена или удалена.
            this.table_MaterialsTableAdapter.Fill(this.cA_DB_DataSet.Table_Materials);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_Employees". При необходимости она может быть перемещена или удалена.
            this.table_EmployeesTableAdapter.Fill(this.cA_DB_DataSet.Table_Employees);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_MoneyFlow". При необходимости она может быть перемещена или удалена.
            this.table_MoneyFlowTableAdapter.Fill(this.cA_DB_DataSet.Table_MoneyFlow);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_Projects". При необходимости она может быть перемещена или удалена.
            this.table_ProjectsTableAdapter.Fill(this.cA_DB_DataSet.Table_Projects);
        }
        public override void BS_Update()
        {
            if (!IsLoaded) return;
            this.table_MoneyFlowBindingSource.EndEdit();
            this.table_MoneyFlowTableAdapter.Adapter.Update(cA_DB_DataSet);
            this.tableProjectsBindingSource.EndEdit();
            this.table_ProjectsTableAdapter.Adapter.Update(cA_DB_DataSet);
        }
        private void toolStripButtonAdd_Click(object sender, EventArgs e)
        {
            IsLoaded = false;
            table_MoneyFlowBindingSource.AddNew();
            DataGridViewRow newrow = table_MoneyFlowDataGridView.Rows[table_MoneyFlowDataGridView.Rows.Count - 1];
            newrow.Cells["MoneyFlowID"].Value = Guid.NewGuid();
            newrow.Cells["MoneyFlowState"].Value = "normal";
            newrow.Cells["MoneyFlowDate"].Value = DateTime.Today;
            newrow.Cells["MoneyFlowIsCash"].Value = true;
            IsLoaded = true;
            BS_Update();
            newrow.Cells["MoneyFlowSum"].Selected = true;
        }
        private void table_MoneyFlowDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            BS_dgvSpecialEdit(sender as DataGridView, e);
        }
        private void table_MoneyFlowDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            BS_Save();
        }
        private void toolStripButtonDelete_Click(object sender, EventArgs e)
        {
            BS_DeleteSingleRow(table_MoneyFlowDataGridView);
        }
        public Form_MoneyFlow()
        { InitializeComponent(); }
        private void Form_MoneyFlow_Load(object sender, EventArgs e)
        {
            BS_Refresh(table_ProjectsDataGridView, table_MoneyFlowDataGridView);
            IsLoaded = true;
        }
        private void button_Refresh_Click(object sender, EventArgs e)
        {
            BS_Refresh(table_ProjectsDataGridView, table_MoneyFlowDataGridView);
        }
        private void table_ProjectsDataGridView_CurrentCellChanged(object sender, EventArgs e)
        {
            BS_TuneUpDataGridViews(sender);
        }
        private void toolStripButton_ToExcel1_Click(object sender, EventArgs e)
        {
            BS_ToExcel(table_ProjectsDataGridView);
        }
        private void toolStripButton_ToExcel2_Click(object sender, EventArgs e)
        {
            BS_ToExcel(table_MoneyFlowDataGridView);
        }
        private void table_MoneyFlowDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (table_MoneyFlowDataGridView.Columns["MoneyFlowMaterialID"].Index == e.ColumnIndex)
                buttonSelect.Configure(new Form_Materials(true, table_MoneyFlowDataGridView[e.ColumnIndex, e.RowIndex].Value.ToString()), sender, e.ColumnIndex, e.RowIndex);
            else if (table_MoneyFlowDataGridView.Columns["MoneyFlowEmployeeID"].Index == e.ColumnIndex)
                buttonSelect.Configure(new Form_Companies(true, true, table_MoneyFlowDataGridView[e.ColumnIndex, e.RowIndex].Value.ToString()), sender, e.ColumnIndex, e.RowIndex);
            else if (table_MoneyFlowDataGridView.Columns["MoneyFlowProjectID"].Index == e.ColumnIndex)
                buttonSelect.Configure(new Form_Projects(true, table_MoneyFlowDataGridView[e.ColumnIndex, e.RowIndex].Value.ToString()), sender, e.ColumnIndex, e.RowIndex);
        }
    }
}