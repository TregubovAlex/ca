﻿namespace CA
{
    partial class Form_Projects : Form_BaseStyle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Projects));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.table_ProjectsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cA_DB_DataSet = new CA.CA_DB_DataSet();
            this.tableCompaniesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tableProjectStatusesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.table_ProjectsTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_ProjectsTableAdapter();
            this.tableAdapterManager = new CA.CA_DB_DataSetTableAdapters.TableAdapterManager();
            this.table_CompaniesTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_CompaniesTableAdapter();
            this.table_ProjectStatusesTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_ProjectStatusesTableAdapter();
            this.table_ProjectsDataGridView = new System.Windows.Forms.DataGridView();
            this.tableUsersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.table_ProjectsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.table_ProjectsBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonOpen = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_ToExcel = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.table_ProductionStagesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.table_ProductionStagesTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_ProductionStagesTableAdapter();
            this.table_MoneyFlowBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.table_MoneyFlowTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_MoneyFlowTableAdapter();
            this.table_SubProjectsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.table_SubProjectsTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_SubProjectsTableAdapter();
            this.table_TEOMaterialsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.table_TEOMaterialsTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_TEOMaterialsTableAdapter();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox_ShowByTypes = new System.Windows.Forms.GroupBox();
            this.checkBox_1 = new System.Windows.Forms.CheckBox();
            this.checkBox_3 = new System.Windows.Forms.CheckBox();
            this.checkBox_2 = new System.Windows.Forms.CheckBox();
            this.checkBox_5 = new System.Windows.Forms.CheckBox();
            this.checkBox_4 = new System.Windows.Forms.CheckBox();
            this.table_UsersTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_UsersTableAdapter();
            this.ProjectID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectCompanyID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ProjectMargin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectSum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectStatusID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ManagerID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ProjectComment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.State = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.table_ProjectsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cA_DB_DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableCompaniesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableProjectStatusesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_ProjectsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableUsersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_ProjectsBindingNavigator)).BeginInit();
            this.table_ProjectsBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_ProductionStagesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_MoneyFlowBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_SubProjectsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_TEOMaterialsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox_ShowByTypes.SuspendLayout();
            this.SuspendLayout();
            // 
            // table_ProjectsBindingSource
            // 
            this.table_ProjectsBindingSource.DataMember = "Table_Projects";
            this.table_ProjectsBindingSource.DataSource = this.cA_DB_DataSet;
            this.table_ProjectsBindingSource.Filter = "Name <> \'[ пусто ]\'";
            this.table_ProjectsBindingSource.Sort = "Num";
            // 
            // cA_DB_DataSet
            // 
            this.cA_DB_DataSet.DataSetName = "CA_DB_DataSet";
            this.cA_DB_DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableCompaniesBindingSource
            // 
            this.tableCompaniesBindingSource.DataMember = "Table_Companies";
            this.tableCompaniesBindingSource.DataSource = this.cA_DB_DataSet;
            this.tableCompaniesBindingSource.Filter = "";
            this.tableCompaniesBindingSource.Sort = "Name";
            // 
            // tableProjectStatusesBindingSource
            // 
            this.tableProjectStatusesBindingSource.DataMember = "Table_ProjectStatuses";
            this.tableProjectStatusesBindingSource.DataSource = this.cA_DB_DataSet;
            this.tableProjectStatusesBindingSource.Filter = "";
            this.tableProjectStatusesBindingSource.Sort = "Num";
            // 
            // table_ProjectsTableAdapter
            // 
            this.table_ProjectsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Table_CompaniesTableAdapter = this.table_CompaniesTableAdapter;
            this.tableAdapterManager.Table_CompanyTypesTableAdapter = null;
            this.tableAdapterManager.Table_EmployeesTableAdapter = null;
            this.tableAdapterManager.Table_HistoryTableAdapter = null;
            this.tableAdapterManager.Table_MaterialGroupsTableAdapter = null;
            this.tableAdapterManager.Table_MaterialsTableAdapter = null;
            this.tableAdapterManager.Table_MeasureUnitsTableAdapter = null;
            this.tableAdapterManager.Table_MoneyFlowTableAdapter = null;
            this.tableAdapterManager.Table_ProductionStagesTableAdapter = null;
            this.tableAdapterManager.Table_ProjectFilesTableAdapter = null;
            this.tableAdapterManager.Table_ProjectsTableAdapter = this.table_ProjectsTableAdapter;
            this.tableAdapterManager.Table_ProjectStatusesTableAdapter = this.table_ProjectStatusesTableAdapter;
            this.tableAdapterManager.Table_SectionsTableAdapter = null;
            this.tableAdapterManager.Table_SubProjectsTableAdapter = null;
            this.tableAdapterManager.Table_TEOMaterialsTableAdapter = null;
            this.tableAdapterManager.Table_UsersTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = CA.CA_DB_DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // table_CompaniesTableAdapter
            // 
            this.table_CompaniesTableAdapter.ClearBeforeFill = true;
            // 
            // table_ProjectStatusesTableAdapter
            // 
            this.table_ProjectStatusesTableAdapter.ClearBeforeFill = true;
            // 
            // table_ProjectsDataGridView
            // 
            this.table_ProjectsDataGridView.AllowUserToAddRows = false;
            this.table_ProjectsDataGridView.AllowUserToDeleteRows = false;
            this.table_ProjectsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.table_ProjectsDataGridView.AutoGenerateColumns = false;
            this.table_ProjectsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_ProjectsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProjectID,
            this.ProjectNum,
            this.ProjectDate,
            this.ProjectName,
            this.ProjectCompanyID,
            this.ProjectMargin,
            this.ProjectSum,
            this.ProjectStatusID,
            this.ManagerID,
            this.ProjectComment,
            this.State});
            this.table_ProjectsDataGridView.DataSource = this.table_ProjectsBindingSource;
            this.table_ProjectsDataGridView.Location = new System.Drawing.Point(0, 28);
            this.table_ProjectsDataGridView.MultiSelect = false;
            this.table_ProjectsDataGridView.Name = "table_ProjectsDataGridView";
            this.table_ProjectsDataGridView.ReadOnly = true;
            this.table_ProjectsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.table_ProjectsDataGridView.Size = new System.Drawing.Size(717, 589);
            this.table_ProjectsDataGridView.TabIndex = 0;
            this.table_ProjectsDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.table_ProjectsDataGridView_CellBeginEdit);
            this.table_ProjectsDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.table_ProjectsDataGridView_CellDoubleClick);
            this.table_ProjectsDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.table_ProjectsDataGridView_CellValueChanged);
            this.table_ProjectsDataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.table_ProjectsDataGridView_KeyDown);
            // 
            // tableUsersBindingSource
            // 
            this.tableUsersBindingSource.DataMember = "Table_Users";
            this.tableUsersBindingSource.DataSource = this.cA_DB_DataSet;
            // 
            // table_ProjectsBindingNavigator
            // 
            this.table_ProjectsBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.table_ProjectsBindingNavigator.BindingSource = this.table_ProjectsBindingSource;
            this.table_ProjectsBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.table_ProjectsBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.table_ProjectsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.toolStripSeparator1,
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.table_ProjectsBindingNavigatorSaveItem,
            this.bindingNavigatorDeleteItem,
            this.toolStripButtonAdd,
            this.toolStripSeparator2,
            this.toolStripButtonOpen,
            this.toolStripSeparator3,
            this.toolStripButtonDelete,
            this.toolStripSeparator4,
            this.toolStripSeparator6,
            this.toolStripButton_ToExcel,
            this.toolStripSeparator5});
            this.table_ProjectsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.table_ProjectsBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.table_ProjectsBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.table_ProjectsBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.table_ProjectsBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.table_ProjectsBindingNavigator.Name = "table_ProjectsBindingNavigator";
            this.table_ProjectsBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.table_ProjectsBindingNavigator.Size = new System.Drawing.Size(717, 28);
            this.table_ProjectsBindingNavigator.TabIndex = 1;
            this.table_ProjectsBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorAddNewItem.Text = "Добавить";
            this.bindingNavigatorAddNewItem.Visible = false;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(43, 25);
            this.bindingNavigatorCountItem.Text = "для {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Общее число элементов";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorDeleteItem.Text = "Удалить";
            this.bindingNavigatorDeleteItem.Visible = false;
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(91, 25);
            this.toolStripLabel1.Text = "Заказы:   ";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveFirstItem.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMovePreviousItem.Text = "Переместить назад";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveNextItem.Text = "Переместить вперед";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveLastItem.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 28);
            // 
            // table_ProjectsBindingNavigatorSaveItem
            // 
            this.table_ProjectsBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.table_ProjectsBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("table_ProjectsBindingNavigatorSaveItem.Image")));
            this.table_ProjectsBindingNavigatorSaveItem.Name = "table_ProjectsBindingNavigatorSaveItem";
            this.table_ProjectsBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 25);
            this.table_ProjectsBindingNavigatorSaveItem.Text = "Сохранить данные";
            this.table_ProjectsBindingNavigatorSaveItem.Visible = false;
            // 
            // toolStripButtonAdd
            // 
            this.toolStripButtonAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAdd.Image")));
            this.toolStripButtonAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAdd.Name = "toolStripButtonAdd";
            this.toolStripButtonAdd.Size = new System.Drawing.Size(63, 25);
            this.toolStripButtonAdd.Text = "Добавить";
            this.toolStripButtonAdd.Click += new System.EventHandler(this.toolStripButtonAdd_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonOpen
            // 
            this.toolStripButtonOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonOpen.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonOpen.Image")));
            this.toolStripButtonOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonOpen.Name = "toolStripButtonOpen";
            this.toolStripButtonOpen.Size = new System.Drawing.Size(89, 25);
            this.toolStripButtonOpen.Text = "Открыть заказ";
            this.toolStripButtonOpen.Click += new System.EventHandler(this.toolStripButtonOpen_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonDelete
            // 
            this.toolStripButtonDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonDelete.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDelete.Image")));
            this.toolStripButtonDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDelete.Name = "toolStripButtonDelete";
            this.toolStripButtonDelete.Size = new System.Drawing.Size(55, 25);
            this.toolStripButtonDelete.Text = "Удалить";
            this.toolStripButtonDelete.Click += new System.EventHandler(this.toolStripButtonDelete_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButton_ToExcel
            // 
            this.toolStripButton_ToExcel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton_ToExcel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_ToExcel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_ToExcel.Image")));
            this.toolStripButton_ToExcel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_ToExcel.Name = "toolStripButton_ToExcel";
            this.toolStripButton_ToExcel.Size = new System.Drawing.Size(46, 25);
            this.toolStripButton_ToExcel.Text = "в Excel";
            this.toolStripButton_ToExcel.Click += new System.EventHandler(this.toolStripButton_ToExcel_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 28);
            // 
            // table_ProductionStagesBindingSource
            // 
            this.table_ProductionStagesBindingSource.DataMember = "FK_Table_ProductionStages_Table_Projects";
            this.table_ProductionStagesBindingSource.DataSource = this.table_ProjectsBindingSource;
            // 
            // table_ProductionStagesTableAdapter
            // 
            this.table_ProductionStagesTableAdapter.ClearBeforeFill = true;
            // 
            // table_MoneyFlowBindingSource
            // 
            this.table_MoneyFlowBindingSource.DataMember = "FK_Table_MoneyMoves_Table_Projects";
            this.table_MoneyFlowBindingSource.DataSource = this.table_ProjectsBindingSource;
            // 
            // table_MoneyFlowTableAdapter
            // 
            this.table_MoneyFlowTableAdapter.ClearBeforeFill = true;
            // 
            // table_SubProjectsBindingSource
            // 
            this.table_SubProjectsBindingSource.DataMember = "FK_Table_SubProjects_Table_Projects";
            this.table_SubProjectsBindingSource.DataSource = this.table_ProjectsBindingSource;
            // 
            // table_SubProjectsTableAdapter
            // 
            this.table_SubProjectsTableAdapter.ClearBeforeFill = true;
            // 
            // table_TEOMaterialsBindingSource
            // 
            this.table_TEOMaterialsBindingSource.DataMember = "FK_Table_TEOMaterials_Table_SubProjects";
            this.table_TEOMaterialsBindingSource.DataSource = this.table_SubProjectsBindingSource;
            // 
            // table_TEOMaterialsTableAdapter
            // 
            this.table_TEOMaterialsTableAdapter.ClearBeforeFill = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox_ShowByTypes);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.table_ProjectsBindingNavigator);
            this.splitContainer1.Panel2.Controls.Add(this.table_ProjectsDataGridView);
            this.splitContainer1.Size = new System.Drawing.Size(870, 619);
            this.splitContainer1.SplitterDistance = 145;
            this.splitContainer1.TabIndex = 2;
            // 
            // groupBox_ShowByTypes
            // 
            this.groupBox_ShowByTypes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_ShowByTypes.Controls.Add(this.checkBox_1);
            this.groupBox_ShowByTypes.Controls.Add(this.checkBox_3);
            this.groupBox_ShowByTypes.Controls.Add(this.checkBox_2);
            this.groupBox_ShowByTypes.Controls.Add(this.checkBox_5);
            this.groupBox_ShowByTypes.Controls.Add(this.checkBox_4);
            this.groupBox_ShowByTypes.Location = new System.Drawing.Point(3, 3);
            this.groupBox_ShowByTypes.Name = "groupBox_ShowByTypes";
            this.groupBox_ShowByTypes.Size = new System.Drawing.Size(135, 146);
            this.groupBox_ShowByTypes.TabIndex = 2;
            this.groupBox_ShowByTypes.TabStop = false;
            this.groupBox_ShowByTypes.Text = "Показывать заказы:";
            // 
            // checkBox_1
            // 
            this.checkBox_1.AutoSize = true;
            this.checkBox_1.Location = new System.Drawing.Point(17, 18);
            this.checkBox_1.Name = "checkBox_1";
            this.checkBox_1.Size = new System.Drawing.Size(98, 17);
            this.checkBox_1.TabIndex = 0;
            this.checkBox_1.Text = "Согласование";
            this.checkBox_1.UseVisualStyleBackColor = true;
            this.checkBox_1.CheckedChanged += new System.EventHandler(this.checkBox_1_CheckedChanged);
            // 
            // checkBox_3
            // 
            this.checkBox_3.AutoSize = true;
            this.checkBox_3.Checked = true;
            this.checkBox_3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_3.Location = new System.Drawing.Point(17, 64);
            this.checkBox_3.Name = "checkBox_3";
            this.checkBox_3.Size = new System.Drawing.Size(66, 17);
            this.checkBox_3.TabIndex = 0;
            this.checkBox_3.Text = "Монтаж";
            this.checkBox_3.UseVisualStyleBackColor = true;
            this.checkBox_3.CheckedChanged += new System.EventHandler(this.checkBox_3_CheckedChanged);
            // 
            // checkBox_2
            // 
            this.checkBox_2.AutoSize = true;
            this.checkBox_2.Checked = true;
            this.checkBox_2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_2.Location = new System.Drawing.Point(17, 41);
            this.checkBox_2.Name = "checkBox_2";
            this.checkBox_2.Size = new System.Drawing.Size(99, 17);
            this.checkBox_2.TabIndex = 0;
            this.checkBox_2.Text = "Производство";
            this.checkBox_2.UseVisualStyleBackColor = true;
            this.checkBox_2.CheckedChanged += new System.EventHandler(this.checkBox_2_CheckedChanged);
            // 
            // checkBox_5
            // 
            this.checkBox_5.AutoSize = true;
            this.checkBox_5.Location = new System.Drawing.Point(17, 110);
            this.checkBox_5.Name = "checkBox_5";
            this.checkBox_5.Size = new System.Drawing.Size(71, 17);
            this.checkBox_5.TabIndex = 0;
            this.checkBox_5.Text = "Отменен";
            this.checkBox_5.UseVisualStyleBackColor = true;
            this.checkBox_5.CheckedChanged += new System.EventHandler(this.checkBox_5_CheckedChanged);
            // 
            // checkBox_4
            // 
            this.checkBox_4.AutoSize = true;
            this.checkBox_4.Location = new System.Drawing.Point(17, 87);
            this.checkBox_4.Name = "checkBox_4";
            this.checkBox_4.Size = new System.Drawing.Size(77, 17);
            this.checkBox_4.TabIndex = 0;
            this.checkBox_4.Text = "Выполнен";
            this.checkBox_4.UseVisualStyleBackColor = true;
            this.checkBox_4.CheckedChanged += new System.EventHandler(this.checkBox_4_CheckedChanged);
            // 
            // table_UsersTableAdapter
            // 
            this.table_UsersTableAdapter.ClearBeforeFill = true;
            // 
            // ProjectID
            // 
            this.ProjectID.DataPropertyName = "id";
            this.ProjectID.HeaderText = "id";
            this.ProjectID.Name = "ProjectID";
            this.ProjectID.ReadOnly = true;
            this.ProjectID.Visible = false;
            this.ProjectID.Width = 40;
            // 
            // ProjectNum
            // 
            this.ProjectNum.DataPropertyName = "Num";
            this.ProjectNum.HeaderText = "№";
            this.ProjectNum.Name = "ProjectNum";
            this.ProjectNum.ReadOnly = true;
            this.ProjectNum.Width = 43;
            // 
            // ProjectDate
            // 
            this.ProjectDate.DataPropertyName = "Date";
            this.ProjectDate.HeaderText = "Дата создания";
            this.ProjectDate.Name = "ProjectDate";
            this.ProjectDate.ReadOnly = true;
            // 
            // ProjectName
            // 
            this.ProjectName.DataPropertyName = "Name";
            this.ProjectName.HeaderText = "Имя";
            this.ProjectName.Name = "ProjectName";
            this.ProjectName.ReadOnly = true;
            this.ProjectName.Width = 54;
            // 
            // ProjectCompanyID
            // 
            this.ProjectCompanyID.DataPropertyName = "CompanyID";
            this.ProjectCompanyID.DataSource = this.tableCompaniesBindingSource;
            this.ProjectCompanyID.DisplayMember = "Name";
            this.ProjectCompanyID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.ProjectCompanyID.HeaderText = "Заказчик";
            this.ProjectCompanyID.Name = "ProjectCompanyID";
            this.ProjectCompanyID.ReadOnly = true;
            this.ProjectCompanyID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ProjectCompanyID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ProjectCompanyID.ValueMember = "id";
            this.ProjectCompanyID.Width = 80;
            // 
            // ProjectMargin
            // 
            this.ProjectMargin.DataPropertyName = "Margin";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N1";
            dataGridViewCellStyle1.NullValue = null;
            this.ProjectMargin.DefaultCellStyle = dataGridViewCellStyle1;
            this.ProjectMargin.HeaderText = "Маржа";
            this.ProjectMargin.Name = "ProjectMargin";
            this.ProjectMargin.ReadOnly = true;
            this.ProjectMargin.Visible = false;
            this.ProjectMargin.Width = 67;
            // 
            // ProjectSum
            // 
            this.ProjectSum.DataPropertyName = "Sum";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "C2";
            dataGridViewCellStyle2.NullValue = null;
            this.ProjectSum.DefaultCellStyle = dataGridViewCellStyle2;
            this.ProjectSum.HeaderText = "Сумма";
            this.ProjectSum.Name = "ProjectSum";
            this.ProjectSum.ReadOnly = true;
            this.ProjectSum.Visible = false;
            this.ProjectSum.Width = 66;
            // 
            // ProjectStatusID
            // 
            this.ProjectStatusID.DataPropertyName = "ProjectStatusID";
            this.ProjectStatusID.DataSource = this.tableProjectStatusesBindingSource;
            this.ProjectStatusID.DisplayMember = "Name";
            this.ProjectStatusID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.ProjectStatusID.HeaderText = "Статус";
            this.ProjectStatusID.Name = "ProjectStatusID";
            this.ProjectStatusID.ReadOnly = true;
            this.ProjectStatusID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ProjectStatusID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ProjectStatusID.ValueMember = "id";
            this.ProjectStatusID.Width = 66;
            // 
            // ManagerID
            // 
            this.ManagerID.DataPropertyName = "ManagerID";
            this.ManagerID.DataSource = this.tableUsersBindingSource;
            this.ManagerID.DisplayMember = "EmployeeID";
            this.ManagerID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.ManagerID.HeaderText = "Менеджер";
            this.ManagerID.Name = "ManagerID";
            this.ManagerID.ReadOnly = true;
            this.ManagerID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ManagerID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ManagerID.ValueMember = "id";
            // 
            // ProjectComment
            // 
            this.ProjectComment.DataPropertyName = "Comment";
            this.ProjectComment.HeaderText = "Комментарий";
            this.ProjectComment.Name = "ProjectComment";
            this.ProjectComment.ReadOnly = true;
            this.ProjectComment.Width = 102;
            // 
            // State
            // 
            this.State.DataPropertyName = "State";
            this.State.HeaderText = "State";
            this.State.Name = "State";
            this.State.ReadOnly = true;
            this.State.Visible = false;
            // 
            // Form_Projects
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(870, 641);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form_Projects";
            this.Text = "Заказы";
            this.Load += new System.EventHandler(this.Form_Projects_Load);
            this.Controls.SetChildIndex(this.splitContainer1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.table_ProjectsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cA_DB_DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableCompaniesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableProjectStatusesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_ProjectsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableUsersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_ProjectsBindingNavigator)).EndInit();
            this.table_ProjectsBindingNavigator.ResumeLayout(false);
            this.table_ProjectsBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_ProductionStagesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_MoneyFlowBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_SubProjectsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_TEOMaterialsBindingSource)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox_ShowByTypes.ResumeLayout(false);
            this.groupBox_ShowByTypes.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CA_DB_DataSet cA_DB_DataSet;
        private System.Windows.Forms.BindingSource table_ProjectsBindingSource;
        private CA_DB_DataSetTableAdapters.Table_ProjectsTableAdapter table_ProjectsTableAdapter;
        private CA_DB_DataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator table_ProjectsBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton table_ProjectsBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView table_ProjectsDataGridView;
        private CA_DB_DataSetTableAdapters.Table_ProjectStatusesTableAdapter table_ProjectStatusesTableAdapter;
        private System.Windows.Forms.BindingSource tableProjectStatusesBindingSource;
        private CA_DB_DataSetTableAdapters.Table_CompaniesTableAdapter table_CompaniesTableAdapter;
        private System.Windows.Forms.BindingSource tableCompaniesBindingSource;
        private System.Windows.Forms.ToolStripButton toolStripButtonOpen;
        private System.Windows.Forms.ToolStripButton toolStripButtonAdd;
        private System.Windows.Forms.ToolStripButton toolStripButtonDelete;
        private System.Windows.Forms.BindingSource table_ProductionStagesBindingSource;
        private CA_DB_DataSetTableAdapters.Table_ProductionStagesTableAdapter table_ProductionStagesTableAdapter;
        private System.Windows.Forms.BindingSource table_MoneyFlowBindingSource;
        private CA_DB_DataSetTableAdapters.Table_MoneyFlowTableAdapter table_MoneyFlowTableAdapter;
        private System.Windows.Forms.BindingSource table_SubProjectsBindingSource;
        private CA_DB_DataSetTableAdapters.Table_SubProjectsTableAdapter table_SubProjectsTableAdapter;
        private System.Windows.Forms.BindingSource table_TEOMaterialsBindingSource;
        private CA_DB_DataSetTableAdapters.Table_TEOMaterialsTableAdapter table_TEOMaterialsTableAdapter;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.CheckBox checkBox_2;
        private System.Windows.Forms.CheckBox checkBox_3;
        private System.Windows.Forms.CheckBox checkBox_5;
        private System.Windows.Forms.CheckBox checkBox_4;
        private System.Windows.Forms.CheckBox checkBox_1;
        private System.Windows.Forms.GroupBox groupBox_ShowByTypes;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton toolStripButton_ToExcel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.BindingSource tableUsersBindingSource;
        private CA_DB_DataSetTableAdapters.Table_UsersTableAdapter table_UsersTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectName;
        private System.Windows.Forms.DataGridViewComboBoxColumn ProjectCompanyID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectMargin;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectSum;
        private System.Windows.Forms.DataGridViewComboBoxColumn ProjectStatusID;
        private System.Windows.Forms.DataGridViewComboBoxColumn ManagerID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectComment;
        private System.Windows.Forms.DataGridViewTextBoxColumn State;
    }
}