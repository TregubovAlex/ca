﻿namespace CA
{
    partial class Form_ProjectCurrent : Form_BaseStyle 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label idLabel;
            System.Windows.Forms.Label numLabel;
            System.Windows.Forms.Label nameLabel;
            System.Windows.Forms.Label dateLabel;
            System.Windows.Forms.Label companyIDLabel;
            System.Windows.Forms.Label marginLabel;
            System.Windows.Forms.Label sumLabel;
            System.Windows.Forms.Label projectStatusIDLabel;
            System.Windows.Forms.Label commentLabel;
            System.Windows.Forms.Label stateLabel;
            System.Windows.Forms.Label label1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_ProjectCurrent));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.managerIDComboBox = new System.Windows.Forms.ComboBox();
            this.table_ProjectsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cA_DB_DataSet = new CA.CA_DB_DataSet();
            this.tableUsersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.stateTextBox = new System.Windows.Forms.TextBox();
            this.marginNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.sumNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.button_Refresh = new System.Windows.Forms.Button();
            this.button_Save = new System.Windows.Forms.Button();
            this.button_TEO_to_Excel = new System.Windows.Forms.Button();
            this.companyComboBox = new System.Windows.Forms.ComboBox();
            this.tableCompaniesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.statusComboBox = new System.Windows.Forms.ComboBox();
            this.tableProjectStatusesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.commentTextBox = new System.Windows.Forms.TextBox();
            this.dateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.idTextBox = new System.Windows.Forms.TextBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.numTextBox = new System.Windows.Forms.TextBox();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage_TEO = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.table_SubProjectsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAddSubProject = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonMove = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonDeleteSubProject = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_ToExcel1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.table_SubProjectsDataGridView = new System.Windows.Forms.DataGridView();
            this.SubProjectID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SubProjectProjectID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tableProjectsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.SubProjectName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SubProjectState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingNavigator2 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem1 = new System.Windows.Forms.ToolStripButton();
            this.table_TEOMaterialsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorCountItem1 = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveFirstItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem1 = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAddTEOMaterial = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonDeleteTEOMaterial = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_ToExcel2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.table_TEOMaterialsDataGridView = new System.Windows.Forms.DataGridView();
            this.TEOMaterialID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TEOMaterialSubProjectID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tableSubProjectsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.TEOMaterialMaterialID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tableMaterialsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.TEOMaterialSelectButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.TEOMaterialCompanyID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tableCompaniesBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.TEOMaterialPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TEOMaterialCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TEOMaterialComment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TEOMaterialState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage_MoneyFlow = new System.Windows.Forms.TabPage();
            this.bindingNavigator3 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem2 = new System.Windows.Forms.ToolStripButton();
            this.table_MoneyFlowBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorCountItem2 = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveFirstItem2 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem2 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem2 = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem2 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem2 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAddMoneyFlow = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonDeleteMoneyFlow = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_ToExcel3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator19 = new System.Windows.Forms.ToolStripSeparator();
            this.table_MoneyFlowDataGridView = new System.Windows.Forms.DataGridView();
            this.MoneyFlowID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MoneyFlowProjectID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tableProjectsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.MoneyFlowDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MoneyFlowSum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MoneyFlowIsCash = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.MoneyFlowEmployeeID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tableEmployeesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.MoneyFlowMaterialID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tableMaterialsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.MoneyFlowComment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MoneyFlowState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage_ProductionStages = new System.Windows.Forms.TabPage();
            this.bindingNavigator4 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem3 = new System.Windows.Forms.ToolStripButton();
            this.table_ProductionStagesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorCountItem3 = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveFirstItem3 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem3 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem3 = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem3 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem3 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAddProductionStage = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonDeleteProductionStage = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator20 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_ToExcel4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator21 = new System.Windows.Forms.ToolStripSeparator();
            this.table_ProductionStagesDataGridView = new System.Windows.Forms.DataGridView();
            this.ProductionStageID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductionStageProjectID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductionStageName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductionStageStartDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductionStageDuration = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductionStageState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage_Files = new System.Windows.Forms.TabPage();
            this.table_ProjectFilesDataGridView = new System.Windows.Forms.DataGridView();
            this.tableProjectsBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.table_ProjectFilesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigator5 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem4 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem4 = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel5 = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem4 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem4 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem4 = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem4 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem4 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAddFile = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator22 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonOpenFile = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator26 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonDeleteFile = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator23 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator24 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_ToExcel5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator25 = new System.Windows.Forms.ToolStripSeparator();
            this.table_ProjectsTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_ProjectsTableAdapter();
            this.tableAdapterManager = new CA.CA_DB_DataSetTableAdapters.TableAdapterManager();
            this.table_CompaniesTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_CompaniesTableAdapter();
            this.table_ProjectStatusesTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_ProjectStatusesTableAdapter();
            this.table_SubProjectsTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_SubProjectsTableAdapter();
            this.table_TEOMaterialsTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_TEOMaterialsTableAdapter();
            this.table_MaterialsTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_MaterialsTableAdapter();
            this.table_MoneyFlowTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_MoneyFlowTableAdapter();
            this.table_EmployeesTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_EmployeesTableAdapter();
            this.table_ProductionStagesTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_ProductionStagesTableAdapter();
            this.table_UsersTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_UsersTableAdapter();
            this.table_ProjectFilesTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_ProjectFilesTableAdapter();
            this.FileID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileProjectID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.FileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileExtension = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileData = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileComment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            idLabel = new System.Windows.Forms.Label();
            numLabel = new System.Windows.Forms.Label();
            nameLabel = new System.Windows.Forms.Label();
            dateLabel = new System.Windows.Forms.Label();
            companyIDLabel = new System.Windows.Forms.Label();
            marginLabel = new System.Windows.Forms.Label();
            sumLabel = new System.Windows.Forms.Label();
            projectStatusIDLabel = new System.Windows.Forms.Label();
            commentLabel = new System.Windows.Forms.Label();
            stateLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_ProjectsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cA_DB_DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableUsersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.marginNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sumNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableCompaniesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableProjectStatusesBindingSource)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabPage_TEO.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_SubProjectsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_SubProjectsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableProjectsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator2)).BeginInit();
            this.bindingNavigator2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_TEOMaterialsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_TEOMaterialsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableSubProjectsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableMaterialsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableCompaniesBindingSource1)).BeginInit();
            this.tabPage_MoneyFlow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator3)).BeginInit();
            this.bindingNavigator3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_MoneyFlowBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_MoneyFlowDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableProjectsBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableEmployeesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableMaterialsBindingSource1)).BeginInit();
            this.tabPage_ProductionStages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator4)).BeginInit();
            this.bindingNavigator4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_ProductionStagesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_ProductionStagesDataGridView)).BeginInit();
            this.tabPage_Files.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_ProjectFilesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableProjectsBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_ProjectFilesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator5)).BeginInit();
            this.bindingNavigator5.SuspendLayout();
            this.SuspendLayout();
            // 
            // idLabel
            // 
            idLabel.AutoSize = true;
            idLabel.Location = new System.Drawing.Point(338, 36);
            idLabel.Name = "idLabel";
            idLabel.Size = new System.Drawing.Size(18, 13);
            idLabel.TabIndex = 0;
            idLabel.Text = "id:";
            idLabel.Visible = false;
            // 
            // numLabel
            // 
            numLabel.AutoSize = true;
            numLabel.Location = new System.Drawing.Point(10, 35);
            numLabel.Name = "numLabel";
            numLabel.Size = new System.Drawing.Size(44, 13);
            numLabel.TabIndex = 2;
            numLabel.Text = "Номер:";
            // 
            // nameLabel
            // 
            nameLabel.AutoSize = true;
            nameLabel.Location = new System.Drawing.Point(10, 10);
            nameLabel.Name = "nameLabel";
            nameLabel.Size = new System.Drawing.Size(60, 13);
            nameLabel.TabIndex = 4;
            nameLabel.Text = "Название:";
            // 
            // dateLabel
            // 
            dateLabel.AutoSize = true;
            dateLabel.Location = new System.Drawing.Point(165, 36);
            dateLabel.Name = "dateLabel";
            dateLabel.Size = new System.Drawing.Size(21, 13);
            dateLabel.TabIndex = 6;
            dateLabel.Text = "от:";
            // 
            // companyIDLabel
            // 
            companyIDLabel.AutoSize = true;
            companyIDLabel.Location = new System.Drawing.Point(10, 59);
            companyIDLabel.Name = "companyIDLabel";
            companyIDLabel.Size = new System.Drawing.Size(58, 13);
            companyIDLabel.TabIndex = 8;
            companyIDLabel.Text = "Заказчик:";
            // 
            // marginLabel
            // 
            marginLabel.AutoSize = true;
            marginLabel.Location = new System.Drawing.Point(220, 87);
            marginLabel.Name = "marginLabel";
            marginLabel.Size = new System.Drawing.Size(59, 13);
            marginLabel.TabIndex = 10;
            marginLabel.Text = "Маржа, %:";
            // 
            // sumLabel
            // 
            sumLabel.AutoSize = true;
            sumLabel.Location = new System.Drawing.Point(10, 87);
            sumLabel.Name = "sumLabel";
            sumLabel.Size = new System.Drawing.Size(70, 13);
            sumLabel.TabIndex = 12;
            sumLabel.Text = "Сумма, руб.:";
            // 
            // projectStatusIDLabel
            // 
            projectStatusIDLabel.AutoSize = true;
            projectStatusIDLabel.Location = new System.Drawing.Point(10, 112);
            projectStatusIDLabel.Name = "projectStatusIDLabel";
            projectStatusIDLabel.Size = new System.Drawing.Size(44, 13);
            projectStatusIDLabel.TabIndex = 14;
            projectStatusIDLabel.Text = "Статус:";
            // 
            // commentLabel
            // 
            commentLabel.AutoSize = true;
            commentLabel.Location = new System.Drawing.Point(10, 167);
            commentLabel.Name = "commentLabel";
            commentLabel.Size = new System.Drawing.Size(80, 13);
            commentLabel.TabIndex = 16;
            commentLabel.Text = "Комментарий:";
            // 
            // stateLabel
            // 
            stateLabel.AutoSize = true;
            stateLabel.Location = new System.Drawing.Point(399, 36);
            stateLabel.Name = "stateLabel";
            stateLabel.Size = new System.Drawing.Size(35, 13);
            stateLabel.TabIndex = 21;
            stateLabel.Text = "State:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(10, 139);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(63, 13);
            label1.TabIndex = 14;
            label1.Text = "Менеджер:";
            // 
            // splitContainer3
            // 
            this.splitContainer3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.AutoScroll = true;
            this.splitContainer3.Panel1.Controls.Add(this.managerIDComboBox);
            this.splitContainer3.Panel1.Controls.Add(stateLabel);
            this.splitContainer3.Panel1.Controls.Add(this.stateTextBox);
            this.splitContainer3.Panel1.Controls.Add(this.marginNumericUpDown);
            this.splitContainer3.Panel1.Controls.Add(this.sumNumericUpDown);
            this.splitContainer3.Panel1.Controls.Add(this.button_Refresh);
            this.splitContainer3.Panel1.Controls.Add(this.button_Save);
            this.splitContainer3.Panel1.Controls.Add(this.button_TEO_to_Excel);
            this.splitContainer3.Panel1.Controls.Add(this.companyComboBox);
            this.splitContainer3.Panel1.Controls.Add(this.statusComboBox);
            this.splitContainer3.Panel1.Controls.Add(idLabel);
            this.splitContainer3.Panel1.Controls.Add(commentLabel);
            this.splitContainer3.Panel1.Controls.Add(companyIDLabel);
            this.splitContainer3.Panel1.Controls.Add(this.commentTextBox);
            this.splitContainer3.Panel1.Controls.Add(this.dateDateTimePicker);
            this.splitContainer3.Panel1.Controls.Add(dateLabel);
            this.splitContainer3.Panel1.Controls.Add(this.idTextBox);
            this.splitContainer3.Panel1.Controls.Add(label1);
            this.splitContainer3.Panel1.Controls.Add(marginLabel);
            this.splitContainer3.Panel1.Controls.Add(projectStatusIDLabel);
            this.splitContainer3.Panel1.Controls.Add(this.nameTextBox);
            this.splitContainer3.Panel1.Controls.Add(numLabel);
            this.splitContainer3.Panel1.Controls.Add(nameLabel);
            this.splitContainer3.Panel1.Controls.Add(this.numTextBox);
            this.splitContainer3.Panel1.Controls.Add(sumLabel);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.AutoScroll = true;
            this.splitContainer3.Panel2.Controls.Add(this.tabControl);
            this.splitContainer3.Size = new System.Drawing.Size(968, 680);
            this.splitContainer3.SplitterDistance = 253;
            this.splitContainer3.TabIndex = 18;
            // 
            // managerIDComboBox
            // 
            this.managerIDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.table_ProjectsBindingSource, "ManagerID", true));
            this.managerIDComboBox.DataSource = this.tableUsersBindingSource;
            this.managerIDComboBox.DisplayMember = "EmployeeID";
            this.managerIDComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.managerIDComboBox.FormattingEnabled = true;
            this.managerIDComboBox.Location = new System.Drawing.Point(96, 136);
            this.managerIDComboBox.Name = "managerIDComboBox";
            this.managerIDComboBox.Size = new System.Drawing.Size(215, 21);
            this.managerIDComboBox.TabIndex = 26;
            this.managerIDComboBox.ValueMember = "id";
            // 
            // table_ProjectsBindingSource
            // 
            this.table_ProjectsBindingSource.DataMember = "Table_Projects";
            this.table_ProjectsBindingSource.DataSource = this.cA_DB_DataSet;
            // 
            // cA_DB_DataSet
            // 
            this.cA_DB_DataSet.DataSetName = "CA_DB_DataSet";
            this.cA_DB_DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableUsersBindingSource
            // 
            this.tableUsersBindingSource.DataMember = "Table_Users";
            this.tableUsersBindingSource.DataSource = this.cA_DB_DataSet;
            // 
            // stateTextBox
            // 
            this.stateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.table_ProjectsBindingSource, "State", true));
            this.stateTextBox.Location = new System.Drawing.Point(440, 33);
            this.stateTextBox.Name = "stateTextBox";
            this.stateTextBox.Size = new System.Drawing.Size(60, 19);
            this.stateTextBox.TabIndex = 22;
            // 
            // marginNumericUpDown
            // 
            this.marginNumericUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.table_ProjectsBindingSource, "Margin", true));
            this.marginNumericUpDown.DecimalPlaces = 2;
            this.marginNumericUpDown.InterceptArrowKeys = false;
            this.marginNumericUpDown.Location = new System.Drawing.Point(282, 85);
            this.marginNumericUpDown.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.marginNumericUpDown.Name = "marginNumericUpDown";
            this.marginNumericUpDown.Size = new System.Drawing.Size(73, 19);
            this.marginNumericUpDown.TabIndex = 20;
            this.marginNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.marginNumericUpDown.ThousandsSeparator = true;
            this.marginNumericUpDown.DoubleClick += new System.EventHandler(this.marginNumericUpDown_DoubleClick);
            // 
            // sumNumericUpDown
            // 
            this.sumNumericUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.table_ProjectsBindingSource, "Sum", true));
            this.sumNumericUpDown.DecimalPlaces = 2;
            this.sumNumericUpDown.InterceptArrowKeys = false;
            this.sumNumericUpDown.Location = new System.Drawing.Point(96, 85);
            this.sumNumericUpDown.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.sumNumericUpDown.Name = "sumNumericUpDown";
            this.sumNumericUpDown.Size = new System.Drawing.Size(101, 19);
            this.sumNumericUpDown.TabIndex = 20;
            this.sumNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.sumNumericUpDown.ThousandsSeparator = true;
            this.sumNumericUpDown.DoubleClick += new System.EventHandler(this.sumNumericUpDown_DoubleClick);
            // 
            // button_Refresh
            // 
            this.button_Refresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Refresh.Location = new System.Drawing.Point(822, 56);
            this.button_Refresh.Name = "button_Refresh";
            this.button_Refresh.Size = new System.Drawing.Size(131, 40);
            this.button_Refresh.TabIndex = 11;
            this.button_Refresh.Text = "Обновить";
            this.button_Refresh.UseVisualStyleBackColor = true;
            this.button_Refresh.Visible = false;
            this.button_Refresh.Click += new System.EventHandler(this.button_Refresh_Click);
            // 
            // button_Save
            // 
            this.button_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Save.Location = new System.Drawing.Point(822, 10);
            this.button_Save.Name = "button_Save";
            this.button_Save.Size = new System.Drawing.Size(131, 40);
            this.button_Save.TabIndex = 19;
            this.button_Save.Text = "Сохранить";
            this.button_Save.UseVisualStyleBackColor = true;
            this.button_Save.Visible = false;
            this.button_Save.Click += new System.EventHandler(this.button_Save_Click);
            // 
            // button_TEO_to_Excel
            // 
            this.button_TEO_to_Excel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_TEO_to_Excel.Location = new System.Drawing.Point(822, 206);
            this.button_TEO_to_Excel.Name = "button_TEO_to_Excel";
            this.button_TEO_to_Excel.Size = new System.Drawing.Size(131, 40);
            this.button_TEO_to_Excel.TabIndex = 2;
            this.button_TEO_to_Excel.Text = "ТЭО --> Excel";
            this.button_TEO_to_Excel.UseVisualStyleBackColor = true;
            this.button_TEO_to_Excel.Click += new System.EventHandler(this.button_TEO_to_Excel_Click);
            // 
            // companyComboBox
            // 
            this.companyComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.table_ProjectsBindingSource, "CompanyID", true));
            this.companyComboBox.DataSource = this.tableCompaniesBindingSource;
            this.companyComboBox.DisplayMember = "Name";
            this.companyComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.companyComboBox.FormattingEnabled = true;
            this.companyComboBox.Location = new System.Drawing.Point(96, 57);
            this.companyComboBox.Name = "companyComboBox";
            this.companyComboBox.Size = new System.Drawing.Size(215, 21);
            this.companyComboBox.TabIndex = 18;
            this.companyComboBox.ValueMember = "id";
            this.companyComboBox.Click += new System.EventHandler(this.companyComboBox_Click);
            // 
            // tableCompaniesBindingSource
            // 
            this.tableCompaniesBindingSource.DataMember = "Table_Companies";
            this.tableCompaniesBindingSource.DataSource = this.cA_DB_DataSet;
            this.tableCompaniesBindingSource.Sort = "Name";
            // 
            // statusComboBox
            // 
            this.statusComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.table_ProjectsBindingSource, "ProjectStatusID", true));
            this.statusComboBox.DataSource = this.tableProjectStatusesBindingSource;
            this.statusComboBox.DisplayMember = "Name";
            this.statusComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.statusComboBox.FormattingEnabled = true;
            this.statusComboBox.Location = new System.Drawing.Point(96, 109);
            this.statusComboBox.Name = "statusComboBox";
            this.statusComboBox.Size = new System.Drawing.Size(215, 21);
            this.statusComboBox.TabIndex = 18;
            this.statusComboBox.ValueMember = "id";
            // 
            // tableProjectStatusesBindingSource
            // 
            this.tableProjectStatusesBindingSource.DataMember = "Table_ProjectStatuses";
            this.tableProjectStatusesBindingSource.DataSource = this.cA_DB_DataSet;
            this.tableProjectStatusesBindingSource.Sort = "Num";
            // 
            // commentTextBox
            // 
            this.commentTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.table_ProjectsBindingSource, "Comment", true));
            this.commentTextBox.Location = new System.Drawing.Point(96, 164);
            this.commentTextBox.Multiline = true;
            this.commentTextBox.Name = "commentTextBox";
            this.commentTextBox.Size = new System.Drawing.Size(327, 59);
            this.commentTextBox.TabIndex = 17;
            // 
            // dateDateTimePicker
            // 
            this.dateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.table_ProjectsBindingSource, "Date", true));
            this.dateDateTimePicker.Location = new System.Drawing.Point(192, 32);
            this.dateDateTimePicker.Name = "dateDateTimePicker";
            this.dateDateTimePicker.Size = new System.Drawing.Size(119, 19);
            this.dateDateTimePicker.TabIndex = 7;
            // 
            // idTextBox
            // 
            this.idTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.table_ProjectsBindingSource, "id", true));
            this.idTextBox.Location = new System.Drawing.Point(362, 33);
            this.idTextBox.Name = "idTextBox";
            this.idTextBox.Size = new System.Drawing.Size(33, 19);
            this.idTextBox.TabIndex = 1;
            this.idTextBox.Visible = false;
            // 
            // nameTextBox
            // 
            this.nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.table_ProjectsBindingSource, "Name", true));
            this.nameTextBox.Location = new System.Drawing.Point(96, 7);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(327, 19);
            this.nameTextBox.TabIndex = 5;
            // 
            // numTextBox
            // 
            this.numTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.table_ProjectsBindingSource, "Num", true));
            this.numTextBox.Location = new System.Drawing.Point(96, 32);
            this.numTextBox.Name = "numTextBox";
            this.numTextBox.Size = new System.Drawing.Size(60, 19);
            this.numTextBox.TabIndex = 3;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage_TEO);
            this.tabControl.Controls.Add(this.tabPage_MoneyFlow);
            this.tabControl.Controls.Add(this.tabPage_ProductionStages);
            this.tabControl.Controls.Add(this.tabPage_Files);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(964, 419);
            this.tabControl.TabIndex = 0;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // tabPage_TEO
            // 
            this.tabPage_TEO.Controls.Add(this.splitContainer2);
            this.tabPage_TEO.Location = new System.Drawing.Point(4, 22);
            this.tabPage_TEO.Name = "tabPage_TEO";
            this.tabPage_TEO.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_TEO.Size = new System.Drawing.Size(956, 393);
            this.tabPage_TEO.TabIndex = 0;
            this.tabPage_TEO.Text = "Технико-Экономическое Обоснование";
            this.tabPage_TEO.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.AutoScroll = true;
            this.splitContainer2.Panel1.Controls.Add(this.bindingNavigator1);
            this.splitContainer2.Panel1.Controls.Add(this.table_SubProjectsDataGridView);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.AutoScroll = true;
            this.splitContainer2.Panel2.Controls.Add(this.bindingNavigator2);
            this.splitContainer2.Panel2.Controls.Add(this.table_TEOMaterialsDataGridView);
            this.splitContainer2.Size = new System.Drawing.Size(950, 387);
            this.splitContainer2.SplitterDistance = 175;
            this.splitContainer2.TabIndex = 0;
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = this.bindingNavigatorAddNewItem;
            this.bindingNavigator1.BindingSource = this.table_SubProjectsBindingSource;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator1.CountItemFormat = "из {0}";
            this.bindingNavigator1.DeleteItem = this.bindingNavigatorDeleteItem;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.toolStripSeparator1,
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.toolStripButtonAddSubProject,
            this.toolStripSeparator6,
            this.toolStripButtonMove,
            this.toolStripSeparator5,
            this.toolStripButtonDeleteSubProject,
            this.toolStripSeparator8,
            this.toolStripSeparator14,
            this.toolStripButton_ToExcel1,
            this.toolStripSeparator15});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator1.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bindingNavigator1.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bindingNavigator1.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigator1.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator1.Size = new System.Drawing.Size(946, 28);
            this.bindingNavigator1.TabIndex = 1;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorAddNewItem.Text = "Добавить";
            this.bindingNavigatorAddNewItem.Visible = false;
            // 
            // table_SubProjectsBindingSource
            // 
            this.table_SubProjectsBindingSource.DataMember = "FK_Table_SubProjects_Table_Projects";
            this.table_SubProjectsBindingSource.DataSource = this.table_ProjectsBindingSource;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(36, 25);
            this.bindingNavigatorCountItem.Text = "из {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Общее число элементов";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorDeleteItem.Text = "Удалить";
            this.bindingNavigatorDeleteItem.Visible = false;
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(114, 25);
            this.toolStripLabel1.Text = "Изделия:     ";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveFirstItem.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMovePreviousItem.Text = "Переместить назад";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveNextItem.Text = "Переместить вперед";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveLastItem.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonAddSubProject
            // 
            this.toolStripButtonAddSubProject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonAddSubProject.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAddSubProject.Image")));
            this.toolStripButtonAddSubProject.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAddSubProject.Name = "toolStripButtonAddSubProject";
            this.toolStripButtonAddSubProject.Size = new System.Drawing.Size(63, 25);
            this.toolStripButtonAddSubProject.Text = "Добавить";
            this.toolStripButtonAddSubProject.Click += new System.EventHandler(this.toolStripButtonAddSubProject_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonMove
            // 
            this.toolStripButtonMove.BackColor = System.Drawing.Color.Transparent;
            this.toolStripButtonMove.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonMove.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonMove.Image")));
            this.toolStripButtonMove.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonMove.Name = "toolStripButtonMove";
            this.toolStripButtonMove.Size = new System.Drawing.Size(164, 25);
            this.toolStripButtonMove.Text = "Переместить в другой заказ";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonDeleteSubProject
            // 
            this.toolStripButtonDeleteSubProject.BackColor = System.Drawing.Color.Transparent;
            this.toolStripButtonDeleteSubProject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonDeleteSubProject.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDeleteSubProject.Image")));
            this.toolStripButtonDeleteSubProject.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDeleteSubProject.Name = "toolStripButtonDeleteSubProject";
            this.toolStripButtonDeleteSubProject.Size = new System.Drawing.Size(55, 25);
            this.toolStripButtonDeleteSubProject.Text = "Удалить";
            this.toolStripButtonDeleteSubProject.Click += new System.EventHandler(this.toolStripButtonDeleteSubProject_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButton_ToExcel1
            // 
            this.toolStripButton_ToExcel1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton_ToExcel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_ToExcel1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_ToExcel1.Image")));
            this.toolStripButton_ToExcel1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_ToExcel1.Name = "toolStripButton_ToExcel1";
            this.toolStripButton_ToExcel1.Size = new System.Drawing.Size(46, 25);
            this.toolStripButton_ToExcel1.Text = "в Excel";
            this.toolStripButton_ToExcel1.Click += new System.EventHandler(this.toolStripButton_ToExcel1_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(6, 28);
            // 
            // table_SubProjectsDataGridView
            // 
            this.table_SubProjectsDataGridView.AllowUserToAddRows = false;
            this.table_SubProjectsDataGridView.AllowUserToDeleteRows = false;
            this.table_SubProjectsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.table_SubProjectsDataGridView.AutoGenerateColumns = false;
            this.table_SubProjectsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_SubProjectsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SubProjectID,
            this.SubProjectProjectID,
            this.SubProjectName,
            this.SubProjectState});
            this.table_SubProjectsDataGridView.DataSource = this.table_SubProjectsBindingSource;
            this.table_SubProjectsDataGridView.Location = new System.Drawing.Point(0, 28);
            this.table_SubProjectsDataGridView.MultiSelect = false;
            this.table_SubProjectsDataGridView.Name = "table_SubProjectsDataGridView";
            this.table_SubProjectsDataGridView.Size = new System.Drawing.Size(944, 143);
            this.table_SubProjectsDataGridView.TabIndex = 0;
            this.table_SubProjectsDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.table_SubProjectsDataGridView_CellBeginEdit);
            this.table_SubProjectsDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.table_SubProjectsDataGridView_CellValueChanged);
            this.table_SubProjectsDataGridView.CurrentCellChanged += new System.EventHandler(this.table_SubProjectsDataGridView_CurrentCellChanged);
            // 
            // SubProjectID
            // 
            this.SubProjectID.DataPropertyName = "id";
            this.SubProjectID.HeaderText = "id";
            this.SubProjectID.Name = "SubProjectID";
            this.SubProjectID.Visible = false;
            // 
            // SubProjectProjectID
            // 
            this.SubProjectProjectID.DataPropertyName = "ProjectID";
            this.SubProjectProjectID.DataSource = this.tableProjectsBindingSource;
            this.SubProjectProjectID.DisplayMember = "Name";
            this.SubProjectProjectID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.SubProjectProjectID.HeaderText = "Проект";
            this.SubProjectProjectID.Name = "SubProjectProjectID";
            this.SubProjectProjectID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SubProjectProjectID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.SubProjectProjectID.ValueMember = "id";
            // 
            // tableProjectsBindingSource
            // 
            this.tableProjectsBindingSource.DataMember = "Table_Projects";
            this.tableProjectsBindingSource.DataSource = this.cA_DB_DataSet;
            this.tableProjectsBindingSource.Sort = "Name";
            // 
            // SubProjectName
            // 
            this.SubProjectName.DataPropertyName = "Name";
            this.SubProjectName.HeaderText = "Изделие";
            this.SubProjectName.Name = "SubProjectName";
            // 
            // SubProjectState
            // 
            this.SubProjectState.DataPropertyName = "State";
            this.SubProjectState.HeaderText = "State";
            this.SubProjectState.Name = "SubProjectState";
            this.SubProjectState.Visible = false;
            // 
            // bindingNavigator2
            // 
            this.bindingNavigator2.AddNewItem = this.bindingNavigatorAddNewItem1;
            this.bindingNavigator2.BindingSource = this.table_TEOMaterialsBindingSource;
            this.bindingNavigator2.CountItem = this.bindingNavigatorCountItem1;
            this.bindingNavigator2.CountItemFormat = "из {0}";
            this.bindingNavigator2.DeleteItem = this.bindingNavigatorDeleteItem1;
            this.bindingNavigator2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel2,
            this.toolStripSeparator2,
            this.bindingNavigatorMoveFirstItem1,
            this.bindingNavigatorMovePreviousItem1,
            this.bindingNavigatorSeparator3,
            this.bindingNavigatorPositionItem1,
            this.bindingNavigatorCountItem1,
            this.bindingNavigatorSeparator4,
            this.bindingNavigatorMoveNextItem1,
            this.bindingNavigatorMoveLastItem1,
            this.bindingNavigatorSeparator5,
            this.bindingNavigatorAddNewItem1,
            this.bindingNavigatorDeleteItem1,
            this.toolStripButtonAddTEOMaterial,
            this.toolStripSeparator7,
            this.toolStripButtonDeleteTEOMaterial,
            this.toolStripSeparator9,
            this.toolStripSeparator16,
            this.toolStripButton_ToExcel2,
            this.toolStripSeparator17});
            this.bindingNavigator2.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator2.MoveFirstItem = this.bindingNavigatorMoveFirstItem1;
            this.bindingNavigator2.MoveLastItem = this.bindingNavigatorMoveLastItem1;
            this.bindingNavigator2.MoveNextItem = this.bindingNavigatorMoveNextItem1;
            this.bindingNavigator2.MovePreviousItem = this.bindingNavigatorMovePreviousItem1;
            this.bindingNavigator2.Name = "bindingNavigator2";
            this.bindingNavigator2.PositionItem = this.bindingNavigatorPositionItem1;
            this.bindingNavigator2.Size = new System.Drawing.Size(946, 28);
            this.bindingNavigator2.TabIndex = 1;
            this.bindingNavigator2.Text = "bindingNavigator2";
            // 
            // bindingNavigatorAddNewItem1
            // 
            this.bindingNavigatorAddNewItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem1.Image")));
            this.bindingNavigatorAddNewItem1.Name = "bindingNavigatorAddNewItem1";
            this.bindingNavigatorAddNewItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorAddNewItem1.Text = "Добавить";
            this.bindingNavigatorAddNewItem1.Visible = false;
            // 
            // table_TEOMaterialsBindingSource
            // 
            this.table_TEOMaterialsBindingSource.DataMember = "FK_Table_TEOMaterials_Table_SubProjects";
            this.table_TEOMaterialsBindingSource.DataSource = this.table_SubProjectsBindingSource;
            // 
            // bindingNavigatorCountItem1
            // 
            this.bindingNavigatorCountItem1.Name = "bindingNavigatorCountItem1";
            this.bindingNavigatorCountItem1.Size = new System.Drawing.Size(36, 25);
            this.bindingNavigatorCountItem1.Text = "из {0}";
            this.bindingNavigatorCountItem1.ToolTipText = "Общее число элементов";
            // 
            // bindingNavigatorDeleteItem1
            // 
            this.bindingNavigatorDeleteItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem1.Image")));
            this.bindingNavigatorDeleteItem1.Name = "bindingNavigatorDeleteItem1";
            this.bindingNavigatorDeleteItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorDeleteItem1.Text = "Удалить";
            this.bindingNavigatorDeleteItem1.Visible = false;
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(116, 25);
            this.toolStripLabel2.Text = "Материалы:";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveFirstItem1
            // 
            this.bindingNavigatorMoveFirstItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem1.Image")));
            this.bindingNavigatorMoveFirstItem1.Name = "bindingNavigatorMoveFirstItem1";
            this.bindingNavigatorMoveFirstItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveFirstItem1.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem1
            // 
            this.bindingNavigatorMovePreviousItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem1.Image")));
            this.bindingNavigatorMovePreviousItem1.Name = "bindingNavigatorMovePreviousItem1";
            this.bindingNavigatorMovePreviousItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMovePreviousItem1.Text = "Переместить назад";
            // 
            // bindingNavigatorSeparator3
            // 
            this.bindingNavigatorSeparator3.Name = "bindingNavigatorSeparator3";
            this.bindingNavigatorSeparator3.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorPositionItem1
            // 
            this.bindingNavigatorPositionItem1.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem1.AutoSize = false;
            this.bindingNavigatorPositionItem1.Name = "bindingNavigatorPositionItem1";
            this.bindingNavigatorPositionItem1.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem1.Text = "0";
            this.bindingNavigatorPositionItem1.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorSeparator4
            // 
            this.bindingNavigatorSeparator4.Name = "bindingNavigatorSeparator4";
            this.bindingNavigatorSeparator4.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveNextItem1
            // 
            this.bindingNavigatorMoveNextItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem1.Image")));
            this.bindingNavigatorMoveNextItem1.Name = "bindingNavigatorMoveNextItem1";
            this.bindingNavigatorMoveNextItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveNextItem1.Text = "Переместить вперед";
            // 
            // bindingNavigatorMoveLastItem1
            // 
            this.bindingNavigatorMoveLastItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem1.Image")));
            this.bindingNavigatorMoveLastItem1.Name = "bindingNavigatorMoveLastItem1";
            this.bindingNavigatorMoveLastItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveLastItem1.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator5
            // 
            this.bindingNavigatorSeparator5.Name = "bindingNavigatorSeparator5";
            this.bindingNavigatorSeparator5.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonAddTEOMaterial
            // 
            this.toolStripButtonAddTEOMaterial.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonAddTEOMaterial.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAddTEOMaterial.Image")));
            this.toolStripButtonAddTEOMaterial.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAddTEOMaterial.Name = "toolStripButtonAddTEOMaterial";
            this.toolStripButtonAddTEOMaterial.Size = new System.Drawing.Size(63, 25);
            this.toolStripButtonAddTEOMaterial.Text = "Добавить";
            this.toolStripButtonAddTEOMaterial.Click += new System.EventHandler(this.toolStripButtonAddTEOMaterial_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonDeleteTEOMaterial
            // 
            this.toolStripButtonDeleteTEOMaterial.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonDeleteTEOMaterial.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDeleteTEOMaterial.Image")));
            this.toolStripButtonDeleteTEOMaterial.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDeleteTEOMaterial.Name = "toolStripButtonDeleteTEOMaterial";
            this.toolStripButtonDeleteTEOMaterial.Size = new System.Drawing.Size(55, 25);
            this.toolStripButtonDeleteTEOMaterial.Text = "Удалить";
            this.toolStripButtonDeleteTEOMaterial.Click += new System.EventHandler(this.toolStripButtonDeleteTEOMaterial_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButton_ToExcel2
            // 
            this.toolStripButton_ToExcel2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton_ToExcel2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_ToExcel2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_ToExcel2.Image")));
            this.toolStripButton_ToExcel2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_ToExcel2.Name = "toolStripButton_ToExcel2";
            this.toolStripButton_ToExcel2.Size = new System.Drawing.Size(46, 25);
            this.toolStripButton_ToExcel2.Text = "в Excel";
            this.toolStripButton_ToExcel2.Click += new System.EventHandler(this.toolStripButton_ToExcel2_Click);
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(6, 28);
            // 
            // table_TEOMaterialsDataGridView
            // 
            this.table_TEOMaterialsDataGridView.AllowUserToAddRows = false;
            this.table_TEOMaterialsDataGridView.AllowUserToDeleteRows = false;
            this.table_TEOMaterialsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.table_TEOMaterialsDataGridView.AutoGenerateColumns = false;
            this.table_TEOMaterialsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_TEOMaterialsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TEOMaterialID,
            this.TEOMaterialSubProjectID,
            this.TEOMaterialMaterialID,
            this.TEOMaterialSelectButton,
            this.TEOMaterialCompanyID,
            this.TEOMaterialPrice,
            this.TEOMaterialCount,
            this.TEOMaterialComment,
            this.TEOMaterialState});
            this.table_TEOMaterialsDataGridView.DataSource = this.table_TEOMaterialsBindingSource;
            this.table_TEOMaterialsDataGridView.Location = new System.Drawing.Point(0, 28);
            this.table_TEOMaterialsDataGridView.MultiSelect = false;
            this.table_TEOMaterialsDataGridView.Name = "table_TEOMaterialsDataGridView";
            this.table_TEOMaterialsDataGridView.Size = new System.Drawing.Size(945, 178);
            this.table_TEOMaterialsDataGridView.TabIndex = 0;
            this.table_TEOMaterialsDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.table_TEOMaterialsDataGridView_CellBeginEdit);
            this.table_TEOMaterialsDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.table_TEOMaterialsDataGridView_CellClick);
            this.table_TEOMaterialsDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.table_TEOMaterialsDataGridView_CellValueChanged);
            // 
            // TEOMaterialID
            // 
            this.TEOMaterialID.DataPropertyName = "id";
            this.TEOMaterialID.HeaderText = "id";
            this.TEOMaterialID.Name = "TEOMaterialID";
            this.TEOMaterialID.Visible = false;
            // 
            // TEOMaterialSubProjectID
            // 
            this.TEOMaterialSubProjectID.DataPropertyName = "SubProjectID";
            this.TEOMaterialSubProjectID.DataSource = this.tableSubProjectsBindingSource;
            this.TEOMaterialSubProjectID.DisplayMember = "Name";
            this.TEOMaterialSubProjectID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.TEOMaterialSubProjectID.HeaderText = "Раздел (подзаказ)";
            this.TEOMaterialSubProjectID.Name = "TEOMaterialSubProjectID";
            this.TEOMaterialSubProjectID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.TEOMaterialSubProjectID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.TEOMaterialSubProjectID.ValueMember = "id";
            // 
            // tableSubProjectsBindingSource
            // 
            this.tableSubProjectsBindingSource.DataMember = "Table_SubProjects";
            this.tableSubProjectsBindingSource.DataSource = this.cA_DB_DataSet;
            this.tableSubProjectsBindingSource.Sort = "Name";
            // 
            // TEOMaterialMaterialID
            // 
            this.TEOMaterialMaterialID.DataPropertyName = "MaterialID";
            this.TEOMaterialMaterialID.DataSource = this.tableMaterialsBindingSource;
            this.TEOMaterialMaterialID.DisplayMember = "Name";
            this.TEOMaterialMaterialID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.TEOMaterialMaterialID.DropDownWidth = 2;
            this.TEOMaterialMaterialID.HeaderText = "Материал";
            this.TEOMaterialMaterialID.Name = "TEOMaterialMaterialID";
            this.TEOMaterialMaterialID.ReadOnly = true;
            this.TEOMaterialMaterialID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.TEOMaterialMaterialID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.TEOMaterialMaterialID.ValueMember = "id";
            // 
            // tableMaterialsBindingSource
            // 
            this.tableMaterialsBindingSource.DataMember = "Table_Materials";
            this.tableMaterialsBindingSource.DataSource = this.cA_DB_DataSet;
            this.tableMaterialsBindingSource.Sort = "Name";
            // 
            // TEOMaterialSelectButton
            // 
            this.TEOMaterialSelectButton.HeaderText = "...";
            this.TEOMaterialSelectButton.Name = "TEOMaterialSelectButton";
            this.TEOMaterialSelectButton.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.TEOMaterialSelectButton.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.TEOMaterialSelectButton.Text = "...";
            this.TEOMaterialSelectButton.UseColumnTextForButtonValue = true;
            this.TEOMaterialSelectButton.Visible = false;
            // 
            // TEOMaterialCompanyID
            // 
            this.TEOMaterialCompanyID.DataPropertyName = "SupplierID";
            this.TEOMaterialCompanyID.DataSource = this.tableCompaniesBindingSource1;
            this.TEOMaterialCompanyID.DisplayMember = "Name";
            this.TEOMaterialCompanyID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.TEOMaterialCompanyID.HeaderText = "Поставщик";
            this.TEOMaterialCompanyID.Name = "TEOMaterialCompanyID";
            this.TEOMaterialCompanyID.ReadOnly = true;
            this.TEOMaterialCompanyID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.TEOMaterialCompanyID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.TEOMaterialCompanyID.ValueMember = "id";
            // 
            // tableCompaniesBindingSource1
            // 
            this.tableCompaniesBindingSource1.DataMember = "Table_Companies";
            this.tableCompaniesBindingSource1.DataSource = this.cA_DB_DataSet;
            this.tableCompaniesBindingSource1.Sort = "Name";
            // 
            // TEOMaterialPrice
            // 
            this.TEOMaterialPrice.DataPropertyName = "Price";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "C2";
            dataGridViewCellStyle1.NullValue = null;
            this.TEOMaterialPrice.DefaultCellStyle = dataGridViewCellStyle1;
            this.TEOMaterialPrice.HeaderText = "Цена";
            this.TEOMaterialPrice.Name = "TEOMaterialPrice";
            // 
            // TEOMaterialCount
            // 
            this.TEOMaterialCount.DataPropertyName = "Count";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.TEOMaterialCount.DefaultCellStyle = dataGridViewCellStyle2;
            this.TEOMaterialCount.HeaderText = "Кол-во";
            this.TEOMaterialCount.Name = "TEOMaterialCount";
            // 
            // TEOMaterialComment
            // 
            this.TEOMaterialComment.DataPropertyName = "Comment";
            this.TEOMaterialComment.HeaderText = "Комментарий";
            this.TEOMaterialComment.Name = "TEOMaterialComment";
            // 
            // TEOMaterialState
            // 
            this.TEOMaterialState.DataPropertyName = "State";
            this.TEOMaterialState.HeaderText = "State";
            this.TEOMaterialState.Name = "TEOMaterialState";
            this.TEOMaterialState.Visible = false;
            // 
            // tabPage_MoneyFlow
            // 
            this.tabPage_MoneyFlow.Controls.Add(this.bindingNavigator3);
            this.tabPage_MoneyFlow.Controls.Add(this.table_MoneyFlowDataGridView);
            this.tabPage_MoneyFlow.Location = new System.Drawing.Point(4, 22);
            this.tabPage_MoneyFlow.Name = "tabPage_MoneyFlow";
            this.tabPage_MoneyFlow.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_MoneyFlow.Size = new System.Drawing.Size(956, 393);
            this.tabPage_MoneyFlow.TabIndex = 2;
            this.tabPage_MoneyFlow.Text = "Финансы";
            this.tabPage_MoneyFlow.UseVisualStyleBackColor = true;
            // 
            // bindingNavigator3
            // 
            this.bindingNavigator3.AddNewItem = this.bindingNavigatorAddNewItem2;
            this.bindingNavigator3.BindingSource = this.table_MoneyFlowBindingSource;
            this.bindingNavigator3.CountItem = this.bindingNavigatorCountItem2;
            this.bindingNavigator3.CountItemFormat = "из {0}";
            this.bindingNavigator3.DeleteItem = this.bindingNavigatorDeleteItem2;
            this.bindingNavigator3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel3,
            this.toolStripSeparator3,
            this.bindingNavigatorMoveFirstItem2,
            this.bindingNavigatorMovePreviousItem2,
            this.bindingNavigatorSeparator6,
            this.bindingNavigatorPositionItem2,
            this.bindingNavigatorCountItem2,
            this.bindingNavigatorSeparator7,
            this.bindingNavigatorMoveNextItem2,
            this.bindingNavigatorMoveLastItem2,
            this.bindingNavigatorSeparator8,
            this.bindingNavigatorAddNewItem2,
            this.bindingNavigatorDeleteItem2,
            this.toolStripButtonAddMoneyFlow,
            this.toolStripSeparator10,
            this.toolStripButtonDeleteMoneyFlow,
            this.toolStripSeparator11,
            this.toolStripSeparator18,
            this.toolStripButton_ToExcel3,
            this.toolStripSeparator19});
            this.bindingNavigator3.Location = new System.Drawing.Point(3, 3);
            this.bindingNavigator3.MoveFirstItem = this.bindingNavigatorMoveFirstItem2;
            this.bindingNavigator3.MoveLastItem = this.bindingNavigatorMoveLastItem2;
            this.bindingNavigator3.MoveNextItem = this.bindingNavigatorMoveNextItem2;
            this.bindingNavigator3.MovePreviousItem = this.bindingNavigatorMovePreviousItem2;
            this.bindingNavigator3.Name = "bindingNavigator3";
            this.bindingNavigator3.PositionItem = this.bindingNavigatorPositionItem2;
            this.bindingNavigator3.Size = new System.Drawing.Size(950, 28);
            this.bindingNavigator3.TabIndex = 1;
            this.bindingNavigator3.Text = "bindingNavigator3";
            // 
            // bindingNavigatorAddNewItem2
            // 
            this.bindingNavigatorAddNewItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem2.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem2.Image")));
            this.bindingNavigatorAddNewItem2.Name = "bindingNavigatorAddNewItem2";
            this.bindingNavigatorAddNewItem2.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem2.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorAddNewItem2.Text = "Добавить";
            this.bindingNavigatorAddNewItem2.Visible = false;
            // 
            // table_MoneyFlowBindingSource
            // 
            this.table_MoneyFlowBindingSource.DataMember = "FK_Table_MoneyMoves_Table_Projects";
            this.table_MoneyFlowBindingSource.DataSource = this.table_ProjectsBindingSource;
            // 
            // bindingNavigatorCountItem2
            // 
            this.bindingNavigatorCountItem2.Name = "bindingNavigatorCountItem2";
            this.bindingNavigatorCountItem2.Size = new System.Drawing.Size(36, 25);
            this.bindingNavigatorCountItem2.Text = "из {0}";
            this.bindingNavigatorCountItem2.ToolTipText = "Общее число элементов";
            // 
            // bindingNavigatorDeleteItem2
            // 
            this.bindingNavigatorDeleteItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem2.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem2.Image")));
            this.bindingNavigatorDeleteItem2.Name = "bindingNavigatorDeleteItem2";
            this.bindingNavigatorDeleteItem2.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem2.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorDeleteItem2.Text = "Удалить";
            this.bindingNavigatorDeleteItem2.Visible = false;
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(221, 25);
            this.toolStripLabel3.Text = "Расходы и поступления:";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveFirstItem2
            // 
            this.bindingNavigatorMoveFirstItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem2.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem2.Image")));
            this.bindingNavigatorMoveFirstItem2.Name = "bindingNavigatorMoveFirstItem2";
            this.bindingNavigatorMoveFirstItem2.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem2.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveFirstItem2.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem2
            // 
            this.bindingNavigatorMovePreviousItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem2.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem2.Image")));
            this.bindingNavigatorMovePreviousItem2.Name = "bindingNavigatorMovePreviousItem2";
            this.bindingNavigatorMovePreviousItem2.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem2.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMovePreviousItem2.Text = "Переместить назад";
            // 
            // bindingNavigatorSeparator6
            // 
            this.bindingNavigatorSeparator6.Name = "bindingNavigatorSeparator6";
            this.bindingNavigatorSeparator6.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorPositionItem2
            // 
            this.bindingNavigatorPositionItem2.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem2.AutoSize = false;
            this.bindingNavigatorPositionItem2.Name = "bindingNavigatorPositionItem2";
            this.bindingNavigatorPositionItem2.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem2.Text = "0";
            this.bindingNavigatorPositionItem2.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorSeparator7
            // 
            this.bindingNavigatorSeparator7.Name = "bindingNavigatorSeparator7";
            this.bindingNavigatorSeparator7.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveNextItem2
            // 
            this.bindingNavigatorMoveNextItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem2.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem2.Image")));
            this.bindingNavigatorMoveNextItem2.Name = "bindingNavigatorMoveNextItem2";
            this.bindingNavigatorMoveNextItem2.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem2.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveNextItem2.Text = "Переместить вперед";
            // 
            // bindingNavigatorMoveLastItem2
            // 
            this.bindingNavigatorMoveLastItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem2.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem2.Image")));
            this.bindingNavigatorMoveLastItem2.Name = "bindingNavigatorMoveLastItem2";
            this.bindingNavigatorMoveLastItem2.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem2.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveLastItem2.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator8
            // 
            this.bindingNavigatorSeparator8.Name = "bindingNavigatorSeparator8";
            this.bindingNavigatorSeparator8.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonAddMoneyFlow
            // 
            this.toolStripButtonAddMoneyFlow.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonAddMoneyFlow.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAddMoneyFlow.Image")));
            this.toolStripButtonAddMoneyFlow.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAddMoneyFlow.Name = "toolStripButtonAddMoneyFlow";
            this.toolStripButtonAddMoneyFlow.Size = new System.Drawing.Size(63, 25);
            this.toolStripButtonAddMoneyFlow.Text = "Добавить";
            this.toolStripButtonAddMoneyFlow.Click += new System.EventHandler(this.toolStripButtonAddMoneyFlow_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonDeleteMoneyFlow
            // 
            this.toolStripButtonDeleteMoneyFlow.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonDeleteMoneyFlow.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDeleteMoneyFlow.Image")));
            this.toolStripButtonDeleteMoneyFlow.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDeleteMoneyFlow.Name = "toolStripButtonDeleteMoneyFlow";
            this.toolStripButtonDeleteMoneyFlow.Size = new System.Drawing.Size(55, 25);
            this.toolStripButtonDeleteMoneyFlow.Text = "Удалить";
            this.toolStripButtonDeleteMoneyFlow.Click += new System.EventHandler(this.toolStripButtonDeleteMoneyFlow_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator18
            // 
            this.toolStripSeparator18.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator18.Name = "toolStripSeparator18";
            this.toolStripSeparator18.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButton_ToExcel3
            // 
            this.toolStripButton_ToExcel3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton_ToExcel3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_ToExcel3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_ToExcel3.Image")));
            this.toolStripButton_ToExcel3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_ToExcel3.Name = "toolStripButton_ToExcel3";
            this.toolStripButton_ToExcel3.Size = new System.Drawing.Size(46, 25);
            this.toolStripButton_ToExcel3.Text = "в Excel";
            this.toolStripButton_ToExcel3.Click += new System.EventHandler(this.toolStripButton_ToExcel3_Click);
            // 
            // toolStripSeparator19
            // 
            this.toolStripSeparator19.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator19.Name = "toolStripSeparator19";
            this.toolStripSeparator19.Size = new System.Drawing.Size(6, 28);
            // 
            // table_MoneyFlowDataGridView
            // 
            this.table_MoneyFlowDataGridView.AllowUserToAddRows = false;
            this.table_MoneyFlowDataGridView.AllowUserToDeleteRows = false;
            this.table_MoneyFlowDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.table_MoneyFlowDataGridView.AutoGenerateColumns = false;
            this.table_MoneyFlowDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_MoneyFlowDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MoneyFlowID,
            this.MoneyFlowProjectID,
            this.MoneyFlowDate,
            this.MoneyFlowSum,
            this.MoneyFlowIsCash,
            this.MoneyFlowEmployeeID,
            this.MoneyFlowMaterialID,
            this.MoneyFlowComment,
            this.MoneyFlowState});
            this.table_MoneyFlowDataGridView.DataSource = this.table_MoneyFlowBindingSource;
            this.table_MoneyFlowDataGridView.Location = new System.Drawing.Point(0, 31);
            this.table_MoneyFlowDataGridView.MultiSelect = false;
            this.table_MoneyFlowDataGridView.Name = "table_MoneyFlowDataGridView";
            this.table_MoneyFlowDataGridView.Size = new System.Drawing.Size(953, 362);
            this.table_MoneyFlowDataGridView.TabIndex = 0;
            this.table_MoneyFlowDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.table_MoneyFlowDataGridView_CellBeginEdit);
            this.table_MoneyFlowDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.table_MoneyFlowDataGridView_CellClick);
            this.table_MoneyFlowDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.table_MoneyFlowDataGridView_CellValueChanged);
            // 
            // MoneyFlowID
            // 
            this.MoneyFlowID.DataPropertyName = "id";
            this.MoneyFlowID.HeaderText = "id";
            this.MoneyFlowID.Name = "MoneyFlowID";
            this.MoneyFlowID.Visible = false;
            // 
            // MoneyFlowProjectID
            // 
            this.MoneyFlowProjectID.DataPropertyName = "ProjectID";
            this.MoneyFlowProjectID.DataSource = this.tableProjectsBindingSource1;
            this.MoneyFlowProjectID.DisplayMember = "Name";
            this.MoneyFlowProjectID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.MoneyFlowProjectID.HeaderText = "Заказ";
            this.MoneyFlowProjectID.Name = "MoneyFlowProjectID";
            this.MoneyFlowProjectID.ReadOnly = true;
            this.MoneyFlowProjectID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.MoneyFlowProjectID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.MoneyFlowProjectID.ValueMember = "id";
            // 
            // tableProjectsBindingSource1
            // 
            this.tableProjectsBindingSource1.DataMember = "Table_Projects";
            this.tableProjectsBindingSource1.DataSource = this.cA_DB_DataSet;
            this.tableProjectsBindingSource1.Sort = "Name";
            // 
            // MoneyFlowDate
            // 
            this.MoneyFlowDate.DataPropertyName = "Date";
            dataGridViewCellStyle3.Format = "d";
            dataGridViewCellStyle3.NullValue = null;
            this.MoneyFlowDate.DefaultCellStyle = dataGridViewCellStyle3;
            this.MoneyFlowDate.HeaderText = "Дата";
            this.MoneyFlowDate.Name = "MoneyFlowDate";
            // 
            // MoneyFlowSum
            // 
            this.MoneyFlowSum.DataPropertyName = "Sum";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "C2";
            dataGridViewCellStyle4.NullValue = null;
            this.MoneyFlowSum.DefaultCellStyle = dataGridViewCellStyle4;
            this.MoneyFlowSum.HeaderText = "Сумма";
            this.MoneyFlowSum.Name = "MoneyFlowSum";
            // 
            // MoneyFlowIsCash
            // 
            this.MoneyFlowIsCash.DataPropertyName = "IsCash";
            this.MoneyFlowIsCash.HeaderText = "Наличные";
            this.MoneyFlowIsCash.Name = "MoneyFlowIsCash";
            // 
            // MoneyFlowEmployeeID
            // 
            this.MoneyFlowEmployeeID.DataPropertyName = "EmployeeID";
            this.MoneyFlowEmployeeID.DataSource = this.tableEmployeesBindingSource;
            this.MoneyFlowEmployeeID.DisplayMember = "LastName";
            this.MoneyFlowEmployeeID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.MoneyFlowEmployeeID.HeaderText = "Сотрудник";
            this.MoneyFlowEmployeeID.Name = "MoneyFlowEmployeeID";
            this.MoneyFlowEmployeeID.ReadOnly = true;
            this.MoneyFlowEmployeeID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.MoneyFlowEmployeeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.MoneyFlowEmployeeID.ValueMember = "id";
            // 
            // tableEmployeesBindingSource
            // 
            this.tableEmployeesBindingSource.DataMember = "Table_Employees";
            this.tableEmployeesBindingSource.DataSource = this.cA_DB_DataSet;
            this.tableEmployeesBindingSource.Sort = "LastName";
            // 
            // MoneyFlowMaterialID
            // 
            this.MoneyFlowMaterialID.DataPropertyName = "MaterialID";
            this.MoneyFlowMaterialID.DataSource = this.tableMaterialsBindingSource1;
            this.MoneyFlowMaterialID.DisplayMember = "Name";
            this.MoneyFlowMaterialID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.MoneyFlowMaterialID.HeaderText = "Материал";
            this.MoneyFlowMaterialID.Name = "MoneyFlowMaterialID";
            this.MoneyFlowMaterialID.ReadOnly = true;
            this.MoneyFlowMaterialID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.MoneyFlowMaterialID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.MoneyFlowMaterialID.ValueMember = "id";
            // 
            // tableMaterialsBindingSource1
            // 
            this.tableMaterialsBindingSource1.DataMember = "Table_Materials";
            this.tableMaterialsBindingSource1.DataSource = this.cA_DB_DataSet;
            this.tableMaterialsBindingSource1.Sort = "Name";
            // 
            // MoneyFlowComment
            // 
            this.MoneyFlowComment.DataPropertyName = "Comment";
            this.MoneyFlowComment.HeaderText = "Комментарий";
            this.MoneyFlowComment.Name = "MoneyFlowComment";
            // 
            // MoneyFlowState
            // 
            this.MoneyFlowState.DataPropertyName = "State";
            this.MoneyFlowState.HeaderText = "State";
            this.MoneyFlowState.Name = "MoneyFlowState";
            this.MoneyFlowState.Visible = false;
            // 
            // tabPage_ProductionStages
            // 
            this.tabPage_ProductionStages.Controls.Add(this.bindingNavigator4);
            this.tabPage_ProductionStages.Controls.Add(this.table_ProductionStagesDataGridView);
            this.tabPage_ProductionStages.Location = new System.Drawing.Point(4, 22);
            this.tabPage_ProductionStages.Name = "tabPage_ProductionStages";
            this.tabPage_ProductionStages.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_ProductionStages.Size = new System.Drawing.Size(956, 393);
            this.tabPage_ProductionStages.TabIndex = 1;
            this.tabPage_ProductionStages.Text = "График производства";
            this.tabPage_ProductionStages.UseVisualStyleBackColor = true;
            // 
            // bindingNavigator4
            // 
            this.bindingNavigator4.AddNewItem = this.bindingNavigatorAddNewItem3;
            this.bindingNavigator4.BindingSource = this.table_ProductionStagesBindingSource;
            this.bindingNavigator4.CountItem = this.bindingNavigatorCountItem3;
            this.bindingNavigator4.CountItemFormat = "из {0}";
            this.bindingNavigator4.DeleteItem = this.bindingNavigatorDeleteItem3;
            this.bindingNavigator4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel4,
            this.toolStripSeparator4,
            this.bindingNavigatorMoveFirstItem3,
            this.bindingNavigatorMovePreviousItem3,
            this.bindingNavigatorSeparator9,
            this.bindingNavigatorPositionItem3,
            this.bindingNavigatorCountItem3,
            this.bindingNavigatorSeparator10,
            this.bindingNavigatorMoveNextItem3,
            this.bindingNavigatorMoveLastItem3,
            this.bindingNavigatorSeparator11,
            this.bindingNavigatorAddNewItem3,
            this.bindingNavigatorDeleteItem3,
            this.toolStripButtonAddProductionStage,
            this.toolStripSeparator12,
            this.toolStripButtonDeleteProductionStage,
            this.toolStripSeparator13,
            this.toolStripSeparator20,
            this.toolStripButton_ToExcel4,
            this.toolStripSeparator21});
            this.bindingNavigator4.Location = new System.Drawing.Point(3, 3);
            this.bindingNavigator4.MoveFirstItem = this.bindingNavigatorMoveFirstItem3;
            this.bindingNavigator4.MoveLastItem = this.bindingNavigatorMoveLastItem3;
            this.bindingNavigator4.MoveNextItem = this.bindingNavigatorMoveNextItem3;
            this.bindingNavigator4.MovePreviousItem = this.bindingNavigatorMovePreviousItem3;
            this.bindingNavigator4.Name = "bindingNavigator4";
            this.bindingNavigator4.PositionItem = this.bindingNavigatorPositionItem3;
            this.bindingNavigator4.Size = new System.Drawing.Size(950, 28);
            this.bindingNavigator4.TabIndex = 1;
            this.bindingNavigator4.Text = "bindingNavigator4";
            // 
            // bindingNavigatorAddNewItem3
            // 
            this.bindingNavigatorAddNewItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem3.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem3.Image")));
            this.bindingNavigatorAddNewItem3.Name = "bindingNavigatorAddNewItem3";
            this.bindingNavigatorAddNewItem3.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem3.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorAddNewItem3.Text = "Добавить";
            this.bindingNavigatorAddNewItem3.Visible = false;
            // 
            // table_ProductionStagesBindingSource
            // 
            this.table_ProductionStagesBindingSource.DataMember = "FK_Table_ProductionStages_Table_Projects";
            this.table_ProductionStagesBindingSource.DataSource = this.table_ProjectsBindingSource;
            this.table_ProductionStagesBindingSource.Sort = "Date";
            // 
            // bindingNavigatorCountItem3
            // 
            this.bindingNavigatorCountItem3.Name = "bindingNavigatorCountItem3";
            this.bindingNavigatorCountItem3.Size = new System.Drawing.Size(36, 25);
            this.bindingNavigatorCountItem3.Text = "из {0}";
            this.bindingNavigatorCountItem3.ToolTipText = "Общее число элементов";
            // 
            // bindingNavigatorDeleteItem3
            // 
            this.bindingNavigatorDeleteItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem3.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem3.Image")));
            this.bindingNavigatorDeleteItem3.Name = "bindingNavigatorDeleteItem3";
            this.bindingNavigatorDeleteItem3.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem3.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorDeleteItem3.Text = "Удалить";
            this.bindingNavigatorDeleteItem3.Visible = false;
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(204, 25);
            this.toolStripLabel4.Text = "График производства:";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveFirstItem3
            // 
            this.bindingNavigatorMoveFirstItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem3.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem3.Image")));
            this.bindingNavigatorMoveFirstItem3.Name = "bindingNavigatorMoveFirstItem3";
            this.bindingNavigatorMoveFirstItem3.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem3.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveFirstItem3.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem3
            // 
            this.bindingNavigatorMovePreviousItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem3.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem3.Image")));
            this.bindingNavigatorMovePreviousItem3.Name = "bindingNavigatorMovePreviousItem3";
            this.bindingNavigatorMovePreviousItem3.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem3.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMovePreviousItem3.Text = "Переместить назад";
            // 
            // bindingNavigatorSeparator9
            // 
            this.bindingNavigatorSeparator9.Name = "bindingNavigatorSeparator9";
            this.bindingNavigatorSeparator9.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorPositionItem3
            // 
            this.bindingNavigatorPositionItem3.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem3.AutoSize = false;
            this.bindingNavigatorPositionItem3.Name = "bindingNavigatorPositionItem3";
            this.bindingNavigatorPositionItem3.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem3.Text = "0";
            this.bindingNavigatorPositionItem3.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorSeparator10
            // 
            this.bindingNavigatorSeparator10.Name = "bindingNavigatorSeparator10";
            this.bindingNavigatorSeparator10.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveNextItem3
            // 
            this.bindingNavigatorMoveNextItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem3.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem3.Image")));
            this.bindingNavigatorMoveNextItem3.Name = "bindingNavigatorMoveNextItem3";
            this.bindingNavigatorMoveNextItem3.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem3.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveNextItem3.Text = "Переместить вперед";
            // 
            // bindingNavigatorMoveLastItem3
            // 
            this.bindingNavigatorMoveLastItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem3.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem3.Image")));
            this.bindingNavigatorMoveLastItem3.Name = "bindingNavigatorMoveLastItem3";
            this.bindingNavigatorMoveLastItem3.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem3.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveLastItem3.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator11
            // 
            this.bindingNavigatorSeparator11.Name = "bindingNavigatorSeparator11";
            this.bindingNavigatorSeparator11.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonAddProductionStage
            // 
            this.toolStripButtonAddProductionStage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonAddProductionStage.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAddProductionStage.Image")));
            this.toolStripButtonAddProductionStage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAddProductionStage.Name = "toolStripButtonAddProductionStage";
            this.toolStripButtonAddProductionStage.Size = new System.Drawing.Size(63, 25);
            this.toolStripButtonAddProductionStage.Text = "Добавить";
            this.toolStripButtonAddProductionStage.Click += new System.EventHandler(this.toolStripButtonAddProductionStage_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonDeleteProductionStage
            // 
            this.toolStripButtonDeleteProductionStage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonDeleteProductionStage.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDeleteProductionStage.Image")));
            this.toolStripButtonDeleteProductionStage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDeleteProductionStage.Name = "toolStripButtonDeleteProductionStage";
            this.toolStripButtonDeleteProductionStage.Size = new System.Drawing.Size(55, 25);
            this.toolStripButtonDeleteProductionStage.Text = "Удалить";
            this.toolStripButtonDeleteProductionStage.Click += new System.EventHandler(this.toolStripButtonDeleteProductionStage_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator20
            // 
            this.toolStripSeparator20.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator20.Name = "toolStripSeparator20";
            this.toolStripSeparator20.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButton_ToExcel4
            // 
            this.toolStripButton_ToExcel4.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton_ToExcel4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_ToExcel4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_ToExcel4.Image")));
            this.toolStripButton_ToExcel4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_ToExcel4.Name = "toolStripButton_ToExcel4";
            this.toolStripButton_ToExcel4.Size = new System.Drawing.Size(46, 25);
            this.toolStripButton_ToExcel4.Text = "в Excel";
            this.toolStripButton_ToExcel4.Click += new System.EventHandler(this.toolStripButton_ToExcel4_Click);
            // 
            // toolStripSeparator21
            // 
            this.toolStripSeparator21.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator21.Name = "toolStripSeparator21";
            this.toolStripSeparator21.Size = new System.Drawing.Size(6, 28);
            // 
            // table_ProductionStagesDataGridView
            // 
            this.table_ProductionStagesDataGridView.AllowUserToAddRows = false;
            this.table_ProductionStagesDataGridView.AllowUserToDeleteRows = false;
            this.table_ProductionStagesDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.table_ProductionStagesDataGridView.AutoGenerateColumns = false;
            this.table_ProductionStagesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_ProductionStagesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProductionStageID,
            this.ProductionStageProjectID,
            this.ProductionStageName,
            this.ProductionStageStartDate,
            this.ProductionStageDuration,
            this.ProductionStageState});
            this.table_ProductionStagesDataGridView.DataSource = this.table_ProductionStagesBindingSource;
            this.table_ProductionStagesDataGridView.Location = new System.Drawing.Point(0, 31);
            this.table_ProductionStagesDataGridView.MultiSelect = false;
            this.table_ProductionStagesDataGridView.Name = "table_ProductionStagesDataGridView";
            this.table_ProductionStagesDataGridView.Size = new System.Drawing.Size(953, 362);
            this.table_ProductionStagesDataGridView.TabIndex = 0;
            this.table_ProductionStagesDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.table_ProductionStagesDataGridView_CellBeginEdit);
            this.table_ProductionStagesDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.table_ProductionStagesDataGridView_CellValueChanged);
            // 
            // ProductionStageID
            // 
            this.ProductionStageID.DataPropertyName = "id";
            this.ProductionStageID.HeaderText = "id";
            this.ProductionStageID.Name = "ProductionStageID";
            this.ProductionStageID.Visible = false;
            // 
            // ProductionStageProjectID
            // 
            this.ProductionStageProjectID.DataPropertyName = "ProjectID";
            this.ProductionStageProjectID.HeaderText = "ProjectID";
            this.ProductionStageProjectID.Name = "ProductionStageProjectID";
            this.ProductionStageProjectID.Visible = false;
            // 
            // ProductionStageName
            // 
            this.ProductionStageName.DataPropertyName = "Name";
            this.ProductionStageName.HeaderText = "Этап";
            this.ProductionStageName.Name = "ProductionStageName";
            // 
            // ProductionStageStartDate
            // 
            this.ProductionStageStartDate.DataPropertyName = "Date";
            this.ProductionStageStartDate.HeaderText = "Дата начала";
            this.ProductionStageStartDate.Name = "ProductionStageStartDate";
            // 
            // ProductionStageDuration
            // 
            this.ProductionStageDuration.DataPropertyName = "Duration";
            this.ProductionStageDuration.HeaderText = "Длительность, кал.дн.";
            this.ProductionStageDuration.Name = "ProductionStageDuration";
            // 
            // ProductionStageState
            // 
            this.ProductionStageState.DataPropertyName = "State";
            this.ProductionStageState.HeaderText = "State";
            this.ProductionStageState.Name = "ProductionStageState";
            this.ProductionStageState.Visible = false;
            // 
            // tabPage_Files
            // 
            this.tabPage_Files.AutoScroll = true;
            this.tabPage_Files.Controls.Add(this.table_ProjectFilesDataGridView);
            this.tabPage_Files.Controls.Add(this.bindingNavigator5);
            this.tabPage_Files.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Files.Name = "tabPage_Files";
            this.tabPage_Files.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Files.Size = new System.Drawing.Size(956, 393);
            this.tabPage_Files.TabIndex = 3;
            this.tabPage_Files.Text = "Файлы";
            this.tabPage_Files.UseVisualStyleBackColor = true;
            // 
            // table_ProjectFilesDataGridView
            // 
            this.table_ProjectFilesDataGridView.AllowUserToAddRows = false;
            this.table_ProjectFilesDataGridView.AllowUserToDeleteRows = false;
            this.table_ProjectFilesDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.table_ProjectFilesDataGridView.AutoGenerateColumns = false;
            this.table_ProjectFilesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_ProjectFilesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FileID,
            this.FileState,
            this.FileProjectID,
            this.FileName,
            this.FileExtension,
            this.FileData,
            this.FileComment});
            this.table_ProjectFilesDataGridView.DataSource = this.table_ProjectFilesBindingSource;
            this.table_ProjectFilesDataGridView.Location = new System.Drawing.Point(0, 31);
            this.table_ProjectFilesDataGridView.MultiSelect = false;
            this.table_ProjectFilesDataGridView.Name = "table_ProjectFilesDataGridView";
            this.table_ProjectFilesDataGridView.Size = new System.Drawing.Size(956, 362);
            this.table_ProjectFilesDataGridView.TabIndex = 1;
            this.table_ProjectFilesDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.table_ProjectFilesDataGridView_CellBeginEdit);
            this.table_ProjectFilesDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.table_ProjectFilesDataGridView_CellClick);
            this.table_ProjectFilesDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.table_ProjectFilesDataGridView_CellValueChanged);
            // 
            // tableProjectsBindingSource2
            // 
            this.tableProjectsBindingSource2.DataMember = "Table_Projects";
            this.tableProjectsBindingSource2.DataSource = this.cA_DB_DataSet;
            // 
            // table_ProjectFilesBindingSource
            // 
            this.table_ProjectFilesBindingSource.DataMember = "FK_Table_ProjectFiles_Table_Projects";
            this.table_ProjectFilesBindingSource.DataSource = this.table_ProjectsBindingSource;
            // 
            // bindingNavigator5
            // 
            this.bindingNavigator5.AddNewItem = this.bindingNavigatorAddNewItem4;
            this.bindingNavigator5.BindingSource = this.table_ProjectFilesBindingSource;
            this.bindingNavigator5.CountItem = this.bindingNavigatorCountItem4;
            this.bindingNavigator5.CountItemFormat = "из {0}";
            this.bindingNavigator5.DeleteItem = this.bindingNavigatorDeleteItem4;
            this.bindingNavigator5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel5,
            this.bindingNavigatorMoveFirstItem4,
            this.bindingNavigatorMovePreviousItem4,
            this.bindingNavigatorSeparator12,
            this.bindingNavigatorPositionItem4,
            this.bindingNavigatorCountItem4,
            this.bindingNavigatorSeparator13,
            this.bindingNavigatorMoveNextItem4,
            this.bindingNavigatorMoveLastItem4,
            this.bindingNavigatorSeparator14,
            this.bindingNavigatorAddNewItem4,
            this.bindingNavigatorDeleteItem4,
            this.toolStripButtonAddFile,
            this.toolStripSeparator22,
            this.toolStripButtonOpenFile,
            this.toolStripSeparator26,
            this.toolStripButtonDeleteFile,
            this.toolStripSeparator23,
            this.toolStripSeparator24,
            this.toolStripButton_ToExcel5,
            this.toolStripSeparator25});
            this.bindingNavigator5.Location = new System.Drawing.Point(3, 3);
            this.bindingNavigator5.MoveFirstItem = this.bindingNavigatorMoveFirstItem4;
            this.bindingNavigator5.MoveLastItem = this.bindingNavigatorMoveLastItem4;
            this.bindingNavigator5.MoveNextItem = this.bindingNavigatorMoveNextItem4;
            this.bindingNavigator5.MovePreviousItem = this.bindingNavigatorMovePreviousItem4;
            this.bindingNavigator5.Name = "bindingNavigator5";
            this.bindingNavigator5.PositionItem = this.bindingNavigatorPositionItem4;
            this.bindingNavigator5.Size = new System.Drawing.Size(950, 28);
            this.bindingNavigator5.TabIndex = 0;
            this.bindingNavigator5.Text = "bindingNavigator5";
            // 
            // bindingNavigatorAddNewItem4
            // 
            this.bindingNavigatorAddNewItem4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem4.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem4.Image")));
            this.bindingNavigatorAddNewItem4.Name = "bindingNavigatorAddNewItem4";
            this.bindingNavigatorAddNewItem4.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem4.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorAddNewItem4.Text = "Добавить";
            this.bindingNavigatorAddNewItem4.Visible = false;
            // 
            // bindingNavigatorCountItem4
            // 
            this.bindingNavigatorCountItem4.Name = "bindingNavigatorCountItem4";
            this.bindingNavigatorCountItem4.Size = new System.Drawing.Size(36, 25);
            this.bindingNavigatorCountItem4.Text = "из {0}";
            this.bindingNavigatorCountItem4.ToolTipText = "Общее число элементов";
            // 
            // bindingNavigatorDeleteItem4
            // 
            this.bindingNavigatorDeleteItem4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem4.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem4.Image")));
            this.bindingNavigatorDeleteItem4.Name = "bindingNavigatorDeleteItem4";
            this.bindingNavigatorDeleteItem4.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem4.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorDeleteItem4.Text = "Удалить";
            this.bindingNavigatorDeleteItem4.Visible = false;
            // 
            // toolStripLabel5
            // 
            this.toolStripLabel5.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripLabel5.Name = "toolStripLabel5";
            this.toolStripLabel5.Size = new System.Drawing.Size(74, 25);
            this.toolStripLabel5.Text = "Файлы:";
            // 
            // bindingNavigatorMoveFirstItem4
            // 
            this.bindingNavigatorMoveFirstItem4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem4.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem4.Image")));
            this.bindingNavigatorMoveFirstItem4.Name = "bindingNavigatorMoveFirstItem4";
            this.bindingNavigatorMoveFirstItem4.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem4.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveFirstItem4.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem4
            // 
            this.bindingNavigatorMovePreviousItem4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem4.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem4.Image")));
            this.bindingNavigatorMovePreviousItem4.Name = "bindingNavigatorMovePreviousItem4";
            this.bindingNavigatorMovePreviousItem4.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem4.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMovePreviousItem4.Text = "Переместить назад";
            // 
            // bindingNavigatorSeparator12
            // 
            this.bindingNavigatorSeparator12.Name = "bindingNavigatorSeparator12";
            this.bindingNavigatorSeparator12.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorPositionItem4
            // 
            this.bindingNavigatorPositionItem4.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem4.AutoSize = false;
            this.bindingNavigatorPositionItem4.Name = "bindingNavigatorPositionItem4";
            this.bindingNavigatorPositionItem4.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem4.Text = "0";
            this.bindingNavigatorPositionItem4.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorSeparator13
            // 
            this.bindingNavigatorSeparator13.Name = "bindingNavigatorSeparator13";
            this.bindingNavigatorSeparator13.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveNextItem4
            // 
            this.bindingNavigatorMoveNextItem4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem4.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem4.Image")));
            this.bindingNavigatorMoveNextItem4.Name = "bindingNavigatorMoveNextItem4";
            this.bindingNavigatorMoveNextItem4.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem4.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveNextItem4.Text = "Переместить вперед";
            // 
            // bindingNavigatorMoveLastItem4
            // 
            this.bindingNavigatorMoveLastItem4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem4.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem4.Image")));
            this.bindingNavigatorMoveLastItem4.Name = "bindingNavigatorMoveLastItem4";
            this.bindingNavigatorMoveLastItem4.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem4.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveLastItem4.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator14
            // 
            this.bindingNavigatorSeparator14.Name = "bindingNavigatorSeparator14";
            this.bindingNavigatorSeparator14.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonAddFile
            // 
            this.toolStripButtonAddFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonAddFile.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAddFile.Image")));
            this.toolStripButtonAddFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAddFile.Name = "toolStripButtonAddFile";
            this.toolStripButtonAddFile.Size = new System.Drawing.Size(63, 25);
            this.toolStripButtonAddFile.Text = "Добавить";
            this.toolStripButtonAddFile.Click += new System.EventHandler(this.toolStripButtonAddFile_Click);
            // 
            // toolStripSeparator22
            // 
            this.toolStripSeparator22.Name = "toolStripSeparator22";
            this.toolStripSeparator22.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonOpenFile
            // 
            this.toolStripButtonOpenFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonOpenFile.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonOpenFile.Image")));
            this.toolStripButtonOpenFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonOpenFile.Name = "toolStripButtonOpenFile";
            this.toolStripButtonOpenFile.Size = new System.Drawing.Size(58, 25);
            this.toolStripButtonOpenFile.Text = "Открыть";
            this.toolStripButtonOpenFile.Click += new System.EventHandler(this.toolStripButtonOpenFile_Click);
            // 
            // toolStripSeparator26
            // 
            this.toolStripSeparator26.Name = "toolStripSeparator26";
            this.toolStripSeparator26.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonDeleteFile
            // 
            this.toolStripButtonDeleteFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonDeleteFile.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDeleteFile.Image")));
            this.toolStripButtonDeleteFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDeleteFile.Name = "toolStripButtonDeleteFile";
            this.toolStripButtonDeleteFile.Size = new System.Drawing.Size(55, 25);
            this.toolStripButtonDeleteFile.Text = "Удалить";
            this.toolStripButtonDeleteFile.Click += new System.EventHandler(this.toolStripButtonDeleteFile_Click);
            // 
            // toolStripSeparator23
            // 
            this.toolStripSeparator23.Name = "toolStripSeparator23";
            this.toolStripSeparator23.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator24
            // 
            this.toolStripSeparator24.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator24.Name = "toolStripSeparator24";
            this.toolStripSeparator24.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButton_ToExcel5
            // 
            this.toolStripButton_ToExcel5.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton_ToExcel5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_ToExcel5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_ToExcel5.Image")));
            this.toolStripButton_ToExcel5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_ToExcel5.Name = "toolStripButton_ToExcel5";
            this.toolStripButton_ToExcel5.Size = new System.Drawing.Size(46, 25);
            this.toolStripButton_ToExcel5.Text = "в Excel";
            this.toolStripButton_ToExcel5.Click += new System.EventHandler(this.toolStripButton_ToExcel5_Click);
            // 
            // toolStripSeparator25
            // 
            this.toolStripSeparator25.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator25.Name = "toolStripSeparator25";
            this.toolStripSeparator25.Size = new System.Drawing.Size(6, 28);
            // 
            // table_ProjectsTableAdapter
            // 
            this.table_ProjectsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Table_CompaniesTableAdapter = null;
            this.tableAdapterManager.Table_CompanyTypesTableAdapter = null;
            this.tableAdapterManager.Table_EmployeesTableAdapter = null;
            this.tableAdapterManager.Table_HistoryTableAdapter = null;
            this.tableAdapterManager.Table_MaterialGroupsTableAdapter = null;
            this.tableAdapterManager.Table_MaterialsTableAdapter = null;
            this.tableAdapterManager.Table_MeasureUnitsTableAdapter = null;
            this.tableAdapterManager.Table_MoneyFlowTableAdapter = null;
            this.tableAdapterManager.Table_ProductionStagesTableAdapter = null;
            this.tableAdapterManager.Table_ProjectFilesTableAdapter = null;
            this.tableAdapterManager.Table_ProjectsTableAdapter = this.table_ProjectsTableAdapter;
            this.tableAdapterManager.Table_ProjectStatusesTableAdapter = null;
            this.tableAdapterManager.Table_SectionsTableAdapter = null;
            this.tableAdapterManager.Table_SubProjectsTableAdapter = null;
            this.tableAdapterManager.Table_TEOMaterialsTableAdapter = null;
            this.tableAdapterManager.Table_UsersTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = CA.CA_DB_DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // table_CompaniesTableAdapter
            // 
            this.table_CompaniesTableAdapter.ClearBeforeFill = true;
            // 
            // table_ProjectStatusesTableAdapter
            // 
            this.table_ProjectStatusesTableAdapter.ClearBeforeFill = true;
            // 
            // table_SubProjectsTableAdapter
            // 
            this.table_SubProjectsTableAdapter.ClearBeforeFill = true;
            // 
            // table_TEOMaterialsTableAdapter
            // 
            this.table_TEOMaterialsTableAdapter.ClearBeforeFill = true;
            // 
            // table_MaterialsTableAdapter
            // 
            this.table_MaterialsTableAdapter.ClearBeforeFill = true;
            // 
            // table_MoneyFlowTableAdapter
            // 
            this.table_MoneyFlowTableAdapter.ClearBeforeFill = true;
            // 
            // table_EmployeesTableAdapter
            // 
            this.table_EmployeesTableAdapter.ClearBeforeFill = true;
            // 
            // table_ProductionStagesTableAdapter
            // 
            this.table_ProductionStagesTableAdapter.ClearBeforeFill = true;
            // 
            // table_UsersTableAdapter
            // 
            this.table_UsersTableAdapter.ClearBeforeFill = true;
            // 
            // table_ProjectFilesTableAdapter
            // 
            this.table_ProjectFilesTableAdapter.ClearBeforeFill = true;
            // 
            // FileID
            // 
            this.FileID.DataPropertyName = "id";
            this.FileID.HeaderText = "id";
            this.FileID.Name = "FileID";
            this.FileID.Visible = false;
            // 
            // FileState
            // 
            this.FileState.DataPropertyName = "State";
            this.FileState.HeaderText = "State";
            this.FileState.Name = "FileState";
            this.FileState.Visible = false;
            // 
            // FileProjectID
            // 
            this.FileProjectID.DataPropertyName = "ProjectID";
            this.FileProjectID.DataSource = this.tableProjectsBindingSource2;
            this.FileProjectID.DisplayMember = "Name";
            this.FileProjectID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.FileProjectID.HeaderText = "Проект";
            this.FileProjectID.Name = "FileProjectID";
            this.FileProjectID.ReadOnly = true;
            this.FileProjectID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.FileProjectID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.FileProjectID.ValueMember = "id";
            // 
            // FileName
            // 
            this.FileName.DataPropertyName = "Name";
            this.FileName.HeaderText = "Имя файла";
            this.FileName.Name = "FileName";
            // 
            // FileExtension
            // 
            this.FileExtension.DataPropertyName = "Extension";
            this.FileExtension.HeaderText = "Тип файла";
            this.FileExtension.Name = "FileExtension";
            // 
            // FileData
            // 
            this.FileData.DataPropertyName = "FileVarbinary";
            this.FileData.HeaderText = "Файл";
            this.FileData.Name = "FileData";
            this.FileData.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.FileData.Visible = false;
            // 
            // FileComment
            // 
            this.FileComment.DataPropertyName = "Comment";
            this.FileComment.HeaderText = "Комментарий";
            this.FileComment.Name = "FileComment";
            this.FileComment.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Form_ProjectCurrent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(968, 702);
            this.Controls.Add(this.splitContainer3);
            this.Name = "Form_ProjectCurrent";
            this.Text = "Заказ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_ProjectCurrent_FormClosing);
            this.Load += new System.EventHandler(this.Form_ProjectCurrent_Load);
            this.Controls.SetChildIndex(this.splitContainer3, 0);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel1.PerformLayout();
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.table_ProjectsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cA_DB_DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableUsersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.marginNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sumNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableCompaniesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableProjectStatusesBindingSource)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabPage_TEO.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_SubProjectsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_SubProjectsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableProjectsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator2)).EndInit();
            this.bindingNavigator2.ResumeLayout(false);
            this.bindingNavigator2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_TEOMaterialsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_TEOMaterialsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableSubProjectsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableMaterialsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableCompaniesBindingSource1)).EndInit();
            this.tabPage_MoneyFlow.ResumeLayout(false);
            this.tabPage_MoneyFlow.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator3)).EndInit();
            this.bindingNavigator3.ResumeLayout(false);
            this.bindingNavigator3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_MoneyFlowBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_MoneyFlowDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableProjectsBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableEmployeesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableMaterialsBindingSource1)).EndInit();
            this.tabPage_ProductionStages.ResumeLayout(false);
            this.tabPage_ProductionStages.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator4)).EndInit();
            this.bindingNavigator4.ResumeLayout(false);
            this.bindingNavigator4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_ProductionStagesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_ProductionStagesDataGridView)).EndInit();
            this.tabPage_Files.ResumeLayout(false);
            this.tabPage_Files.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_ProjectFilesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableProjectsBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_ProjectFilesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator5)).EndInit();
            this.bindingNavigator5.ResumeLayout(false);
            this.bindingNavigator5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CA_DB_DataSet cA_DB_DataSet;
        private System.Windows.Forms.BindingSource table_ProjectsBindingSource;
        private CA_DB_DataSetTableAdapters.Table_ProjectsTableAdapter table_ProjectsTableAdapter;
        private CA_DB_DataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox idTextBox;
        private System.Windows.Forms.TextBox numTextBox;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.DateTimePicker dateDateTimePicker;
        private System.Windows.Forms.TextBox commentTextBox;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage_TEO;
        private System.Windows.Forms.ComboBox companyComboBox;
        private System.Windows.Forms.ComboBox statusComboBox;
        private System.Windows.Forms.Button button_Refresh;
        private System.Windows.Forms.Button button_TEO_to_Excel;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TabPage tabPage_ProductionStages;
        private System.Windows.Forms.BindingSource tableCompaniesBindingSource;
        private CA_DB_DataSetTableAdapters.Table_CompaniesTableAdapter table_CompaniesTableAdapter;
        private System.Windows.Forms.BindingSource tableProjectStatusesBindingSource;
        private CA_DB_DataSetTableAdapters.Table_ProjectStatusesTableAdapter table_ProjectStatusesTableAdapter;
        private System.Windows.Forms.BindingSource table_SubProjectsBindingSource;
        private CA_DB_DataSetTableAdapters.Table_SubProjectsTableAdapter table_SubProjectsTableAdapter;
        private System.Windows.Forms.DataGridView table_SubProjectsDataGridView;
        private System.Windows.Forms.BindingSource table_TEOMaterialsBindingSource;
        private CA_DB_DataSetTableAdapters.Table_TEOMaterialsTableAdapter table_TEOMaterialsTableAdapter;
        private System.Windows.Forms.DataGridView table_TEOMaterialsDataGridView;
        private System.Windows.Forms.BindingSource tableProjectsBindingSource;
        private System.Windows.Forms.BindingSource tableMaterialsBindingSource;
        private CA_DB_DataSetTableAdapters.Table_MaterialsTableAdapter table_MaterialsTableAdapter;
        private System.Windows.Forms.BindingSource tableSubProjectsBindingSource;
        private System.Windows.Forms.BindingSource tableCompaniesBindingSource1;
        private System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButtonDeleteSubProject;
        private System.Windows.Forms.BindingNavigator bindingNavigator2;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem1;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator3;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator4;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator5;
        private System.Windows.Forms.ToolStripButton toolStripButtonDeleteTEOMaterial;
        private System.Windows.Forms.Button button_Save;
        private System.Windows.Forms.NumericUpDown sumNumericUpDown;
        private System.Windows.Forms.NumericUpDown marginNumericUpDown;
        private System.Windows.Forms.TabPage tabPage_MoneyFlow;
        private System.Windows.Forms.BindingSource table_MoneyFlowBindingSource;
        private CA_DB_DataSetTableAdapters.Table_MoneyFlowTableAdapter table_MoneyFlowTableAdapter;
        private System.Windows.Forms.BindingNavigator bindingNavigator3;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem2;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem2;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem2;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem2;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem2;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator6;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem2;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator7;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem2;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem2;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator8;
        private System.Windows.Forms.ToolStripButton toolStripButtonDeleteMoneyFlow;
        private System.Windows.Forms.DataGridView table_MoneyFlowDataGridView;
        private System.Windows.Forms.BindingSource tableProjectsBindingSource1;
        private System.Windows.Forms.BindingSource tableEmployeesBindingSource;
        private CA_DB_DataSetTableAdapters.Table_EmployeesTableAdapter table_EmployeesTableAdapter;
        private System.Windows.Forms.BindingSource tableMaterialsBindingSource1;
        private System.Windows.Forms.BindingSource table_ProductionStagesBindingSource;
        private CA_DB_DataSetTableAdapters.Table_ProductionStagesTableAdapter table_ProductionStagesTableAdapter;
        private System.Windows.Forms.BindingNavigator bindingNavigator4;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem3;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem3;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem3;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem3;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem3;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator9;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem3;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator10;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem3;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem3;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator11;
        private System.Windows.Forms.ToolStripButton toolStripButtonAddProductionStage;
        private System.Windows.Forms.ToolStripButton toolStripButtonDeleteProductionStage;
        private System.Windows.Forms.DataGridView table_ProductionStagesDataGridView;
        private System.Windows.Forms.ToolStripButton toolStripButtonAddSubProject;
        private System.Windows.Forms.ToolStripButton toolStripButtonAddTEOMaterial;
        private System.Windows.Forms.ToolStripButton toolStripButtonAddMoneyFlow;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripButtonMove;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.TextBox stateTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn SubProjectID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SubProjectState;
        private System.Windows.Forms.DataGridViewComboBoxColumn SubProjectProjectID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SubProjectName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductionStageID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductionStageState;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductionStageProjectID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductionStageName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductionStageStartDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductionStageDuration;
        private System.Windows.Forms.BindingSource tableUsersBindingSource;
        private CA_DB_DataSetTableAdapters.Table_UsersTableAdapter table_UsersTableAdapter;
        private System.Windows.Forms.ComboBox managerIDComboBox;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripButton toolStripButton_ToExcel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripButton toolStripButton_ToExcel2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
        private System.Windows.Forms.ToolStripButton toolStripButton_ToExcel3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator19;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator20;
        private System.Windows.Forms.ToolStripButton toolStripButton_ToExcel4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator21;
        private System.Windows.Forms.DataGridViewTextBoxColumn TEOMaterialID;
        private System.Windows.Forms.DataGridViewComboBoxColumn TEOMaterialSubProjectID;
        private System.Windows.Forms.DataGridViewComboBoxColumn TEOMaterialMaterialID;
        private System.Windows.Forms.DataGridViewButtonColumn TEOMaterialSelectButton;
        private System.Windows.Forms.DataGridViewComboBoxColumn TEOMaterialCompanyID;
        private System.Windows.Forms.DataGridViewTextBoxColumn TEOMaterialPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn TEOMaterialCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn TEOMaterialComment;
        private System.Windows.Forms.DataGridViewTextBoxColumn TEOMaterialState;
        private System.Windows.Forms.TabPage tabPage_Files;
        private System.Windows.Forms.BindingNavigator bindingNavigator5;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem4;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem4;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem4;
        private System.Windows.Forms.ToolStripLabel toolStripLabel5;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem4;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem4;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator12;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem4;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator13;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem4;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem4;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator14;
        private System.Windows.Forms.ToolStripButton toolStripButtonAddFile;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator22;
        private System.Windows.Forms.ToolStripButton toolStripButtonDeleteFile;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator23;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator24;
        private System.Windows.Forms.ToolStripButton toolStripButton_ToExcel5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator25;
        private System.Windows.Forms.BindingSource table_ProjectFilesBindingSource;
        private CA_DB_DataSetTableAdapters.Table_ProjectFilesTableAdapter table_ProjectFilesTableAdapter;
        private System.Windows.Forms.DataGridView table_ProjectFilesDataGridView;
        private System.Windows.Forms.BindingSource tableProjectsBindingSource2;
        private System.Windows.Forms.DataGridViewImageColumn FileFileVarbinary;
        private System.Windows.Forms.ToolStripButton toolStripButtonOpenFile;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator26;
        private System.Windows.Forms.DataGridViewTextBoxColumn MoneyFlowID;
        private System.Windows.Forms.DataGridViewComboBoxColumn MoneyFlowProjectID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MoneyFlowDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn MoneyFlowSum;
        private System.Windows.Forms.DataGridViewCheckBoxColumn MoneyFlowIsCash;
        private System.Windows.Forms.DataGridViewComboBoxColumn MoneyFlowEmployeeID;
        private System.Windows.Forms.DataGridViewComboBoxColumn MoneyFlowMaterialID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MoneyFlowComment;
        private System.Windows.Forms.DataGridViewTextBoxColumn MoneyFlowState;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileID;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileState;
        private System.Windows.Forms.DataGridViewComboBoxColumn FileProjectID;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileExtension;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileData;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileComment;
    }
}