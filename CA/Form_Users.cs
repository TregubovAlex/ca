﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CA
{
    public partial class Form_Users : Form_BaseStyle
    {
        public override void BS_Fill()
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_Employees". При необходимости она может быть перемещена или удалена.
            this.tableEmployeesTableAdapter.Fill(this.cA_DB_DataSet.Table_Employees);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_Users". При необходимости она может быть перемещена или удалена.
            this.table_UsersTableAdapter.Fill(this.cA_DB_DataSet.Table_Users);
        }
        public override void BS_Update()
        {
            if (!IsLoaded) return;
            this.tableEmployeesBindingSource.EndEdit();
            this.tableEmployeesTableAdapter.Adapter.Update(cA_DB_DataSet);
            this.table_UsersBindingSource.EndEdit();
            this.table_UsersTableAdapter.Adapter.Update(cA_DB_DataSet);
        }
        private void table_UsersDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            BS_dgvSpecialEdit(sender as DataGridView, e);
        }
        private void table_UsersDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            BS_Save();
        }
        private void toolStripButtonAddUser_Click(object sender, EventArgs e)
        {
            IsLoaded = false;
            table_UsersBindingSource.AddNew();
            DataGridViewRow newrow = table_UsersDataGridView.Rows[table_UsersDataGridView.Rows.Count - 1];
            newrow.Cells["UserID"].Value = Guid.NewGuid();
            newrow.Cells["UserState"].Value = "normal";
            newrow.Cells["UserEmployeeID"].Value = "edf9ac8e-6b57-47fc-aa1f-513f286e446e"; // [ пусто ]
            newrow.Cells["UserPassword"].Value = "1234";
            newrow.Cells["UserPermission1"].Value = false;
            newrow.Cells["UserPermission2"].Value = false;
            IsLoaded = true;
            BS_Update();
            newrow.Cells["UserEmployeeID"].Selected = true;
        }
        private void toolStripButtonDeleteUser_Click(object sender, EventArgs e)
        {
            //BS_DeleteRow(table_UsersDataGridView); ссылки в базе!!!
        }
        public Form_Users()
        { InitializeComponent(); }
        private void Form_Users_Load(object sender, EventArgs e)
        {
            BS_Refresh(table_UsersDataGridView);
            IsLoaded = true;
        }
        private void toolStripButton_ToExcel_Click(object sender, EventArgs e)
        {
            BS_ToExcel(table_UsersDataGridView);
        }
    }
}
