﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CA
{
    public partial class Form_Companies : Form_BaseStyle
    {
        bool IsSelectEmployee;
        Guid oldValue = Guid.Empty;
        public override Guid SelectedValue
        {
            get
            {
                if (IsSelectEmployee)//выбираем сотрудника
                    return (Guid)table_EmployeesDataGridView["EmployeeID", table_EmployeesDataGridView.CurrentRow.Index].Value;
                else                 //выбираем организацию
                    return (Guid)table_CompaniesDataGridView["CompanyID", table_CompaniesDataGridView.CurrentRow.Index].Value;
            }
        }
        public override void BS_Fill()
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_Employees". При необходимости она может быть перемещена или удалена.
            this.table_EmployeesTableAdapter.Fill(this.cA_DB_DataSet.Table_Employees);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_Companies". При необходимости она может быть перемещена или удалена.
            this.table_CompaniesTableAdapter.Fill(this.cA_DB_DataSet.Table_Companies);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_CompanyTypes". При необходимости она может быть перемещена или удалена.
            this.table_CompanyTypesTableAdapter.Fill(this.cA_DB_DataSet.Table_CompanyTypes);
        }
        public override void BS_Update()
        {
            if (!IsLoaded) return;
            this.table_CompanyTypesBindingSource.EndEdit();
            this.table_CompanyTypesTableAdapter.Adapter.Update(cA_DB_DataSet);
            this.table_CompaniesBindingSource.EndEdit();
            this.table_CompaniesTableAdapter.Adapter.Update(cA_DB_DataSet);
            this.table_EmployeesBindingSource.EndEdit();
            this.table_EmployeesTableAdapter.Adapter.Update(cA_DB_DataSet);
        }
        private void table_CompanyTypesDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            BS_dgvSpecialEdit(sender as DataGridView, e);
        }
        private void table_CompaniesDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            BS_dgvSpecialEdit(sender as DataGridView, e);
        }
        private void table_EmployeesDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            BS_dgvSpecialEdit(sender as DataGridView, e);
        }
        private void table_CompanyTypesDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            BS_Save();
        }
        private void table_CompaniesDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            BS_Save();
        }
        private void table_EmployeesDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            BS_Save();
        }
        private void toolStripButtonAddCompanyType_Click(object sender, EventArgs e)
        {
            IsLoaded = false;
            table_CompanyTypesBindingSource.AddNew();
            DataGridViewRow newrow = table_CompanyTypesDataGridView.Rows[table_CompanyTypesDataGridView.Rows.Count - 1];
            newrow.Cells["CompanyTypeID"].Value = Guid.NewGuid();
            newrow.Cells["CompanyTypeState"].Value = "normal";
            newrow.Cells["CompanyTypeName"].Value = "пусто";
            IsLoaded = true;
            BS_Update();
            newrow.Cells["CompanyTypeName"].Selected = true;
        }
        private void toolStripButtonAddCompany_Click(object sender, EventArgs e)
        {
            IsLoaded = false;
            table_CompaniesBindingSource.AddNew();
            DataGridViewRow newrow = table_CompaniesDataGridView.Rows[table_CompaniesDataGridView.Rows.Count - 1];
            newrow.Cells["CompanyID"].Value = Guid.NewGuid();
            newrow.Cells["CompanyState"].Value = "normal";
            DataTable dt = table_CompaniesTableAdapter.GetData();
            int dtmax = dt.AsEnumerable()
                .Select(r => r.Field<int>("Num"))
                .Distinct()
                .Max();
            newrow.Cells["CompanyNum"].Value = dtmax + 1;
            newrow.Cells["CompanyName"].Value = "пусто";
            newrow.Cells["CompanyDate"].Value = DateTime.Today;
            IsLoaded = true;
            BS_Update();
            newrow.Cells["CompanyName"].Selected = true;
        }
        private void toolStripButtonAddEmployee_Click(object sender, EventArgs e)
        {
            IsLoaded = false;
            table_EmployeesBindingSource.AddNew();
            DataGridViewRow newrow = table_EmployeesDataGridView.Rows[table_EmployeesDataGridView.Rows.Count - 1];
            newrow.Cells["EmployeeID"].Value = Guid.NewGuid();
            newrow.Cells["EmployeeState"].Value = "normal";
            IsLoaded = true;
            BS_Update();
            newrow.Cells["EmployeeLastName"].Selected = true;
        }
        private void toolStripButtonDeleteType_Click(object sender, EventArgs e)
        {
            //BS_DeleteRow(table_CompanyTypesDataGridView); ссылки в базе!!!
        }
        private void toolStripButtonDelete_Click(object sender, EventArgs e)
        {
            //BS_DeleteRow(table_CompaniesDataGridView); ссылки в базе!!!
        }
        private void toolStripButtonDeleteEmployee_Click(object sender, EventArgs e)
        {
            //BS_DeleteRow(table_EmployeesDataGridView); ссылки в базе!!!
        }
        /*public Form_Companies()
        { 
            InitializeComponent(); 
        }*/
        public Form_Companies(bool _IsSelectionMode = false, bool _IsSelectEmployee = false, string _oldValue = "")
        {
            InitializeComponent();
            IsSelectionMode = _IsSelectionMode;
            IsSelectEmployee = _IsSelectEmployee;
            oldValue = (_oldValue != "") ? new Guid(_oldValue) : Guid.Empty;
        }
        /*private void table_CompanyTypesBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.table_CompanyTypesBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.cA_DB_DataSet);
        }*/
        private void Form_Companies_Load(object sender, EventArgs e)
        {
            BS_Refresh(table_CompanyTypesDataGridView, table_CompaniesDataGridView, table_EmployeesDataGridView);
            if (IsSelectionMode && oldValue != Guid.Empty)
            {
                if (IsSelectEmployee)
                {    //выбираем сотрудника
                    DataTable dt = table_EmployeesTableAdapter.GetData();
                    Guid oldCompanyID = dt.AsEnumerable()
                        .Where(r => r.Field<Guid>("id").Equals(oldValue))
                        .Select(r => r.Field<Guid>("CompanyID"))
                        .First();
                    DataTable dt2 = table_CompaniesTableAdapter.GetData();
                    Guid oldCompanyTypeID = dt2.AsEnumerable()
                        .Where(r => r.Field<Guid>("id").Equals(oldCompanyID))
                        .Select(r => r.Field<Guid>("CompanyTypeID"))
                        .First();
                    table_CompanyTypesBindingSource.Position = table_CompanyTypesBindingSource.Find("id", oldCompanyTypeID);
                    BS_dgv_SetPosition(table_CompaniesDataGridView, oldCompanyID);
                    BS_dgv_SetPosition(table_EmployeesDataGridView, oldValue);
                }
                else //выбираем организацию
                {
                    DataTable dt = table_CompaniesTableAdapter.GetData();
                    Guid oldCompanyTypeID = dt.AsEnumerable()
                        .Where(r => r.Field<Guid>("id").Equals(oldValue))
                        .Select(r => r.Field<Guid>("CompanyTypeID"))
                        .First();
                    table_CompanyTypesBindingSource.Position = table_CompanyTypesBindingSource.Find("id", oldCompanyTypeID);
                    BS_dgv_SetPosition(table_CompaniesDataGridView, oldValue);
                }
            }
            IsLoaded = true;
        }
        private void button_Refresh_Click(object sender, EventArgs e)
        {
            BS_Refresh(table_CompanyTypesDataGridView, table_CompaniesDataGridView, table_EmployeesDataGridView);
        }
        private void table_CompanyTypesDataGridView_CurrentCellChanged(object sender, EventArgs e)
        {
            BS_TuneUpDataGridViews(sender);
        }
        private void table_CompaniesDataGridView_CurrentCellChanged(object sender, EventArgs e)
        {
            BS_TuneUpDataGridViews(sender);
        }
        private void toolStripButton_ToExcel_Click(object sender, EventArgs e)
        {
            BS_ToExcel(table_CompanyTypesDataGridView);
        }
        private void toolStripButton_ToExcel2_Click(object sender, EventArgs e)
        {
            BS_ToExcel(table_CompaniesDataGridView);
        }
        private void toolStripButton_ToExcel3_Click(object sender, EventArgs e)
        {
            BS_ToExcel(table_EmployeesDataGridView);
        }
    }
}