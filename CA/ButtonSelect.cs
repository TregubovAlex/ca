﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CA
{
    public partial class ButtonSelect : UserControl
    {
        Form_BaseStyle formToSelectItem;
        DataGridView dgv = null;
        ComboBox cb = null;
        int ColumnIndex;
        int RowIndex;
        public ButtonSelect()
        { InitializeComponent(); }
        public void Configure(Form_BaseStyle _formToSelectItem, object _sender, int _ColumnIndex = 0, int _RowIndex = 0)
        {
            formToSelectItem = _formToSelectItem;
            (_sender as Control).Parent.Controls.Add(this);
            if (_sender is DataGridView)
            {
                dgv = (_sender as DataGridView);
                cb = null;
                ColumnIndex = _ColumnIndex;
                RowIndex = _RowIndex;
                int location_X_in_DGV = 0;
                foreach (DataGridViewColumn column in dgv.Columns)
                    if (column.Visible == true && column.Index <= ColumnIndex)
                        location_X_in_DGV += column.Width;
                this.Height = dgv.RowTemplate.Height;
                this.Location = new Point(dgv.Location.X +
                                          dgv.RowHeadersWidth +
                                          location_X_in_DGV - this.Width,
                                          dgv.Location.Y +
                                          dgv.ColumnHeadersHeight +
                                          (RowIndex - dgv.FirstDisplayedScrollingRowIndex) * dgv.RowTemplate.Height);
            }
            else if (_sender is ComboBox)
            {
                dgv = null;
                cb = (_sender as ComboBox);
                this.Height = cb.Height;
                this.Location = new Point(cb.Location.X +
                                          cb.Width - this.Width,
                                          cb.Location.Y);
            }
            this.BringToFront();
            this.Visible = true;
            this.Focus();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            formToSelectItem.ShowDialog();
            if (dgv != null)
                dgv[ColumnIndex, RowIndex].Value = formToSelectItem.SelectedValue;
            else if (cb != null)
                cb.SelectedValue = formToSelectItem.SelectedValue;
            this.Visible = false;
        }
        private void ButtonSelect_Leave(object sender, EventArgs e)
        {
            try
            {
                this.Parent.Controls.Remove(this);
            }
            catch { }
        }
    }
}