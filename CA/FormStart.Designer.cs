﻿namespace CA
{
    partial class FormStart
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_MoneyMoves = new System.Windows.Forms.Button();
            this.button_Companies = new System.Windows.Forms.Button();
            this.button_Materials = new System.Windows.Forms.Button();
            this.button_Projects = new System.Windows.Forms.Button();
            this.button_Exit = new System.Windows.Forms.Button();
            this.button_Users = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_MoneyMoves
            // 
            this.button_MoneyMoves.Location = new System.Drawing.Point(14, 80);
            this.button_MoneyMoves.Name = "button_MoneyMoves";
            this.button_MoneyMoves.Size = new System.Drawing.Size(147, 62);
            this.button_MoneyMoves.TabIndex = 1;
            this.button_MoneyMoves.Text = "Расходы и поступления";
            this.button_MoneyMoves.UseVisualStyleBackColor = true;
            this.button_MoneyMoves.Click += new System.EventHandler(this.button_MoneyMoves_Click);
            // 
            // button_Companies
            // 
            this.button_Companies.Location = new System.Drawing.Point(14, 216);
            this.button_Companies.Name = "button_Companies";
            this.button_Companies.Size = new System.Drawing.Size(147, 62);
            this.button_Companies.TabIndex = 3;
            this.button_Companies.Text = "Организации \r\nи \r\nсотрудники";
            this.button_Companies.UseVisualStyleBackColor = true;
            this.button_Companies.Click += new System.EventHandler(this.button_Companies_Click);
            // 
            // button_Materials
            // 
            this.button_Materials.Location = new System.Drawing.Point(14, 148);
            this.button_Materials.Name = "button_Materials";
            this.button_Materials.Size = new System.Drawing.Size(147, 62);
            this.button_Materials.TabIndex = 2;
            this.button_Materials.Text = "Справочник Материалов";
            this.button_Materials.UseVisualStyleBackColor = true;
            this.button_Materials.Click += new System.EventHandler(this.button_Materials_Click);
            // 
            // button_Projects
            // 
            this.button_Projects.Location = new System.Drawing.Point(12, 12);
            this.button_Projects.Name = "button_Projects";
            this.button_Projects.Size = new System.Drawing.Size(147, 62);
            this.button_Projects.TabIndex = 0;
            this.button_Projects.Text = "Заказы";
            this.button_Projects.UseVisualStyleBackColor = true;
            this.button_Projects.Click += new System.EventHandler(this.button_Projects_Click);
            // 
            // button_Exit
            // 
            this.button_Exit.Location = new System.Drawing.Point(12, 438);
            this.button_Exit.Name = "button_Exit";
            this.button_Exit.Size = new System.Drawing.Size(147, 62);
            this.button_Exit.TabIndex = 99;
            this.button_Exit.Text = "Выход";
            this.button_Exit.UseVisualStyleBackColor = true;
            this.button_Exit.Click += new System.EventHandler(this.button_Exit_Click);
            // 
            // button_Users
            // 
            this.button_Users.Location = new System.Drawing.Point(12, 284);
            this.button_Users.Name = "button_Users";
            this.button_Users.Size = new System.Drawing.Size(147, 62);
            this.button_Users.TabIndex = 4;
            this.button_Users.Text = "Пользователи и права";
            this.button_Users.UseVisualStyleBackColor = true;
            this.button_Users.Click += new System.EventHandler(this.button_Users_Click);
            // 
            // FormStart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(173, 512);
            this.ControlBox = false;
            this.Controls.Add(this.button_Companies);
            this.Controls.Add(this.button_Exit);
            this.Controls.Add(this.button_Projects);
            this.Controls.Add(this.button_MoneyMoves);
            this.Controls.Add(this.button_Users);
            this.Controls.Add(this.button_Materials);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormStart";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "СА";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_MoneyMoves;
        private System.Windows.Forms.Button button_Companies;
        private System.Windows.Forms.Button button_Materials;
        private System.Windows.Forms.Button button_Projects;
        private System.Windows.Forms.Button button_Exit;
        private System.Windows.Forms.Button button_Users;
    }
}

