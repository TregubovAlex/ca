﻿namespace CA
{
    partial class Form_BaseStyle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.BS_toolTips = new System.Windows.Forms.ToolTip(this.components);
            this.BS_statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel_Separator1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_UserName = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_Separator2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_FormMode = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_Separator99 = new System.Windows.Forms.ToolStripStatusLabel();
            this.BS_statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // BS_toolTips
            // 
            this.BS_toolTips.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // BS_statusStrip
            // 
            this.BS_statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel_Separator1,
            this.toolStripStatusLabel_UserName,
            this.toolStripStatusLabel_Separator2,
            this.toolStripStatusLabel_FormMode,
            this.toolStripStatusLabel_Separator99});
            this.BS_statusStrip.Location = new System.Drawing.Point(0, 479);
            this.BS_statusStrip.Name = "BS_statusStrip";
            this.BS_statusStrip.Size = new System.Drawing.Size(569, 22);
            this.BS_statusStrip.TabIndex = 0;
            this.BS_statusStrip.Text = "BS_statusStrip";
            // 
            // toolStripStatusLabel_Separator1
            // 
            this.toolStripStatusLabel_Separator1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabel_Separator1.Name = "toolStripStatusLabel_Separator1";
            this.toolStripStatusLabel_Separator1.Size = new System.Drawing.Size(19, 17);
            this.toolStripStatusLabel_Separator1.Text = "│";
            // 
            // toolStripStatusLabel_UserName
            // 
            this.toolStripStatusLabel_UserName.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabel_UserName.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripStatusLabel_UserName.Name = "toolStripStatusLabel_UserName";
            this.toolStripStatusLabel_UserName.Size = new System.Drawing.Size(234, 17);
            this.toolStripStatusLabel_UserName.Text = "Пользователь:  Иванов Иван Иванович";
            // 
            // toolStripStatusLabel_Separator2
            // 
            this.toolStripStatusLabel_Separator2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabel_Separator2.Name = "toolStripStatusLabel_Separator2";
            this.toolStripStatusLabel_Separator2.Size = new System.Drawing.Size(19, 17);
            this.toolStripStatusLabel_Separator2.Text = "│";
            // 
            // toolStripStatusLabel_FormMode
            // 
            this.toolStripStatusLabel_FormMode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabel_FormMode.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripStatusLabel_FormMode.ForeColor = System.Drawing.SystemColors.ControlText;
            this.toolStripStatusLabel_FormMode.Name = "toolStripStatusLabel_FormMode";
            this.toolStripStatusLabel_FormMode.Size = new System.Drawing.Size(198, 17);
            this.toolStripStatusLabel_FormMode.Text = "Режим:  текущий режим формы";
            // 
            // toolStripStatusLabel_Separator99
            // 
            this.toolStripStatusLabel_Separator99.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabel_Separator99.Name = "toolStripStatusLabel_Separator99";
            this.toolStripStatusLabel_Separator99.Size = new System.Drawing.Size(19, 17);
            this.toolStripStatusLabel_Separator99.Text = "│";
            // 
            // Form_BaseStyle
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(569, 501);
            this.Controls.Add(this.BS_statusStrip);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "Form_BaseStyle";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form_BaseStyle";
            this.Load += new System.EventHandler(this.Form_BaseStyle_Load);
            this.BS_statusStrip.ResumeLayout(false);
            this.BS_statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ToolTip BS_toolTips;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_UserName;
        public System.Windows.Forms.StatusStrip BS_statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_FormMode;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_Separator99;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_Separator1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_Separator2;



    }
}