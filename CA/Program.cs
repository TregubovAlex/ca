﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CA
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            MessageBox.Show(
            "Сделать так:\nВ режиме IsSelectionMode записи выделяются целыми рядами, выбор двойным щелчком, и нередактируются (но не все???)" + "\n\n" +
            "Изменить статусы элементов управления в режиме IsSelectionMode" + "\n\n"
            );

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormLogin());
            //Application.Run(new FormStart());

            //Form_Calculator fc = new Form_Calculator(33);
            //fc.ShowDialog();
            //MessageBox.Show(fc.Value.ToString());
        }
    }
}