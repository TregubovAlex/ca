﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;


namespace CA
{
    public partial class Form_BaseStyle : Form
    {
        bool localIsLoaded;
        public virtual bool IsLoaded
        {
            get { return localIsLoaded; }
            set { localIsLoaded = value; }
        }
        bool localIsSelectionMode;
        public virtual bool IsSelectionMode
        {
            get { return localIsSelectionMode; }
            set { localIsSelectionMode = value; }
        }
        public virtual Guid SelectedValue
        {
            get { return Guid.Empty; }
        }
        public virtual void BS_Fill() { }
        public virtual void BS_Update() { }
        public void BS_Refresh(DataGridView _dgvFirst, DataGridView _dgvSecond = null, DataGridView _dgvThird = null)
        {
            Guid _currFirstID = (_dgvFirst.CurrentRow != null) ? (Guid)_dgvFirst[0, _dgvFirst.CurrentRow.Index].Value : Guid.Empty;

            Guid _currSecondID = Guid.Empty;
            if (_dgvSecond != null)
                _currSecondID = (_dgvSecond.CurrentRow != null) ? (Guid)_dgvSecond[0, _dgvSecond.CurrentRow.Index].Value : Guid.Empty;

            Guid _currThird = Guid.Empty;
            if (_dgvThird != null)
                _currThird = (_dgvThird.CurrentRow != null) ? (Guid)_dgvThird[0, _dgvThird.CurrentRow.Index].Value : Guid.Empty;

            BS_Fill();

            BS_dgv_SetPosition(_dgvFirst, _currFirstID);
            if (_dgvSecond != null)
                BS_dgv_SetPosition(_dgvSecond, _currSecondID);
            if (_dgvThird != null)
                BS_dgv_SetPosition(_dgvThird, _currThird);
            BS_TuneUpDataGridViews();
        }
        public void BS_dgv_SetPosition(DataGridView _dgv, Guid _guid)
        {
            if (_guid != Guid.Empty)
            {
                DataGridViewRow row = _dgv.Rows
                    .Cast<DataGridViewRow>()
                    .Where(r => r.IsNewRow.Equals(false))
                    .Where(r => r.Cells[0].Value.ToString().Equals(_guid.ToString()))
                    .First();
                BindingSource bs = _dgv.DataSource as BindingSource;
                bs.Position = row.Index;
            }
        }
        public void BS_TuneUpDataGridViews(object sender)
        {
            if ((sender as DataGridView).Focused == true)
                BS_TuneUpDataGridViews();
        }
        public void BS_TuneUpDataGridViews()
        {
            TuneUpDataGridViews(Controls);
            Refresh();
        }
        void TuneUpDataGridViews(Control.ControlCollection _collection)
        {
            foreach (Control ctrl in _collection)
            {
                if (ctrl is SplitContainer ||
                    ctrl is SplitterPanel ||
                    ctrl is TabControl ||
                    ctrl is TabPage)
                    TuneUpDataGridViews(ctrl.Controls);
                else if (ctrl is DataGridView)
                {
                    DataGridView dgv = ctrl as DataGridView;
                    dgv.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
                    foreach (DataGridViewColumn col in dgv.Columns)
                    {
                        int size = col.Width;
                        col.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                        col.Width = size;
                    }
                }
            }
        }
        public void BS_Save()
        {
            //добавить проверку корректности внесенных данных
            if (!localIsLoaded) return;
            BS_Update();
            BS_TuneUpDataGridViews();
        }
        public void BS_DeleteSingleRow(DataGridView dgv)
        {
            dgv.Focus();//необходимо для пропадания на форме компонента ButtonSelect
            DataGridViewRow r = dgv.CurrentRow;
            if (r != null) dgv.Rows.Remove(r);
            BS_Update();
        }
        public void BS_dgvSpecialEdit(DataGridView dgv, DataGridViewCellCancelEventArgs e)
        {
            if (dgv.Columns[e.ColumnIndex].ValueType.Name.Equals("DateTime"))
            {
                FormEdit_Calendar form = new FormEdit_Calendar((DateTime)dgv[e.ColumnIndex, e.RowIndex].Value);
                form.ShowDialog();
                dgv[e.ColumnIndex, e.RowIndex].Value = form.Value;
                EndEdit();
            }
            else if (dgv.Columns[e.ColumnIndex].ValueType.Name.Equals("Decimal"))
            {
                FormEdit_Calculator form = new FormEdit_Calculator(dgv[e.ColumnIndex, e.RowIndex].Value.ToString());
                form.ShowDialog();
                dgv[e.ColumnIndex, e.RowIndex].Value = form.Value;
                EndEdit();
            }
        }
        void EndEdit()
        {
            SendKeys.Send("{TAB}");
            SendKeys.Send("+{TAB}");
            /*DataGridViewEditMode oldMode = dgv.EditMode;
            dgv.EditMode = DataGridViewEditMode.EditProgrammatically;
            dgv.EndEdit();
            dgv.EditMode = oldMode;*/
        }
        public Form_BaseStyle()
        { InitializeComponent(); }
        private void Form_BaseStyle_Load(object sender, EventArgs e)
        {
            toolStripStatusLabel_UserName.Text = "Пользователь:  " + User.FullName;
            if (IsSelectionMode)
            {
                toolStripStatusLabel_FormMode.Text = "Режим:  Выбор (только чтение)";
                toolStripStatusLabel_FormMode.ForeColor = Color.DimGray;
            }
            else
            {
                toolStripStatusLabel_FormMode.Text = "Режим:  Редактирование";
                toolStripStatusLabel_FormMode.ForeColor = Color.IndianRed;
            }
            if (IsSelectionMode) BS_SwitchDataGridViewsToSelectionMode();
        }
        public void BS_SwitchDataGridViewsToSelectionMode()
        {
            SwitchDataGridViewsToSelectionMode(Controls);
            Refresh();
        }
        void SwitchDataGridViewsToSelectionMode(Control.ControlCollection _collection)
        {
            foreach (Control ctrl in _collection)
            {
                if (ctrl is SplitContainer ||
                    ctrl is SplitterPanel ||
                    ctrl is TabControl ||
                    ctrl is TabPage)
                    SwitchDataGridViewsToSelectionMode(ctrl.Controls);
                else if (ctrl is DataGridView)
                {
                    DataGridView dgv = ctrl as DataGridView;
                    dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                    dgv.ReadOnly = true;
                    dgv.CellDoubleClick += dgv_CellDoubleClick;
                }
            }
        }
        void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (IsSelectionMode) this.Close();
        }
        public void BS_ToExcel(DataGridView dgv)
        {
            Excel.Application exApp = new Excel.Application();
            Excel.Workbook exWorkBook = exApp.Workbooks.Add();
            Excel.Worksheet exSheet = exWorkBook.Sheets[1];
            int colIndex = 0;
            foreach (DataGridViewColumn column in dgv.Columns)
            {
                if (column.Visible == true && !column.CellType.Name.ToLower().Contains("button"))
                {
                    colIndex++;
                    exSheet.Cells[1, colIndex] = column.HeaderText;
                    int rowIndex = 1;
                    if (column.DefaultCellStyle.Format == "C2")
                        exSheet.Columns[colIndex].NumberFormat = "### ##0,00";
                    else if (column.DefaultCellStyle.Format == "N1")
                        exSheet.Columns[colIndex].NumberFormat = "### ##0,0";
                    foreach (DataGridViewRow row in dgv.Rows)
                    {
                        rowIndex++;
                        if (column.ValueType.Name.Equals("Guid"))
                            exSheet.Cells[rowIndex, colIndex] = dgv[column.Index, row.Index].FormattedValue;
                        else if (column.ValueType.Name.Equals("Boolean"))
                            if (Convert.ToBoolean(dgv[column.Index, row.Index].Value) == true)
                                exSheet.Cells[rowIndex, colIndex] = "Да";
                            else
                                exSheet.Cells[rowIndex, colIndex] = "Нет";
                        else
                            exSheet.Cells[rowIndex, colIndex] = dgv[column.Index, row.Index].Value;
                    }
                }
            }
            exApp.ActiveWindow.FreezePanes = false;
            exSheet.get_Range("A2").Select();
            exApp.ActiveWindow.FreezePanes = true;
            exSheet.Rows[1].Cells.HorizontalAlignment = Excel.Constants.xlCenter;
            exSheet.Rows[1].Font.Bold = true;
            exSheet.Rows[1].AutoFilter();
            exSheet.Columns.AutoFit();
            exApp.Visible = true;
            //exApp = null;
        }
    }
}