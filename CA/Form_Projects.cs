﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CA
{
    public partial class Form_Projects : Form_BaseStyle
    {
        Guid oldValue = Guid.Empty;
        public override Guid SelectedValue
        {
            get { return (Guid)table_ProjectsDataGridView["ProjectID", table_ProjectsDataGridView.CurrentRow.Index].Value; }
        }
        public override void BS_Fill()
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_Users". При необходимости она может быть перемещена или удалена.
            this.table_UsersTableAdapter.Fill(this.cA_DB_DataSet.Table_Users);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_TEOMaterials". При необходимости она может быть перемещена или удалена.
            this.table_TEOMaterialsTableAdapter.Fill(this.cA_DB_DataSet.Table_TEOMaterials);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_SubProjects". При необходимости она может быть перемещена или удалена.
            this.table_SubProjectsTableAdapter.Fill(this.cA_DB_DataSet.Table_SubProjects);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_MoneyFlow". При необходимости она может быть перемещена или удалена.
            this.table_MoneyFlowTableAdapter.Fill(this.cA_DB_DataSet.Table_MoneyFlow);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_ProductionStages". При необходимости она может быть перемещена или удалена.
            this.table_ProductionStagesTableAdapter.Fill(this.cA_DB_DataSet.Table_ProductionStages);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_Companies". При необходимости она может быть перемещена или удалена.
            this.table_CompaniesTableAdapter.Fill(this.cA_DB_DataSet.Table_Companies);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_ProjectStatuses". При необходимости она может быть перемещена или удалена.
            this.table_ProjectStatusesTableAdapter.Fill(this.cA_DB_DataSet.Table_ProjectStatuses);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_Projects". При необходимости она может быть перемещена или удалена.
            this.table_ProjectsTableAdapter.Fill(this.cA_DB_DataSet.Table_Projects);
        }
        public override void BS_Update()
        {
            if (!IsLoaded) return;
            this.table_TEOMaterialsBindingSource.EndEdit();
            this.table_TEOMaterialsTableAdapter.Adapter.Update(cA_DB_DataSet);
            this.table_SubProjectsBindingSource.EndEdit();
            this.table_SubProjectsTableAdapter.Adapter.Update(cA_DB_DataSet);
            this.table_MoneyFlowBindingSource.EndEdit();
            this.table_MoneyFlowTableAdapter.Adapter.Update(cA_DB_DataSet);
            this.table_ProductionStagesBindingSource.EndEdit();
            this.table_ProductionStagesTableAdapter.Adapter.Update(cA_DB_DataSet);
            this.table_ProjectsBindingSource.EndEdit();
            this.table_ProjectsTableAdapter.Adapter.Update(cA_DB_DataSet);
        }
        private void table_ProjectsDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            BS_dgvSpecialEdit(sender as DataGridView, e);
        }
        private void table_ProjectsDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            BS_Save();
        }
        /*public Form_Projects()
        { 
            InitializeComponent(); 
        }*/
        public Form_Projects(bool _IsSelectionMode = false, string _oldValue = "")
        {
            InitializeComponent();
            IsSelectionMode = _IsSelectionMode;
            oldValue = (_oldValue != "") ? new Guid(_oldValue) : Guid.Empty;
        }
        /*private void table_ProjectsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.table_ProjectsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.cA_DB_DataSet);
        }*/
        private void Form_Projects_Load(object sender, EventArgs e)
        {
            BS_Refresh(table_ProjectsDataGridView);
            if (IsSelectionMode)
            {
                if (oldValue != Guid.Empty)
                {
                    table_ProjectsBindingSource.Position = table_ProjectsBindingSource.Find("id", oldValue);
                    foreach (CheckBox control in groupBox_ShowByTypes.Controls)
                        control.Checked = false;
                    IsLoaded = true;
                }
            }
            else
            {
                IsLoaded = true;
                UpdateFilter();
            }
        }
        private void toolStripButtonAdd_Click(object sender, EventArgs e)
        {
            IsLoaded = false;
            table_ProjectsBindingSource.AddNew();
            DataGridViewRow newrow = table_ProjectsDataGridView.Rows[table_ProjectsDataGridView.Rows.Count - 1];
            newrow.Cells["ProjectID"].Value = Guid.NewGuid();
            newrow.Cells["ProjectState"].Value = "normal";
            DataTable dt = table_ProjectsTableAdapter.GetData();
            int dtmax = dt.AsEnumerable()
                .Select(r => r.Field<int>("Num"))
                .Distinct()
                .Max();
            newrow.Cells["ProjectNum"].Value = dtmax + 1;
            newrow.Cells["ProjectDate"].Value = DateTime.Today;
            newrow.Cells["ProjectName"].Value = "пусто";
            newrow.Cells["ProjectMargin"].Value = 40;
            newrow.Cells["ProjectCompanyID"].Value = "f7422c37-6eda-43ea-874e-8cbfb37a161a";//[ пусто ]
            newrow.Cells["ProjectStatusID"].Value = "e610c5dc-2016-4ed4-b5c2-c9977bf67256";//[ пусто ]
            IsLoaded = true;
            BS_Update();
            newrow.Cells["ProjectName"].Selected = true;
        }
        private void toolStripButtonOpen_Click(object sender, EventArgs e)
        {
            OpenSelectedProject();
        }
        private void OpenSelectedProject()
        {
            this.Hide();
            Form_ProjectCurrent form_ProjectCurrent = new Form_ProjectCurrent((Guid)table_ProjectsDataGridView["ProjectID", table_ProjectsDataGridView.CurrentRow.Index].Value);
            form_ProjectCurrent.ShowDialog();
            BS_Refresh(table_ProjectsDataGridView);
            this.Show();
        }
        private void toolStripButtonDelete_Click(object sender, EventArgs e)
        {
            DataGridViewRow r = table_ProjectsDataGridView.CurrentRow;
            if (MessageBox.Show("Заказ \"" + r.Cells["ProjectName"].Value + "\" будет удален безвозвратно, включая все его списки материалов!",
                "Внимание - удаление заказа!", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                foreach (object item in table_TEOMaterialsBindingSource.List)
                    table_TEOMaterialsBindingSource.Remove(item);
                BS_Update();
                foreach (object item in table_SubProjectsBindingSource.List)
                    table_SubProjectsBindingSource.Remove(item);
                BS_Update();
                foreach (object item in table_ProductionStagesBindingSource.List)
                    table_ProductionStagesBindingSource.Remove(item);
                BS_Update();
                if (table_MoneyFlowBindingSource.List.Count > 0)
                {
                    if (MessageBox.Show("Удалить все записи из таблицы \"Расходы и поступления\"\nсвязанные с заказом \"" + r.Cells["ProjectName"].Value + "\"?\n\nДа - удалить безвозвратно.\nНет - оставить без привязки к заказам.",
                        "Внимание - удаление заказа!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        foreach (object item in table_MoneyFlowBindingSource.List)
                            table_MoneyFlowBindingSource.Remove(item);
                    else //нет
                        foreach (DataRowView drv in table_MoneyFlowBindingSource.List)
                            drv.Row.SetField("ProjectID", "c9b48abc-6828-4a04-b2ce-04592d8e1395");//[ пусто ]
                    BS_Update();
                }
                BS_DeleteSingleRow(table_ProjectsDataGridView);
                MessageBox.Show("Заказ удален.");
            }
        }
        private void checkBox_1_CheckedChanged(object sender, EventArgs e)
        { UpdateFilter(); }
        private void checkBox_2_CheckedChanged(object sender, EventArgs e)
        { UpdateFilter(); }
        private void checkBox_3_CheckedChanged(object sender, EventArgs e)
        { UpdateFilter(); }
        private void checkBox_4_CheckedChanged(object sender, EventArgs e)
        { UpdateFilter(); }
        private void checkBox_5_CheckedChanged(object sender, EventArgs e)
        { UpdateFilter(); }
        private void UpdateFilter()
        {
            if (!IsLoaded) return;
            string filter = "";
            DataTable dt = table_ProjectStatusesTableAdapter.GetData();
            foreach (CheckBox control in groupBox_ShowByTypes.Controls)
                if (control.Checked)
                {
                    if (filter == "")
                        filter = " AND (";
                    Guid currStateID = dt.AsEnumerable()
                        .Where(state => (state["Name"].Equals(control.Text)))
                        .Select(r => r.Field<Guid>("id"))
                        .First();
                    if (filter.Contains("="))
                        filter += " OR ";
                    filter += "ProjectStatusID='" + currStateID.ToString() + "'";
                }
            if (filter != "")
                filter += ")";
            this.table_ProjectsBindingSource.Filter = "Name <> '[ пусто ]'" + filter;
        }
        private void toolStripButton_ToExcel_Click(object sender, EventArgs e)
        {
            BS_ToExcel(table_ProjectsDataGridView);
        }
        private void table_ProjectsDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            OpenSelectedProject();
        }
        private void table_ProjectsDataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                OpenSelectedProject();
        }
    }
}