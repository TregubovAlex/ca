﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CA
{
    public partial class Form_Materials : Form_BaseStyle
    {
        Guid oldValue = Guid.Empty;
        public override Guid SelectedValue
        {
            get { return (Guid)table_MaterialsDataGridView["MaterialID", table_MaterialsDataGridView.CurrentRow.Index].Value; }
        }
        public override void BS_Fill()
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_MeasureUnits". При необходимости она может быть перемещена или удалена.
            this.table_MeasureUnitsTableAdapter.Fill(this.cA_DB_DataSet.Table_MeasureUnits);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_Sections". При необходимости она может быть перемещена или удалена.
            this.table_SectionsTableAdapter.Fill(this.cA_DB_DataSet.Table_Sections);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_Materials". При необходимости она может быть перемещена или удалена.
            this.table_MaterialsTableAdapter.Fill(this.cA_DB_DataSet.Table_Materials);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_MaterialGroups". При необходимости она может быть перемещена или удалена.
            this.table_MaterialGroupsTableAdapter.Fill(this.cA_DB_DataSet.Table_MaterialGroups);
        }
        public override void BS_Update()
        {
            if (!IsLoaded) return;
            this.table_MaterialGroupsBindingSource.EndEdit();
            this.table_MaterialGroupsTableAdapter.Adapter.Update(cA_DB_DataSet);
            this.table_MaterialsBindingSource.EndEdit();
            this.table_MaterialsTableAdapter.Adapter.Update(cA_DB_DataSet);
        }
        private void table_MaterialGroupsDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            BS_dgvSpecialEdit(sender as DataGridView, e);
        }
        private void table_MaterialsDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            BS_dgvSpecialEdit(sender as DataGridView, e);
        }
        private void table_MaterialsDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            BS_Save();
        }
        private void table_MaterialGroupsDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            BS_Save();
        }
        private void toolStripButtonDelete_Click(object sender, EventArgs e)
        {
            //BS_DeleteRow(table_MaterialsDataGridView); ссылки в базе !!!
        }
        private void toolStripButtonDeleteGroup_Click(object sender, EventArgs e)
        {
            //BS_DeleteRow(table_MaterialGroupsDataGridView); Придумать как удалять/перемещать материалы удаляемой группы
        }
        /*public Form_Materials()
        {
            InitializeComponent();
        }*/
        public Form_Materials(bool _IsSelectionMode = false, string _oldValue = "")
        {
            InitializeComponent();
            IsSelectionMode = _IsSelectionMode;
            oldValue = (_oldValue != "") ? new Guid(_oldValue) : Guid.Empty;
        }
        /*private void table_MaterialGroupsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.table_MaterialGroupsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.cA_DB_DataSet);
        }*/
        private void Form_Materials_Load(object sender, EventArgs e)
        {
            BS_Refresh(table_MaterialGroupsDataGridView, table_MaterialsDataGridView);
            button_MeasureUnits.Visible = !IsSelectionMode;
            if (IsSelectionMode && oldValue != Guid.Empty)
            {
                DataTable dt = table_MaterialsTableAdapter.GetData();
                Guid oldGroupID = dt.AsEnumerable()
                    .Where(r => r.Field<Guid>("id").Equals(oldValue))
                    .Select(r => r.Field<Guid>("GroupID"))
                    .First();
                table_MaterialGroupsBindingSource.Position = table_MaterialGroupsBindingSource.Find("id", oldGroupID);
                BS_dgv_SetPosition(table_MaterialsDataGridView, oldValue);
            }
            IsLoaded = true;
        }
        private void button_Refresh_Click(object sender, EventArgs e)
        {
            BS_Refresh(table_MaterialGroupsDataGridView, table_MaterialsDataGridView);
        }
        private void comboBoxSection_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxSection.SelectedValue != null)
                table_MaterialsBindingSource.Filter = "SectionID = '" + comboBoxSection.SelectedValue + "'";
            else
                table_MaterialsBindingSource.Filter = "";
        }
        private void button_MeasureUnits_Click(object sender, EventArgs e)
        {
            Form_MeasureUnits form_MeasureUnits = new Form_MeasureUnits();
            form_MeasureUnits.ShowDialog();
        }
        private void table_MaterialsDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            //MessageBox.Show(e.Exception.Message);
        }
        private void toolStripButtonAdd_Click(object sender, EventArgs e)
        {
            IsLoaded = false;
            table_MaterialsBindingSource.AddNew();
            DataGridViewRow newrow = table_MaterialsDataGridView.Rows[table_MaterialsDataGridView.Rows.Count - 1];
            newrow.Cells["MaterialID"].Value = Guid.NewGuid();
            newrow.Cells["MaterialState"].Value = "normal";
            newrow.Cells["IsWork"].Value = false;
            newrow.Cells["MaterialName"].Value = "имя";
            newrow.Cells["MaterialMeasureUnitID"].Value = "8da54887-aace-4bc6-b0c6-34a3fa037b39";
            newrow.Cells["MaterialSectionID"].Value = comboBoxSection.SelectedValue;
            IsLoaded = true;
            BS_Update();
            newrow.Cells["MaterialName"].Selected = true;
        }
        private void toolStripButtonAddGroup_Click(object sender, EventArgs e)
        {
            IsLoaded = false;
            table_MaterialGroupsBindingSource.AddNew();
            DataGridViewRow newrow = table_MaterialGroupsDataGridView.Rows[table_MaterialGroupsDataGridView.Rows.Count - 1];
            newrow.Cells["GroupID"].Value = Guid.NewGuid();
            newrow.Cells["GroupState"].Value = "normal";
            newrow.Cells["GroupName"].Value = "имя";
            IsLoaded = true;
            BS_Update();
            newrow.Cells["GroupName"].Selected = true;
        }
        private void toolStripButton_ToExcel1_Click(object sender, EventArgs e)
        {
            BS_ToExcel(table_MaterialGroupsDataGridView);
        }
        private void toolStripButton_ToExcel2_Click(object sender, EventArgs e)
        {
            BS_ToExcel(table_MaterialsDataGridView);
        }
    }
}