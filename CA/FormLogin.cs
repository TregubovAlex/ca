﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CA.Properties;

namespace CA
{
    public partial class FormLogin : Form
    {
        private DataTable dt;
        BindingSource bs;
        public FormLogin()
        { InitializeComponent(); }
        private void FormLogin_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_Employees". При необходимости она может быть перемещена или удалена.
            this.table_EmployeesTableAdapter.Fill(this.cA_DB_DataSet.Table_Employees);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_Users". При необходимости она может быть перемещена или удалена.
            this.table_UsersTableAdapter.Fill(this.cA_DB_DataSet.Table_Users);

            DataTable tableUsers = table_UsersTableAdapter.GetData();
            DataTable tableEmployees = table_EmployeesTableAdapter.GetData();
            dt = new DataTable();
            dt.Columns.Add("UserID", typeof(Guid));
            dt.Columns.Add("Password", typeof(string));
            dt.Columns.Add("FullName", typeof(string));
            var result = from dataRowsUsers in tableUsers.AsEnumerable()
                         join dataRowsEmployees in tableEmployees.AsEnumerable()
                         on dataRowsUsers.Field<Guid>("EmployeeID") equals dataRowsEmployees.Field<Guid>("id")
                         select dt.LoadDataRow(new object[]
                         {
                            dataRowsUsers.Field<Guid>("id"),
                            dataRowsUsers.Field<string>("Password"),
                            dataRowsEmployees.Field<string>("LastName") + " " +
                            dataRowsEmployees.Field<string>("FirstName") + " " +
                            dataRowsEmployees.Field<string>("MiddleName")
                         }, false);
            result.CopyToDataTable();
            bs = new BindingSource();
            bs.DataSource = dt;
            comboBox_User.DataSource = bs;
            comboBox_User.DisplayMember = "FullName";
            comboBox_User.ValueMember = "UserID";
            comboBox_User.SelectedValue = Settings.Default.LastUser;
        }
        private void textBox_Password_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                TryLogin();
        }
        private void button_Login_Click(object sender, EventArgs e)
        {
            TryLogin();
        }
        void TryLogin()
        {
            string currStoredUserPassword = dt.AsEnumerable()
                .Where(r => r.Field<Guid>("UserID").Equals(comboBox_User.SelectedValue))
                .Select(r => r.Field<string>("Password"))
                .Last();
            if (currStoredUserPassword.Equals(textBox_Password.Text))
            {
                notifyIcon.Icon = SystemIcons.Information;
                notifyIcon.BalloonTipIcon = ToolTipIcon.Info;
                notifyIcon.BalloonTipTitle = "Вход выполнен успешно";
                notifyIcon.BalloonTipText = "\nПользователь: " + comboBox_User.Text + ".";
                notifyIcon.ShowBalloonTip(3000);
                User.ID = (Guid)comboBox_User.SelectedValue;
                User.FullName = comboBox_User.Text;
                Settings.Default.LastUser = User.ID;
                Settings.Default.Save();
                this.Hide();
                FormStart formStart = new FormStart();
                formStart.ShowDialog();
            }
            else
            {
                notifyIcon.Icon = SystemIcons.Error;
                notifyIcon.BalloonTipIcon = ToolTipIcon.Error;
                notifyIcon.BalloonTipTitle = "Неверный пароль";
                notifyIcon.BalloonTipText = "\nВведёный пароль не совпадает с паролем пользователя " + comboBox_User.Text + ".";
                if (Console.CapsLock)
                    notifyIcon.BalloonTipText = "\nВНИМАНИЕ: ВКЛЮЧЕН CAPSLOCK !!!\n" + notifyIcon.BalloonTipText;
                notifyIcon.ShowBalloonTip(3000);
                textBox_Password.Focus();
                textBox_Password.SelectAll();
            }
        }
        private void button_ShowPassword_MouseDown(object sender, MouseEventArgs e)
        {
            textBox_Password.UseSystemPasswordChar = false;
        }
        private void button_ShowPassword_MouseUp(object sender, MouseEventArgs e)
        {
            textBox_Password.UseSystemPasswordChar = true;
            textBox_Password.Focus();
        }
    }
}