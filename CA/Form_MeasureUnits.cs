﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CA
{
    public partial class Form_MeasureUnits : Form_BaseStyle
    {
        public override void BS_Fill()
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "cA_DB_DataSet.Table_MeasureUnits". При необходимости она может быть перемещена или удалена.
            this.table_MeasureUnitsTableAdapter.Fill(this.cA_DB_DataSet.Table_MeasureUnits);
        }
        public override void BS_Update()
        {
            if (!IsLoaded) return;
            this.table_MeasureUnitsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.cA_DB_DataSet);
        }
        private void toolStripButtonAdd_Click(object sender, EventArgs e)
        {
            IsLoaded = false;
            table_MeasureUnitsBindingSource.AddNew();
            DataGridViewRow newrow = table_MeasureUnitsDataGridView.Rows[table_MeasureUnitsDataGridView.Rows.Count - 1];
            newrow.Cells["MeasureUnitID"].Value = Guid.NewGuid();
            newrow.Cells["MeasureUnitState"].Value = "normal";
            newrow.Cells["MeasureUnitName"].Value = "имя";
            IsLoaded = true;
            BS_Update();
            newrow.Cells["MeasureUnitName"].Selected = true;
        }
        private void table_MeasureUnitsDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            BS_Save();
        }
        private void toolStripButtonDelete_Click(object sender, EventArgs e)
        {
            //BS_DeleteRow(table_MeasureUnitsDataGridView);// ссылки в базе !!!
        }
        public Form_MeasureUnits()
        { InitializeComponent(); }
        private void Form_MeasureUnits_Load(object sender, EventArgs e)
        {
            BS_Refresh(table_MeasureUnitsDataGridView);
            table_MeasureUnitsBindingSource.Filter = "Name <> '[ пусто ]'";
            IsLoaded = true;
        }
        private void toolStripButton_ToExcel_Click(object sender, EventArgs e)
        {
            BS_ToExcel(table_MeasureUnitsDataGridView);
        }
    }
}