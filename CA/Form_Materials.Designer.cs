﻿namespace CA
{
    partial class Form_Materials : Form_BaseStyle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Materials));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.comboBoxSection = new System.Windows.Forms.ComboBox();
            this.tableSectionsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.cA_DB_DataSet = new CA.CA_DB_DataSet();
            this.table_MaterialGroupsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.table_MaterialGroupsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.table_MaterialGroupsBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonAddGroup = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonDeleteGroup = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_ToExcel1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.table_MaterialGroupsDataGridView = new System.Windows.Forms.DataGridView();
            this.GroupID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GroupState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GroupName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem1 = new System.Windows.Forms.ToolStripButton();
            this.table_MaterialsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorCountItem1 = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveFirstItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem1 = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_ToExcel2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.table_MaterialsDataGridView = new System.Windows.Forms.DataGridView();
            this.MaterialID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaterialState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaterialSectionID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tableSectionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.MaterialGroupID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tableMaterialGroupsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.IsWork = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.MaterialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Matter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Article = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaterialMeasureUnitID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tableMeasureUnitsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button_MeasureUnits = new System.Windows.Forms.Button();
            this.table_MaterialGroupsTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_MaterialGroupsTableAdapter();
            this.tableAdapterManager = new CA.CA_DB_DataSetTableAdapters.TableAdapterManager();
            this.table_MaterialsTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_MaterialsTableAdapter();
            this.table_MeasureUnitsTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_MeasureUnitsTableAdapter();
            this.table_SectionsTableAdapter = new CA.CA_DB_DataSetTableAdapters.Table_SectionsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableSectionsBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cA_DB_DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_MaterialGroupsBindingNavigator)).BeginInit();
            this.table_MaterialGroupsBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_MaterialGroupsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_MaterialGroupsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_MaterialsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_MaterialsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableSectionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableMaterialGroupsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableMeasureUnitsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer3);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.AutoScroll = true;
            this.splitContainer2.Panel2.Controls.Add(this.bindingNavigator1);
            this.splitContainer2.Panel2.Controls.Add(this.table_MaterialsDataGridView);
            this.splitContainer2.Size = new System.Drawing.Size(988, 543);
            this.splitContainer2.SplitterDistance = 174;
            this.splitContainer2.TabIndex = 0;
            // 
            // splitContainer3
            // 
            this.splitContainer3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.AutoScroll = true;
            this.splitContainer3.Panel1.Controls.Add(this.button_MeasureUnits);
            this.splitContainer3.Panel1.Controls.Add(this.comboBoxSection);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.table_MaterialGroupsBindingNavigator);
            this.splitContainer3.Panel2.Controls.Add(this.table_MaterialGroupsDataGridView);
            this.splitContainer3.Size = new System.Drawing.Size(988, 174);
            this.splitContainer3.SplitterDistance = 354;
            this.splitContainer3.TabIndex = 0;
            // 
            // comboBoxSection
            // 
            this.comboBoxSection.DataSource = this.tableSectionsBindingSource1;
            this.comboBoxSection.DisplayMember = "Name";
            this.comboBoxSection.FormattingEnabled = true;
            this.comboBoxSection.Location = new System.Drawing.Point(10, 10);
            this.comboBoxSection.Name = "comboBoxSection";
            this.comboBoxSection.Size = new System.Drawing.Size(203, 21);
            this.comboBoxSection.TabIndex = 0;
            this.comboBoxSection.ValueMember = "id";
            this.comboBoxSection.SelectedIndexChanged += new System.EventHandler(this.comboBoxSection_SelectedIndexChanged);
            // 
            // tableSectionsBindingSource1
            // 
            this.tableSectionsBindingSource1.DataMember = "Table_Sections";
            this.tableSectionsBindingSource1.DataSource = this.cA_DB_DataSet;
            // 
            // cA_DB_DataSet
            // 
            this.cA_DB_DataSet.DataSetName = "CA_DB_DataSet";
            this.cA_DB_DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // table_MaterialGroupsBindingNavigator
            // 
            this.table_MaterialGroupsBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.table_MaterialGroupsBindingNavigator.BindingSource = this.table_MaterialGroupsBindingSource;
            this.table_MaterialGroupsBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.table_MaterialGroupsBindingNavigator.CountItemFormat = "из {0}";
            this.table_MaterialGroupsBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.table_MaterialGroupsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel2,
            this.toolStripSeparator2,
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.table_MaterialGroupsBindingNavigatorSaveItem,
            this.toolStripButtonAddGroup,
            this.toolStripSeparator5,
            this.toolStripButtonDeleteGroup,
            this.toolStripSeparator6,
            this.toolStripSeparator7,
            this.toolStripButton_ToExcel1,
            this.toolStripSeparator8});
            this.table_MaterialGroupsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.table_MaterialGroupsBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.table_MaterialGroupsBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.table_MaterialGroupsBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.table_MaterialGroupsBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.table_MaterialGroupsBindingNavigator.Name = "table_MaterialGroupsBindingNavigator";
            this.table_MaterialGroupsBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.table_MaterialGroupsBindingNavigator.Size = new System.Drawing.Size(626, 28);
            this.table_MaterialGroupsBindingNavigator.TabIndex = 1;
            this.table_MaterialGroupsBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorAddNewItem.Text = "Добавить";
            this.bindingNavigatorAddNewItem.Visible = false;
            // 
            // table_MaterialGroupsBindingSource
            // 
            this.table_MaterialGroupsBindingSource.DataMember = "Table_MaterialGroups";
            this.table_MaterialGroupsBindingSource.DataSource = this.cA_DB_DataSet;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(36, 25);
            this.bindingNavigatorCountItem.Text = "из {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Общее число элементов";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorDeleteItem.Text = "Удалить";
            this.bindingNavigatorDeleteItem.Visible = false;
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(95, 25);
            this.toolStripLabel2.Text = "Группы:   ";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveFirstItem.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMovePreviousItem.Text = "Переместить назад";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveNextItem.Text = "Переместить вперед";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveLastItem.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 28);
            // 
            // table_MaterialGroupsBindingNavigatorSaveItem
            // 
            this.table_MaterialGroupsBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.table_MaterialGroupsBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("table_MaterialGroupsBindingNavigatorSaveItem.Image")));
            this.table_MaterialGroupsBindingNavigatorSaveItem.Name = "table_MaterialGroupsBindingNavigatorSaveItem";
            this.table_MaterialGroupsBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 25);
            this.table_MaterialGroupsBindingNavigatorSaveItem.Text = "Сохранить данные";
            this.table_MaterialGroupsBindingNavigatorSaveItem.Visible = false;
            // 
            // toolStripButtonAddGroup
            // 
            this.toolStripButtonAddGroup.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonAddGroup.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAddGroup.Image")));
            this.toolStripButtonAddGroup.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAddGroup.Name = "toolStripButtonAddGroup";
            this.toolStripButtonAddGroup.Size = new System.Drawing.Size(63, 25);
            this.toolStripButtonAddGroup.Text = "Добавить";
            this.toolStripButtonAddGroup.Click += new System.EventHandler(this.toolStripButtonAddGroup_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonDeleteGroup
            // 
            this.toolStripButtonDeleteGroup.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonDeleteGroup.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDeleteGroup.Image")));
            this.toolStripButtonDeleteGroup.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDeleteGroup.Name = "toolStripButtonDeleteGroup";
            this.toolStripButtonDeleteGroup.Size = new System.Drawing.Size(55, 25);
            this.toolStripButtonDeleteGroup.Text = "Удалить";
            this.toolStripButtonDeleteGroup.Click += new System.EventHandler(this.toolStripButtonDeleteGroup_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButton_ToExcel1
            // 
            this.toolStripButton_ToExcel1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton_ToExcel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_ToExcel1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_ToExcel1.Image")));
            this.toolStripButton_ToExcel1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_ToExcel1.Name = "toolStripButton_ToExcel1";
            this.toolStripButton_ToExcel1.Size = new System.Drawing.Size(46, 25);
            this.toolStripButton_ToExcel1.Text = "в Excel";
            this.toolStripButton_ToExcel1.Click += new System.EventHandler(this.toolStripButton_ToExcel1_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 28);
            // 
            // table_MaterialGroupsDataGridView
            // 
            this.table_MaterialGroupsDataGridView.AllowUserToAddRows = false;
            this.table_MaterialGroupsDataGridView.AllowUserToDeleteRows = false;
            this.table_MaterialGroupsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.table_MaterialGroupsDataGridView.AutoGenerateColumns = false;
            this.table_MaterialGroupsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_MaterialGroupsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GroupID,
            this.GroupState,
            this.GroupName});
            this.table_MaterialGroupsDataGridView.DataSource = this.table_MaterialGroupsBindingSource;
            this.table_MaterialGroupsDataGridView.Location = new System.Drawing.Point(0, 28);
            this.table_MaterialGroupsDataGridView.MultiSelect = false;
            this.table_MaterialGroupsDataGridView.Name = "table_MaterialGroupsDataGridView";
            this.table_MaterialGroupsDataGridView.Size = new System.Drawing.Size(625, 141);
            this.table_MaterialGroupsDataGridView.TabIndex = 0;
            this.table_MaterialGroupsDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.table_MaterialGroupsDataGridView_CellBeginEdit);
            this.table_MaterialGroupsDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.table_MaterialGroupsDataGridView_CellValueChanged);
            // 
            // GroupID
            // 
            this.GroupID.DataPropertyName = "id";
            this.GroupID.HeaderText = "id";
            this.GroupID.Name = "GroupID";
            this.GroupID.Visible = false;
            this.GroupID.Width = 40;
            // 
            // GroupState
            // 
            this.GroupState.DataPropertyName = "State";
            this.GroupState.HeaderText = "State";
            this.GroupState.Name = "GroupState";
            this.GroupState.Visible = false;
            // 
            // GroupName
            // 
            this.GroupName.DataPropertyName = "Name";
            this.GroupName.HeaderText = "Группа";
            this.GroupName.Name = "GroupName";
            this.GroupName.Width = 67;
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = this.bindingNavigatorAddNewItem1;
            this.bindingNavigator1.BindingSource = this.table_MaterialsBindingSource;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem1;
            this.bindingNavigator1.CountItemFormat = "из {0}";
            this.bindingNavigator1.DeleteItem = this.bindingNavigatorDeleteItem1;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.toolStripSeparator1,
            this.bindingNavigatorMoveFirstItem1,
            this.bindingNavigatorMovePreviousItem1,
            this.bindingNavigatorSeparator3,
            this.bindingNavigatorPositionItem1,
            this.bindingNavigatorCountItem1,
            this.bindingNavigatorSeparator4,
            this.bindingNavigatorMoveNextItem1,
            this.bindingNavigatorMoveLastItem1,
            this.bindingNavigatorSeparator5,
            this.bindingNavigatorAddNewItem1,
            this.bindingNavigatorDeleteItem1,
            this.toolStripButtonAdd,
            this.toolStripSeparator3,
            this.toolStripButtonDelete,
            this.toolStripSeparator4,
            this.toolStripSeparator9,
            this.toolStripButton_ToExcel2,
            this.toolStripSeparator10});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator1.MoveFirstItem = this.bindingNavigatorMoveFirstItem1;
            this.bindingNavigator1.MoveLastItem = this.bindingNavigatorMoveLastItem1;
            this.bindingNavigator1.MoveNextItem = this.bindingNavigatorMoveNextItem1;
            this.bindingNavigator1.MovePreviousItem = this.bindingNavigatorMovePreviousItem1;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem1;
            this.bindingNavigator1.Size = new System.Drawing.Size(984, 28);
            this.bindingNavigator1.TabIndex = 1;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem1
            // 
            this.bindingNavigatorAddNewItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem1.Image")));
            this.bindingNavigatorAddNewItem1.Name = "bindingNavigatorAddNewItem1";
            this.bindingNavigatorAddNewItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorAddNewItem1.Text = "Добавить";
            this.bindingNavigatorAddNewItem1.Visible = false;
            // 
            // table_MaterialsBindingSource
            // 
            this.table_MaterialsBindingSource.DataMember = "FK_Table_Materials_Table_MaterialGroups";
            this.table_MaterialsBindingSource.DataSource = this.table_MaterialGroupsBindingSource;
            // 
            // bindingNavigatorCountItem1
            // 
            this.bindingNavigatorCountItem1.Name = "bindingNavigatorCountItem1";
            this.bindingNavigatorCountItem1.Size = new System.Drawing.Size(36, 25);
            this.bindingNavigatorCountItem1.Text = "из {0}";
            this.bindingNavigatorCountItem1.ToolTipText = "Общее число элементов";
            // 
            // bindingNavigatorDeleteItem1
            // 
            this.bindingNavigatorDeleteItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem1.Image")));
            this.bindingNavigatorDeleteItem1.Name = "bindingNavigatorDeleteItem1";
            this.bindingNavigatorDeleteItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorDeleteItem1.Text = "Удалить";
            this.bindingNavigatorDeleteItem1.Visible = false;
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(131, 25);
            this.toolStripLabel1.Text = "Материалы:   ";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveFirstItem1
            // 
            this.bindingNavigatorMoveFirstItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem1.Image")));
            this.bindingNavigatorMoveFirstItem1.Name = "bindingNavigatorMoveFirstItem1";
            this.bindingNavigatorMoveFirstItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveFirstItem1.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem1
            // 
            this.bindingNavigatorMovePreviousItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem1.Image")));
            this.bindingNavigatorMovePreviousItem1.Name = "bindingNavigatorMovePreviousItem1";
            this.bindingNavigatorMovePreviousItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMovePreviousItem1.Text = "Переместить назад";
            // 
            // bindingNavigatorSeparator3
            // 
            this.bindingNavigatorSeparator3.Name = "bindingNavigatorSeparator3";
            this.bindingNavigatorSeparator3.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorPositionItem1
            // 
            this.bindingNavigatorPositionItem1.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem1.AutoSize = false;
            this.bindingNavigatorPositionItem1.Name = "bindingNavigatorPositionItem1";
            this.bindingNavigatorPositionItem1.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem1.Text = "0";
            this.bindingNavigatorPositionItem1.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorSeparator4
            // 
            this.bindingNavigatorSeparator4.Name = "bindingNavigatorSeparator4";
            this.bindingNavigatorSeparator4.Size = new System.Drawing.Size(6, 28);
            // 
            // bindingNavigatorMoveNextItem1
            // 
            this.bindingNavigatorMoveNextItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem1.Image")));
            this.bindingNavigatorMoveNextItem1.Name = "bindingNavigatorMoveNextItem1";
            this.bindingNavigatorMoveNextItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveNextItem1.Text = "Переместить вперед";
            // 
            // bindingNavigatorMoveLastItem1
            // 
            this.bindingNavigatorMoveLastItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem1.Image")));
            this.bindingNavigatorMoveLastItem1.Name = "bindingNavigatorMoveLastItem1";
            this.bindingNavigatorMoveLastItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem1.Size = new System.Drawing.Size(23, 25);
            this.bindingNavigatorMoveLastItem1.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator5
            // 
            this.bindingNavigatorSeparator5.Name = "bindingNavigatorSeparator5";
            this.bindingNavigatorSeparator5.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonAdd
            // 
            this.toolStripButtonAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAdd.Image")));
            this.toolStripButtonAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAdd.Name = "toolStripButtonAdd";
            this.toolStripButtonAdd.Size = new System.Drawing.Size(63, 25);
            this.toolStripButtonAdd.Text = "Добавить";
            this.toolStripButtonAdd.Click += new System.EventHandler(this.toolStripButtonAdd_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonDelete
            // 
            this.toolStripButtonDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonDelete.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDelete.Image")));
            this.toolStripButtonDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDelete.Name = "toolStripButtonDelete";
            this.toolStripButtonDelete.Size = new System.Drawing.Size(55, 25);
            this.toolStripButtonDelete.Text = "Удалить";
            this.toolStripButtonDelete.Click += new System.EventHandler(this.toolStripButtonDelete_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButton_ToExcel2
            // 
            this.toolStripButton_ToExcel2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton_ToExcel2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_ToExcel2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_ToExcel2.Image")));
            this.toolStripButton_ToExcel2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_ToExcel2.Name = "toolStripButton_ToExcel2";
            this.toolStripButton_ToExcel2.Size = new System.Drawing.Size(46, 25);
            this.toolStripButton_ToExcel2.Text = "в Excel";
            this.toolStripButton_ToExcel2.Click += new System.EventHandler(this.toolStripButton_ToExcel2_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 28);
            // 
            // table_MaterialsDataGridView
            // 
            this.table_MaterialsDataGridView.AllowUserToAddRows = false;
            this.table_MaterialsDataGridView.AllowUserToDeleteRows = false;
            this.table_MaterialsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.table_MaterialsDataGridView.AutoGenerateColumns = false;
            this.table_MaterialsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_MaterialsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaterialID,
            this.MaterialState,
            this.MaterialSectionID,
            this.MaterialGroupID,
            this.IsWork,
            this.MaterialName,
            this.Matter,
            this.Article,
            this.MaterialMeasureUnitID,
            this.Price});
            this.table_MaterialsDataGridView.DataSource = this.table_MaterialsBindingSource;
            this.table_MaterialsDataGridView.Location = new System.Drawing.Point(0, 28);
            this.table_MaterialsDataGridView.MultiSelect = false;
            this.table_MaterialsDataGridView.Name = "table_MaterialsDataGridView";
            this.table_MaterialsDataGridView.Size = new System.Drawing.Size(981, 335);
            this.table_MaterialsDataGridView.TabIndex = 0;
            this.table_MaterialsDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.table_MaterialsDataGridView_CellBeginEdit);
            this.table_MaterialsDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.table_MaterialsDataGridView_CellValueChanged);
            this.table_MaterialsDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.table_MaterialsDataGridView_DataError);
            // 
            // MaterialID
            // 
            this.MaterialID.DataPropertyName = "id";
            this.MaterialID.HeaderText = "id";
            this.MaterialID.Name = "MaterialID";
            this.MaterialID.Visible = false;
            this.MaterialID.Width = 40;
            // 
            // MaterialState
            // 
            this.MaterialState.DataPropertyName = "State";
            this.MaterialState.HeaderText = "State";
            this.MaterialState.Name = "MaterialState";
            this.MaterialState.Visible = false;
            // 
            // MaterialSectionID
            // 
            this.MaterialSectionID.DataPropertyName = "SectionID";
            this.MaterialSectionID.DataSource = this.tableSectionsBindingSource;
            this.MaterialSectionID.DisplayMember = "Name";
            this.MaterialSectionID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.MaterialSectionID.HeaderText = "Секция";
            this.MaterialSectionID.Name = "MaterialSectionID";
            this.MaterialSectionID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.MaterialSectionID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.MaterialSectionID.ValueMember = "id";
            this.MaterialSectionID.Width = 69;
            // 
            // tableSectionsBindingSource
            // 
            this.tableSectionsBindingSource.DataMember = "Table_Sections";
            this.tableSectionsBindingSource.DataSource = this.cA_DB_DataSet;
            // 
            // MaterialGroupID
            // 
            this.MaterialGroupID.DataPropertyName = "GroupID";
            this.MaterialGroupID.DataSource = this.tableMaterialGroupsBindingSource;
            this.MaterialGroupID.DisplayMember = "Name";
            this.MaterialGroupID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.MaterialGroupID.HeaderText = "Группа";
            this.MaterialGroupID.Name = "MaterialGroupID";
            this.MaterialGroupID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.MaterialGroupID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.MaterialGroupID.ValueMember = "id";
            this.MaterialGroupID.Width = 67;
            // 
            // tableMaterialGroupsBindingSource
            // 
            this.tableMaterialGroupsBindingSource.DataMember = "Table_MaterialGroups";
            this.tableMaterialGroupsBindingSource.DataSource = this.cA_DB_DataSet;
            // 
            // IsWork
            // 
            this.IsWork.DataPropertyName = "IsWork";
            this.IsWork.HeaderText = "Работы";
            this.IsWork.Name = "IsWork";
            this.IsWork.Width = 51;
            // 
            // MaterialName
            // 
            this.MaterialName.DataPropertyName = "Name";
            this.MaterialName.HeaderText = "Наименование";
            this.MaterialName.Name = "MaterialName";
            this.MaterialName.Width = 108;
            // 
            // Matter
            // 
            this.Matter.DataPropertyName = "Matter";
            this.Matter.HeaderText = "Состав (материал)";
            this.Matter.Name = "Matter";
            this.Matter.Width = 115;
            // 
            // Article
            // 
            this.Article.DataPropertyName = "Article";
            this.Article.HeaderText = "Артикул";
            this.Article.Name = "Article";
            this.Article.Width = 73;
            // 
            // MaterialMeasureUnitID
            // 
            this.MaterialMeasureUnitID.DataPropertyName = "MeasureUnitID";
            this.MaterialMeasureUnitID.DataSource = this.tableMeasureUnitsBindingSource;
            this.MaterialMeasureUnitID.DisplayMember = "Name";
            this.MaterialMeasureUnitID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.MaterialMeasureUnitID.HeaderText = "ЕИ";
            this.MaterialMeasureUnitID.Name = "MaterialMeasureUnitID";
            this.MaterialMeasureUnitID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.MaterialMeasureUnitID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.MaterialMeasureUnitID.ValueMember = "id";
            this.MaterialMeasureUnitID.Width = 47;
            // 
            // tableMeasureUnitsBindingSource
            // 
            this.tableMeasureUnitsBindingSource.DataMember = "Table_MeasureUnits";
            this.tableMeasureUnitsBindingSource.DataSource = this.cA_DB_DataSet;
            // 
            // Price
            // 
            this.Price.DataPropertyName = "Price";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "C2";
            dataGridViewCellStyle2.NullValue = null;
            this.Price.DefaultCellStyle = dataGridViewCellStyle2;
            this.Price.HeaderText = "Актуальная цена";
            this.Price.Name = "Price";
            this.Price.Width = 108;
            // 
            // button_MeasureUnits
            // 
            this.button_MeasureUnits.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_MeasureUnits.Location = new System.Drawing.Point(10, 127);
            this.button_MeasureUnits.Name = "button_MeasureUnits";
            this.button_MeasureUnits.Size = new System.Drawing.Size(130, 40);
            this.button_MeasureUnits.TabIndex = 11;
            this.button_MeasureUnits.Text = "Единицы измерения";
            this.button_MeasureUnits.UseVisualStyleBackColor = true;
            this.button_MeasureUnits.Click += new System.EventHandler(this.button_MeasureUnits_Click);
            // 
            // table_MaterialGroupsTableAdapter
            // 
            this.table_MaterialGroupsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Table_CompaniesTableAdapter = null;
            this.tableAdapterManager.Table_CompanyTypesTableAdapter = null;
            this.tableAdapterManager.Table_EmployeesTableAdapter = null;
            this.tableAdapterManager.Table_HistoryTableAdapter = null;
            this.tableAdapterManager.Table_MaterialGroupsTableAdapter = this.table_MaterialGroupsTableAdapter;
            this.tableAdapterManager.Table_MaterialsTableAdapter = this.table_MaterialsTableAdapter;
            this.tableAdapterManager.Table_MeasureUnitsTableAdapter = this.table_MeasureUnitsTableAdapter;
            this.tableAdapterManager.Table_MoneyFlowTableAdapter = null;
            this.tableAdapterManager.Table_ProductionStagesTableAdapter = null;
            this.tableAdapterManager.Table_ProjectFilesTableAdapter = null;
            this.tableAdapterManager.Table_ProjectsTableAdapter = null;
            this.tableAdapterManager.Table_ProjectStatusesTableAdapter = null;
            this.tableAdapterManager.Table_SectionsTableAdapter = this.table_SectionsTableAdapter;
            this.tableAdapterManager.Table_SubProjectsTableAdapter = null;
            this.tableAdapterManager.Table_TEOMaterialsTableAdapter = null;
            this.tableAdapterManager.Table_UsersTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = CA.CA_DB_DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // table_MaterialsTableAdapter
            // 
            this.table_MaterialsTableAdapter.ClearBeforeFill = true;
            // 
            // table_MeasureUnitsTableAdapter
            // 
            this.table_MeasureUnitsTableAdapter.ClearBeforeFill = true;
            // 
            // table_SectionsTableAdapter
            // 
            this.table_SectionsTableAdapter.ClearBeforeFill = true;
            // 
            // Form_Materials
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(988, 565);
            this.Controls.Add(this.splitContainer2);
            this.Name = "Form_Materials";
            this.Text = "Материалы";
            this.Load += new System.EventHandler(this.Form_Materials_Load);
            this.Controls.SetChildIndex(this.splitContainer2, 0);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tableSectionsBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cA_DB_DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_MaterialGroupsBindingNavigator)).EndInit();
            this.table_MaterialGroupsBindingNavigator.ResumeLayout(false);
            this.table_MaterialGroupsBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_MaterialGroupsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_MaterialGroupsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_MaterialsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table_MaterialsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableSectionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableMaterialGroupsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableMeasureUnitsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private CA_DB_DataSet cA_DB_DataSet;
        private System.Windows.Forms.BindingSource table_MaterialGroupsBindingSource;
        private CA_DB_DataSetTableAdapters.Table_MaterialGroupsTableAdapter table_MaterialGroupsTableAdapter;
        private CA_DB_DataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator table_MaterialGroupsBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton table_MaterialGroupsBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView table_MaterialGroupsDataGridView;
        private CA_DB_DataSetTableAdapters.Table_MaterialsTableAdapter table_MaterialsTableAdapter;
        private System.Windows.Forms.BindingSource table_MaterialsBindingSource;
        private System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem1;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator3;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator4;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator5;
        private System.Windows.Forms.DataGridView table_MaterialsDataGridView;
        private CA_DB_DataSetTableAdapters.Table_SectionsTableAdapter table_SectionsTableAdapter;
        private System.Windows.Forms.BindingSource tableSectionsBindingSource;
        private CA_DB_DataSetTableAdapters.Table_MeasureUnitsTableAdapter table_MeasureUnitsTableAdapter;
        private System.Windows.Forms.BindingSource tableMaterialGroupsBindingSource;
        private System.Windows.Forms.BindingSource tableMeasureUnitsBindingSource;
        private System.Windows.Forms.ToolStripButton toolStripButtonDelete;
        private System.Windows.Forms.ToolStripButton toolStripButtonDeleteGroup;
        private System.Windows.Forms.ComboBox comboBoxSection;
        private System.Windows.Forms.BindingSource tableSectionsBindingSource1;
        private System.Windows.Forms.Button button_MeasureUnits;
        private System.Windows.Forms.ToolStripButton toolStripButtonAdd;
        private System.Windows.Forms.ToolStripButton toolStripButtonAddGroup;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaterialID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaterialState;
        private System.Windows.Forms.DataGridViewComboBoxColumn MaterialSectionID;
        private System.Windows.Forms.DataGridViewComboBoxColumn MaterialGroupID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsWork;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaterialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Matter;
        private System.Windows.Forms.DataGridViewTextBoxColumn Article;
        private System.Windows.Forms.DataGridViewComboBoxColumn MaterialMeasureUnitID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn GroupID;
        private System.Windows.Forms.DataGridViewTextBoxColumn GroupState;
        private System.Windows.Forms.DataGridViewTextBoxColumn GroupName;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton toolStripButton_ToExcel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripButton toolStripButton_ToExcel2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
    }
}